\select@language {english}
\select@language {english}
\contentsline {chapter}{List of Figures}{15}
\contentsline {chapter}{List of Tables}{19}
\contentsline {chapter}{\numberline {1}Introduction}{23}
\contentsline {section}{\numberline {1.1}Author's contributions}{24}
\contentsline {subsubsection}{NEMO-3}{24}
\contentsline {subsubsection}{SuperNEMO}{25}
\contentsline {chapter}{\numberline {2}Neutrino Phenomenology}{27}
\contentsline {section}{\numberline {2.1}Historical overview}{27}
\contentsline {subsection}{\numberline {2.1.1}Radioactive decay}{27}
\contentsline {subsection}{\numberline {2.1.2}Proposal and observation of the neutrino}{28}
\contentsline {subsection}{\numberline {2.1.3}Parity violation in weak interactions}{28}
\contentsline {subsection}{\numberline {2.1.4}Neutrino flavour}{29}
\contentsline {section}{\numberline {2.2}Neutrinos in the SM}{30}
\contentsline {section}{\numberline {2.3}Neutrino oscillations}{31}
\contentsline {subsection}{\numberline {2.3.1}The solar neutrino anomaly}{33}
\contentsline {subsection}{\numberline {2.3.2}Neutrino mixing}{34}
\contentsline {subsection}{\numberline {2.3.3}Oscillations in matter}{35}
\contentsline {subsection}{\numberline {2.3.4}Oscillation parameters}{36}
\contentsline {subsection}{\numberline {2.3.5}Mass hierarchy}{37}
\contentsline {section}{\numberline {2.4}Neutrino Mass}{37}
\contentsline {subsection}{\numberline {2.4.1}Dirac mass term}{38}
\contentsline {subsection}{\numberline {2.4.2}Majorana mass term}{39}
\contentsline {subsection}{\numberline {2.4.3}See-saw mechanism}{40}
\contentsline {section}{\numberline {2.5}Constraints on neutrino mass}{41}
\contentsline {subsection}{\numberline {2.5.1}Beta decay of tritium}{41}
\contentsline {subsection}{\numberline {2.5.2}Cosmology}{43}
\contentsline {subsection}{\numberline {2.5.3}Neutrinoless double-$\beta $ decay}{43}
\contentsline {section}{\numberline {2.6}Summary and outlook}{44}
\contentsline {chapter}{\numberline {3}Double-$\beta $ decay}{47}
\contentsline {section}{\numberline {3.1}$\beta $ decay}{47}
\contentsline {subsection}{\numberline {3.1.1}The semi-empirical mass formula}{48}
\contentsline {section}{\numberline {3.2}Two-neutrino double-$\beta $ decay}{50}
\contentsline {section}{\numberline {3.3}Neutrinoless double-$\beta $ decay}{52}
\contentsline {subsection}{\numberline {3.3.1}Mass mechanism}{53}
\contentsline {subsection}{\numberline {3.3.2}${\mathchoice {{\setbox \z@ \hbox {$\mathsurround \z@ \displaystyle R$}\setbox \tw@ \hbox {$\mathsurround \z@ \displaystyle /$}\dimen 4\wd \z@ \dimen@ \ht \tw@ \advance \dimen@ -\dp \tw@ \advance \dimen@ -\ht \z@ \advance \dimen@ \dp \z@ \divide \dimen@ \tw@ \advance \dimen@ -0\ht \tw@ \advance \dimen@ -0\dp \tw@ \dimen@ii 0\wd \z@ \raise -\dimen@ \hbox to\dimen 4{\hss \kern \dimen@ii \box \tw@ \kern -\dimen@ii \hss }\hbox to\z@ {\hss \hbox to\dimen 4{\hss \box \z@ \hss }}}}{{\setbox \z@ \hbox {$\mathsurround \z@ \textstyle R$}\setbox \tw@ \hbox {$\mathsurround \z@ \textstyle /$}\dimen 4\wd \z@ \dimen@ \ht \tw@ \advance \dimen@ -\dp \tw@ \advance \dimen@ -\ht \z@ \advance \dimen@ \dp \z@ \divide \dimen@ \tw@ \advance \dimen@ -0\ht \tw@ \advance \dimen@ -0\dp \tw@ \dimen@ii 0\wd \z@ \raise -\dimen@ \hbox to\dimen 4{\hss \kern \dimen@ii \box \tw@ \kern -\dimen@ii \hss }\hbox to\z@ {\hss \hbox to\dimen 4{\hss \box \z@ \hss }}}}{{\setbox \z@ \hbox {$\mathsurround \z@ \scriptstyle R$}\setbox \tw@ \hbox {$\mathsurround \z@ \scriptstyle /$}\dimen 4\wd \z@ \dimen@ \ht \tw@ \advance \dimen@ -\dp \tw@ \advance \dimen@ -\ht \z@ \advance \dimen@ \dp \z@ \divide \dimen@ \tw@ \advance \dimen@ -0\ht \tw@ \advance \dimen@ -0\dp \tw@ \dimen@ii 0\wd \z@ \raise -\dimen@ \hbox to\dimen 4{\hss \kern \dimen@ii \box \tw@ \kern -\dimen@ii \hss }\hbox to\z@ {\hss \hbox to\dimen 4{\hss \box \z@ \hss }}}}{{\setbox \z@ \hbox {$\mathsurround \z@ \scriptscriptstyle R$}\setbox \tw@ \hbox {$\mathsurround \z@ \scriptscriptstyle /$}\dimen 4\wd \z@ \dimen@ \ht \tw@ \advance \dimen@ -\dp \tw@ \advance \dimen@ -\ht \z@ \advance \dimen@ \dp \z@ \divide \dimen@ \tw@ \advance \dimen@ -0\ht \tw@ \advance \dimen@ -0\dp \tw@ \dimen@ii 0\wd \z@ \raise -\dimen@ \hbox to\dimen 4{\hss \kern \dimen@ii \box \tw@ \kern -\dimen@ii \hss }\hbox to\z@ {\hss \hbox to\dimen 4{\hss \box \z@ \hss }}}}}_p$ supersymmetry}{54}
\contentsline {subsection}{\numberline {3.3.3}Right handed currents}{55}
\contentsline {subsection}{\numberline {3.3.4}Majoron emission}{56}
\contentsline {section}{\numberline {3.4}Nuclear matrix elements}{56}
\contentsline {subsection}{\numberline {3.4.1}Comparison of calculations}{58}
\contentsline {chapter}{\numberline {4}Double-$\beta $ decay experiments and techniques}{61}
\contentsline {section}{\numberline {4.1}Detector technologies}{65}
\contentsline {subsection}{\numberline {4.1.1}Semiconductor experiments}{65}
\contentsline {subsection}{\numberline {4.1.2}Bolometer experiments}{67}
\contentsline {subsection}{\numberline {4.1.3}Scintillator experiments}{67}
\contentsline {subsection}{\numberline {4.1.4}Time projection chamber experiments}{68}
\contentsline {subsection}{\numberline {4.1.5}Tracker and calorimeter experiments}{69}
\contentsline {section}{\numberline {4.2}Experiments with \Ca {48}}{69}
\contentsline {subsubsection}{Mateosian-Goldhaber experiment}{70}
\contentsline {subsubsection}{Beijing experiment}{70}
\contentsline {subsubsection}{Hoover Dam experiment}{70}
\contentsline {subsubsection}{TGV}{70}
\contentsline {subsubsection}{ELEGANT VI}{71}
\contentsline {subsubsection}{CANDLES}{71}
\contentsline {section}{\numberline {4.3}Current status of double-$\beta $ decay measurements}{71}
\contentsline {part}{\numberline {I}Search for double-$\beta $ decay of $^{48}$Ca with the NEMO-3 detector}{73}
\contentsline {chapter}{\numberline {5}The NEMO-3 detector}{75}
\contentsline {section}{\numberline {5.1}Source foils}{75}
\contentsline {subsection}{\numberline {5.1.1}\Ca {48} source}{79}
\contentsline {subsubsection}{Rotation of the \Ca {48} source from the nominal position}{79}
\contentsline {section}{\numberline {5.2}Tracker}{81}
\contentsline {section}{\numberline {5.3}Calorimeter}{84}
\contentsline {section}{\numberline {5.4}Electronics, DAQ and trigger}{85}
\contentsline {subsection}{\numberline {5.4.1}Calorimeter electronics}{86}
\contentsline {subsection}{\numberline {5.4.2}Tracker electronics}{86}
\contentsline {subsection}{\numberline {5.4.3}Trigger system}{87}
\contentsline {section}{\numberline {5.5}Energy and time calibration}{88}
\contentsline {subsection}{\numberline {5.5.1}Radioactive sources}{88}
\contentsline {subsection}{\numberline {5.5.2}Laser calibration system}{89}
\contentsline {section}{\numberline {5.6}Magnetic field}{90}
\contentsline {section}{\numberline {5.7}Passive shielding}{90}
\contentsline {section}{\numberline {5.8}Anti-radon facility}{90}
\contentsline {chapter}{\numberline {6}General analysis techniques}{93}
\contentsline {section}{\numberline {6.1}Monte Carlo simulation}{93}
\contentsline {section}{\numberline {6.2}Event reconstruction}{94}
\contentsline {section}{\numberline {6.3}Particle identification}{95}
\contentsline {subsection}{\numberline {6.3.1}Electrons}{95}
\contentsline {subsection}{\numberline {6.3.2}Photons}{95}
\contentsline {subsection}{\numberline {6.3.3}Alpha emission from \Bi {214} - \Po {214} decays}{96}
\contentsline {section}{\numberline {6.4}Time of flight}{97}
\contentsline {subsection}{\numberline {6.4.1}Internal hypothesis}{98}
\contentsline {subsubsection}{Electrons}{99}
\contentsline {subsubsection}{$\gamma $'s}{99}
\contentsline {subsection}{\numberline {6.4.2}External hypothesis}{100}
\contentsline {subsubsection}{Crossing electron}{101}
\contentsline {subsubsection}{External electron and $\gamma $}{102}
\contentsline {section}{\numberline {6.5}Statistical analysis}{102}
\contentsline {subsection}{\numberline {6.5.1}Fitting MC predictions to data}{103}
\contentsline {subsection}{\numberline {6.5.2}Limit setting}{105}
\contentsline {section}{\numberline {6.6}Half-life calculation}{107}
\contentsline {section}{\numberline {6.7}Analysis data set}{108}
\contentsline {chapter}{\numberline {7}Measurement of backgrounds in the \Ca {48} source}{109}
\contentsline {section}{\numberline {7.1}Background production processes}{109}
\contentsline {subsection}{\numberline {7.1.1}Background classification}{110}
\contentsline {subsubsection}{Internal backgrounds}{110}
\contentsline {subsubsection}{External backgrounds}{112}
\contentsline {subsubsection}{Radon backgrounds}{112}
\contentsline {section}{\numberline {7.2}Sources of background}{112}
\contentsline {subsection}{\numberline {7.2.1}Decay chains}{114}
\contentsline {subsection}{\numberline {7.2.2}\Rn {222}}{117}
\contentsline {subsection}{\numberline {7.2.3}Other natural background sources}{119}
\contentsline {subsection}{\numberline {7.2.4}Artificial contaminants in source foil}{119}
\contentsline {subsubsection}{\Sr {90} in the \Ca {48} source}{120}
\contentsline {subsection}{\numberline {7.2.5}\2nu as a background to \0nu }{120}
\contentsline {subsection}{\numberline {7.2.6}Leakage}{121}
\contentsline {section}{\numberline {7.3}NEMO-3 background model}{121}
\contentsline {section}{\numberline {7.4}HPGe measurements of internal backgrounds}{123}
\contentsline {section}{\numberline {7.5}Background fitting procedure}{124}
\contentsline {section}{\numberline {7.6}General event selection}{126}
\contentsline {subsection}{\numberline {7.6.1}Hot runs}{126}
\contentsline {subsection}{\numberline {7.6.2}Energy threshold}{126}
\contentsline {section}{\numberline {7.7}Background measurement channels}{129}
\contentsline {subsection}{\numberline {7.7.1}1eN$\gamma $}{129}
\contentsline {subsection}{\numberline {7.7.2}1e1$\gamma $ (internal)}{130}
\contentsline {subsection}{\numberline {7.7.3}1e2$\gamma $}{131}
\contentsline {subsection}{\numberline {7.7.4}1e1$\alpha $}{133}
\contentsline {subsection}{\numberline {7.7.5}External background measurement channels}{133}
\contentsline {subsubsection}{1e1$\gamma $ (external)}{133}
\contentsline {subsubsection}{Crossing electron}{135}
\contentsline {section}{\numberline {7.8}Background measurement results}{135}
\contentsline {subsubsection}{Internal backgrounds}{136}
\contentsline {subsubsection}{External backgrounds}{137}
\contentsline {subsubsection}{Radon backgrounds}{138}
\contentsline {chapter}{\numberline {8}\Ca {48} double-$\beta $ decay results}{139}
\contentsline {section}{\numberline {8.1}2eN$\gamma $ event selection}{139}
\contentsline {subsection}{\numberline {8.1.1}Validation of 2eN$\gamma $ selection on \Bi {207} calibration source data}{141}
\contentsline {section}{\numberline {8.2}2eN$\gamma $ distributions}{142}
\contentsline {section}{\numberline {8.3}Systematic uncertainty on the \2nu measurement}{145}
\contentsline {subsection}{\numberline {8.3.1}Optimisation of \2nu fitting window}{149}
\contentsline {section}{\numberline {8.4}\2nu half-life result and discussion}{151}
\contentsline {section}{\numberline {8.5}Limits on \0nu and discussion of results}{153}
\contentsline {subsubsection}{Mass mechanism}{154}
\contentsline {subsubsection}{${\mathchoice {{\setbox \z@ \hbox {$\mathsurround \z@ \displaystyle R$}\setbox \tw@ \hbox {$\mathsurround \z@ \displaystyle /$}\dimen 4\wd \z@ \dimen@ \ht \tw@ \advance \dimen@ -\dp \tw@ \advance \dimen@ -\ht \z@ \advance \dimen@ \dp \z@ \divide \dimen@ \tw@ \advance \dimen@ -0\ht \tw@ \advance \dimen@ -0\dp \tw@ \dimen@ii 0\wd \z@ \raise -\dimen@ \hbox to\dimen 4{\hss \kern \dimen@ii \box \tw@ \kern -\dimen@ii \hss }\hbox to\z@ {\hss \hbox to\dimen 4{\hss \box \z@ \hss }}}}{{\setbox \z@ \hbox {$\mathsurround \z@ \textstyle R$}\setbox \tw@ \hbox {$\mathsurround \z@ \textstyle /$}\dimen 4\wd \z@ \dimen@ \ht \tw@ \advance \dimen@ -\dp \tw@ \advance \dimen@ -\ht \z@ \advance \dimen@ \dp \z@ \divide \dimen@ \tw@ \advance \dimen@ -0\ht \tw@ \advance \dimen@ -0\dp \tw@ \dimen@ii 0\wd \z@ \raise -\dimen@ \hbox to\dimen 4{\hss \kern \dimen@ii \box \tw@ \kern -\dimen@ii \hss }\hbox to\z@ {\hss \hbox to\dimen 4{\hss \box \z@ \hss }}}}{{\setbox \z@ \hbox {$\mathsurround \z@ \scriptstyle R$}\setbox \tw@ \hbox {$\mathsurround \z@ \scriptstyle /$}\dimen 4\wd \z@ \dimen@ \ht \tw@ \advance \dimen@ -\dp \tw@ \advance \dimen@ -\ht \z@ \advance \dimen@ \dp \z@ \divide \dimen@ \tw@ \advance \dimen@ -0\ht \tw@ \advance \dimen@ -0\dp \tw@ \dimen@ii 0\wd \z@ \raise -\dimen@ \hbox to\dimen 4{\hss \kern \dimen@ii \box \tw@ \kern -\dimen@ii \hss }\hbox to\z@ {\hss \hbox to\dimen 4{\hss \box \z@ \hss }}}}{{\setbox \z@ \hbox {$\mathsurround \z@ \scriptscriptstyle R$}\setbox \tw@ \hbox {$\mathsurround \z@ \scriptscriptstyle /$}\dimen 4\wd \z@ \dimen@ \ht \tw@ \advance \dimen@ -\dp \tw@ \advance \dimen@ -\ht \z@ \advance \dimen@ \dp \z@ \divide \dimen@ \tw@ \advance \dimen@ -0\ht \tw@ \advance \dimen@ -0\dp \tw@ \dimen@ii 0\wd \z@ \raise -\dimen@ \hbox to\dimen 4{\hss \kern \dimen@ii \box \tw@ \kern -\dimen@ii \hss }\hbox to\z@ {\hss \hbox to\dimen 4{\hss \box \z@ \hss }}}}}_p$ supersymmetry}{157}
\contentsline {subsubsection}{Right-handed currents}{159}
\contentsline {subsubsection}{Majoron emission}{161}
\contentsline {part}{\numberline {II}Tracker commissioning for the SuperNEMO detector}{163}
\contentsline {chapter}{\numberline {9}The SuperNEMO experiment}{165}
\contentsline {section}{\numberline {9.1}The Demonstrator module}{166}
\contentsline {section}{\numberline {9.2}Research and development}{168}
\contentsline {subsubsection}{Source}{168}
\contentsline {subsubsection}{Calibration systems}{169}
\contentsline {subsubsection}{Calorimeter}{169}
\contentsline {subsubsection}{Radon emanation}{170}
\contentsline {subsubsection}{Tracker}{170}
\contentsline {section}{\numberline {9.3}Timescale and sensitivity}{171}
\contentsline {chapter}{\numberline {10}The SuperNEMO tracker}{173}
\contentsline {section}{\numberline {10.1}The physics of Geiger counters}{174}
\contentsline {section}{\numberline {10.2}The SuperNEMO tracker cells}{176}
\contentsline {section}{\numberline {10.3}The tracker frame}{180}
\contentsline {section}{\numberline {10.4}Construction of the tracker}{182}
\contentsline {section}{\numberline {10.5}Tracker electronics and DAQ}{183}
\contentsline {chapter}{\numberline {11}Electronics for surface commissioning}{187}
\contentsline {section}{\numberline {11.1}HV supply and distribution}{188}
\contentsline {section}{\numberline {11.2}Cosmic ray trigger}{191}
\contentsline {section}{\numberline {11.3}DAQ cards}{192}
\contentsline {section}{\numberline {11.4}VME controller and read-out cycle}{195}
\contentsline {subsection}{\numberline {11.4.1}Read-out cycle duration and dead time}{196}
\contentsline {section}{\numberline {11.5}Testing the tracker commissioning DAQ system on the cassette test tank}{198}
\contentsline {subsubsection}{$x$-axis scan}{199}
\contentsline {subsubsection}{Geiger plateau scan}{199}
\contentsline {subsubsection}{Cathode ring read-out}{201}
\contentsline {subsubsection}{Discussion of results}{204}
\contentsline {chapter}{\numberline {12}Gas mixing system for the Demonstrator module}{205}
\contentsline {section}{\numberline {12.1}Principle of operation}{205}
\contentsline {subsection}{\numberline {12.1.1}Two-stage ethanol vapour addition}{208}
\contentsline {section}{\numberline {12.2}Materials}{210}
\contentsline {section}{\numberline {12.3}Tests}{212}
\contentsline {section}{\numberline {12.4}Flow rate and \Rn {222} activity in the tracker}{213}
\contentsline {chapter}{\numberline {13}Conclusion}{215}
\contentsline {chapter}{Bibliography}{219}
