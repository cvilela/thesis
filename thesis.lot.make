\select@language {english}
\select@language {english}
\addvspace {10\p@ }
\addvspace {10\p@ }
\contentsline {table}{\numberline {\relax 2.1}{\ignorespaces Best fit oscillation parameters assuming normal hierarchy.}}{37}
\contentsline {table}{\numberline {\relax 2.2}{\ignorespaces Current limits on neutrino mass.}}{44}
\addvspace {10\p@ }
\addvspace {10\p@ }
\contentsline {table}{\numberline {\relax 4.1}{\ignorespaces $Q_{\beta \beta }$ and natural abundances for isotopes commonly considered for double-$\beta $ decay experiments.}}{63}
\contentsline {table}{\numberline {\relax 4.2}{\ignorespaces Most precise direct measurements of \2nu .}}{72}
\contentsline {table}{\numberline {\relax 4.3}{\ignorespaces Best limits on the \0nu half-life of the isotopes for which \2nu has been directly observed.}}{72}
\addvspace {10\p@ }
\addvspace {10\p@ }
\contentsline {table}{\numberline {\relax 6.1}{\ignorespaces Exposure of the NEMO-3 \emph {standard} analysis run set.}}{108}
\addvspace {10\p@ }
\contentsline {table}{\numberline {\relax 7.1}{\ignorespaces NEMO-3 external background model.}}{122}
\contentsline {table}{\numberline {\relax 7.2}{\ignorespaces HPGe measurement of internal backgrounds in the \Ca {48} source.}}{123}
\contentsline {table}{\numberline {\relax 7.3}{\ignorespaces Analysis channels used in the background measurement.}}{124}
\contentsline {table}{\numberline {\relax 7.4}{\ignorespaces Overview of the parameters used in the global fit.}}{125}
\contentsline {table}{\numberline {\relax 7.5}{\ignorespaces Results of internal background measurement.}}{136}
\contentsline {table}{\numberline {\relax 7.6}{\ignorespaces Results of external background measurement.}}{137}
\contentsline {table}{\numberline {\relax 7.7}{\ignorespaces Results of radon background measurement.}}{138}
\addvspace {10\p@ }
\contentsline {table}{\numberline {\relax 8.1}{\ignorespaces Measurement of the activity of \Bi {207} sources with NEMO-3 and HPGe detector data.}}{142}
\contentsline {table}{\numberline {\relax 8.2}{\ignorespaces Activity of the \Sr {90}/\Y {90} system measured in the global fit and in the 2eN$\gamma $ channel.}}{145}
\contentsline {table}{\numberline {\relax 8.3}{\ignorespaces Systematic uncertainty on the \2nu measurement due to data mis-modelling in ``same side'' and ``opposite side'' events.}}{149}
\contentsline {table}{\numberline {\relax 8.4}{\ignorespaces Total systematic uncertainty on the \2nu half-life measurement.}}{150}
\contentsline {table}{\numberline {\relax 8.5}{\ignorespaces Systematic uncertainties used in \0nu limits.}}{153}
\contentsline {table}{\numberline {\relax 8.6}{\ignorespaces Limits on \0nu modes.}}{154}
\addvspace {10\p@ }
\contentsline {table}{\numberline {\relax 9.1}{\ignorespaces Experimental parameters and $0\nu \beta \beta $ sensitivity for the SuperNEMO experiment and its Demonstrator module.}}{166}
\addvspace {10\p@ }
\addvspace {10\p@ }
\addvspace {10\p@ }
\addvspace {10\p@ }
