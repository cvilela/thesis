\@setckpt{chapters/conclusions/chapter.conclusions}{
\@setckpt{chapters/double-beta-decay-exp/chapter.double-beta-decay-exp}{
\@setckpt{chapters/double-beta-decay/chapter.double-beta-decay}{
\@setckpt{chapters/introduction/chapter.introduction}{
\@setckpt{chapters/nemo-3-analysis-techniques/chapter.nemo-3-analysis-techniques}{
\@setckpt{chapters/nemo-3-backgrounds/chapter.nemo-3-backgrounds}{
\@setckpt{chapters/nemo-3-detector/chapter.nemo-3-detector}{
\@setckpt{chapters/nemo-3-results/chapter.nemo-3-results}{
\@setckpt{chapters/neutrino-theory/chapter.neutrino-theory}{
\@setckpt{chapters/supernemo-detector/chapter.supernemo-detector}{
\@setckpt{chapters/supernemo-elec/chapter.supernemo-elec}{
\@setckpt{chapters/supernemo-gas/chapter.supernemo-gas}{
\@setckpt{chapters/supernemo-tracker/chapter.supernemo-tracker}{
\@setckpt{structure/backmatter}{
\@writefile{lof}{\addvspace {10\p@ }}
\@writefile{lof}{\addvspace {10\p@ }}
\@writefile{lof}{\addvspace {10\p@ }}
\@writefile{lof}{\addvspace {10\p@ }}
\@writefile{lof}{\addvspace {10\p@ }}
\@writefile{lof}{\addvspace {10\p@ }}
\@writefile{lof}{\addvspace {10\p@ }}
\@writefile{lof}{\addvspace {10\p@ }}
\@writefile{lof}{\addvspace {10\p@ }}
\@writefile{lof}{\addvspace {10\p@ }}
\@writefile{lof}{\addvspace {10\p@ }}
\@writefile{lof}{\addvspace {10\p@ }}
\@writefile{lof}{\addvspace {10\p@ }}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 10.1}{\ignorespaces Sketch of the signal generating process in Geiger counters.}}{175}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 10.2}{\ignorespaces Sketch of a SuperNEMO tracker cell.}}{177}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 10.3}{\ignorespaces Example set of signals from a Geiger discharge in a SuperNEMO tracker cell.}}{179}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 10.4}{\ignorespaces Sketch of a SuperNEMO tracker C-section.}}{181}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 10.5}{\ignorespaces Photograph of the wiring robot in the University of Manchester clean-room.}}{183}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 10.6}{\ignorespaces Photograph of the C0 frame at MSSL, partially populated with cells.}}{184}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 10.7}{\ignorespaces Derivative of the anode signal.}}{185}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 11.10}{\ignorespaces Distribution of the propagation time to the read out cathode ring for six cells in a cassette.}}{204}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 11.1}{\ignorespaces Schematic diagram of the tracker commissioning electronics.}}{189}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 11.2}{\ignorespaces The NEMO-3 HV distribution and decoupling system.}}{190}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 11.3}{\ignorespaces Timing diagram for the tracker commissioning DAQ system.}}{193}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 11.4}{\ignorespaces Read-out dead time and rate of the tracker commissioning DAQ system.}}{196}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 11.5}{\ignorespaces Photograph of the electronics and cosmic ray trigger at the University of Manchester}}{197}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 11.6}{\ignorespaces Sketch of a cassette in the test tank.}}{199}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 11.7}{\ignorespaces Results of the $x$-axis scan of the cassette.}}{200}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 11.8}{\ignorespaces Scan of the Geiger plateau.}}{202}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 11.9}{\ignorespaces Efficiency of the avalanche propagation of six cells in a cassette.}}{203}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 12.1}{\ignorespaces Flow diagram for the Demonstrator's gas mixing system.}}{207}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 12.2}{\ignorespaces Diagram of a tentative gas outlet system of the Demonstrator.}}{208}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 12.3}{\ignorespaces Fractional volumes of ethanol obtained at equilibrium under controlled temperature and pressure conditions.}}{210}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 12.4}{\ignorespaces Photograph of the Demonstrator's gas mixing system.}}{211}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 12.5}{\ignorespaces Ethanol volume fraction in the gas measured by the depletion rate of liquid ethanol.}}{213}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 2.1}{\ignorespaces Measurement of the hadronic cross-section at the Z mass pole by the LEP experiments.}}{30}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 2.2}{\ignorespaces Standard Model vertices involving neutrinos.}}{31}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 2.3}{\ignorespaces Neutrino interactions with matter: charged and neutral current scattering off nucleons or atomic electrons.}}{32}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 2.4}{\ignorespaces Normal and inverted scenarios for the neutrino mass spectrum.}}{38}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 2.5}{\ignorespaces Electron energy spectrum in the $\beta $ decay of tritium}}{42}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 2.6}{\ignorespaces Effective \0nu   mass against smallest neutrino mass.}}{45}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 3.1}{\ignorespaces Masses of even $A$ nuclei, as described by the SEMF.}}{49}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 3.2}{\ignorespaces Feynman diagram for the two-neutrino double-$\beta $ decay.}}{50}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 3.3}{\ignorespaces Majorana mass term due to \0nu  .}}{52}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 3.4}{\ignorespaces Feynman diagram for the mass mechanism \0nu  .}}{53}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 3.5}{\ignorespaces Summed energy spectrum of double-$\beta $ decay.}}{54}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 3.6}{\ignorespaces Feynman diagram for the $\mathinner {\delimiter "426830A {\lambda }\delimiter "526930B }$ mode of \0nu   arising from left-right symmetric models.}}{55}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 3.7}{\ignorespaces Comparison between the nuclear matrix elements for 11 isotopes calculated in five different frameworks.}}{59}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 3.8}{\ignorespaces Comparison between the \0nu   half-lives of 11 isotopes assuming an effective Majorana mass of $\mathinner {\delimiter "426830A {m_{\beta \beta }}\delimiter "526930B }=50$\meV  .}}{59}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 5.1}{\ignorespaces Cutaway drawing of the NEMO-3 detector.}}{76}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 5.2}{\ignorespaces Top-view diagram of the NEMO-3 detector.}}{76}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 5.3}{\ignorespaces Arrangement of the different sources in the NEMO-3 detector.}}{77}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 5.4}{\ignorespaces Map of the \Ca  {48} and neighbouring sources as seen in NEMO-3 data.}}{78}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 5.5}{\ignorespaces Structure of the \CaF2   source discs.}}{80}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 5.6}{\ignorespaces Photograph of the \Ca  {48} source.}}{80}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 5.7}{\ignorespaces Rotated source foil strips.}}{81}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 5.8}{\ignorespaces Plan view of the tracker in one NEMO-3 sector.}}{82}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 5.9}{\ignorespaces Schematic drawing of a NEMO-3 calorimeter module.}}{84}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 6.1}{\ignorespaces Example of a two-electron event.}}{96}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 6.2}{\ignorespaces Example of an event with a delayed $\alpha $ track.}}{97}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 6.3}{\ignorespaces Example of events with external origin.}}{98}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 6.4}{\ignorespaces Distributions of p-values for the internal time of flight hypothesis.}}{101}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 6.5}{\ignorespaces Distributions of p-values for the external time of flight hypothesis.}}{103}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 6.6}{\ignorespaces Example of NLLR distributions.}}{106}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 7.10}{\ignorespaces Distribution of the cosine of the opening angle between the electron and the $\gamma $ in 1e1$\gamma $ (internal) events.}}{131}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 7.11}{\ignorespaces Distribution of the summed energy of the electron and the $\gamma $ in 1e1$\gamma $ (internal) events.}}{132}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 7.12}{\ignorespaces Distribution of the summed energy of the electron and the two $\gamma $'s in 1e2$\gamma $ events.}}{132}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 7.13}{\ignorespaces Distribution of the length of the delayed $\alpha $ track in 1e1$\alpha $ events.}}{134}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 7.14}{\ignorespaces Distribution of the summed energy of the electron and the $\gamma $ in 1e1$\gamma $ (external) events.}}{134}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 7.15}{\ignorespaces Summed calorimeter energy distribution in crossing electron events.}}{135}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 7.16}{\ignorespaces Comparison between measured internal background activities and HPGe measurements.}}{136}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 7.1}{\ignorespaces Internal background production processes.}}{111}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 7.2}{\ignorespaces External background production processes.}}{113}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 7.3}{\ignorespaces Thorium decay chain.}}{115}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 7.4}{\ignorespaces Uranium decay chain.}}{116}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 7.5}{\ignorespaces Radon level in the NEMO-3 detector.}}{118}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 7.6}{\ignorespaces Exponential decay of \Sr  {90}/\Y  {90} in the \Ca  {48} source.}}{121}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 7.7}{\ignorespaces Abnormal calorimeter activity leading to the exclusion of runs 8779 -- 8786.}}{127}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 7.8}{\ignorespaces Distributions of total energy in 1eN$\gamma $ and 1e1$\gamma $ (internal) events for three energy thresholds.}}{128}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 7.9}{\ignorespaces Electron energy distribution in 1eN$\gamma $ events.}}{130}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 8.10}{\ignorespaces Comparison of the $\mathinner {\delimiter "426830A {m_{\beta \beta }}\delimiter "526930B }$ limit measured in this analysis to the best available limits with \Ca  {48}, \Mo  {100} and \Xe  {136}.}}{158}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 8.1}{\ignorespaces Mis-modelling of the distance between electron vertices in ``same side'' events and difference in efficiency of the vertex distance cuts in data and MC.}}{141}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 8.2}{\ignorespaces Distribution of events against the run number.}}{143}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 8.3}{\ignorespaces Distributions of geometrical variables in 2eN$\gamma $ events.}}{144}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 8.4}{\ignorespaces Distributions of kinematic variables in 2eN$\gamma $ events.}}{145}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 8.5}{\ignorespaces Distribution of the summed electron energies in 2eN$\gamma $ events.}}{146}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 8.6}{\ignorespaces Optimisation of the \2nu   fitting window.}}{150}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 8.7}{\ignorespaces Comparison of the measured $\left | M^{2\nu }\right |$ to theoretical calculations.}}{152}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 8.8}{\ignorespaces 2eN$\gamma $ summed electron distribution with \0nu   limits.}}{155}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 8.9}{\ignorespaces 2eN$\gamma $ background subtracted data with \0nu   limits.}}{155}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 9.1}{\ignorespaces Exploded view drawing of the SuperNEMO Demonstrator module.}}{167}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 9.2}{\ignorespaces Construction of the SuperNEMO main wall calorimeter modules.}}{168}}
\@writefile{lof}{\contentsline {figure}{\numberline {\relax 9.3}{\ignorespaces Sensitivity of the SuperNEMO experiment as a function of exposure.}}{172}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(a)}{\ignorespaces {$\beta $-decay and M{\o }ller Scattering}}}{111}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(a)}{\ignorespaces {1e1$\alpha $ Phase 1}}}{134}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(a)}{\ignorespaces {1e1$\gamma $ (external) Phase 1}}}{134}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(a)}{\ignorespaces {1e1$\gamma $ (internal) Phase 1}}}{132}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(a)}{\ignorespaces {1e2$\gamma $ Phase 1}}}{132}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(a)}{\ignorespaces {1eN$\gamma $ 200\keV threshold}}}{128}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(a)}{\ignorespaces {1eN$\gamma $ Phase 1}}}{130}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(a)}{\ignorespaces {CC $\nu $-$e^-$ Scattering}}}{32}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(a)}{\ignorespaces {Charged current vertex.}}}{31}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(a)}{\ignorespaces {Commissioning DAQ}}}{202}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(a)}{\ignorespaces {Crossing electron Phase 1}}}{135}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(a)}{\ignorespaces {Crossing electron}}}{103}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(a)}{\ignorespaces {Crossing electron}}}{98}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(a)}{\ignorespaces {Pair Production}}}{113}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(a)}{\ignorespaces {Phase 1}}}{146}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(a)}{\ignorespaces {Position $A$}}}{200}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(a)}{\ignorespaces {Read-out time}}}{196}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(a)}{\ignorespaces {Two-electron}}}{101}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(a)}{\ignorespaces {}}}{127}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(a)}{\ignorespaces {}}}{141}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(a)}{\ignorespaces {}}}{144}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(a)}{\ignorespaces {}}}{145}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(a)}{\ignorespaces {}}}{177}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(a)}{\ignorespaces {}}}{181}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(a)}{\ignorespaces {}}}{197}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(a)}{\ignorespaces {}}}{210}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(a)}{\ignorespaces {}}}{81}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(b)}{\ignorespaces {$\Delta t$ between consecutive read-out cycles}}}{196}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(b)}{\ignorespaces {$\beta $-decay and Internal Conversion}}}{111}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(b)}{\ignorespaces {1e1$\alpha $ Phase 2}}}{134}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(b)}{\ignorespaces {1e1$\gamma $ (external) Phase 2}}}{134}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(b)}{\ignorespaces {1e1$\gamma $ (internal) 200\keV threshold}}}{128}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(b)}{\ignorespaces {1e1$\gamma $ (internal) Phase 2}}}{132}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(b)}{\ignorespaces {1e2$\gamma $ Phase 2}}}{132}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(b)}{\ignorespaces {1eN$\gamma $ Phase 2}}}{130}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(b)}{\ignorespaces {Cassette testing DAQ\cite {manchester-data}}}}{202}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(b)}{\ignorespaces {Compton + M{\o }ller Scattering}}}{113}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(b)}{\ignorespaces {Crossing electron Phase 2}}}{135}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(b)}{\ignorespaces {Event rate for events where one $\gamma $ hits the calorimeter hot spot in sectors 9 and 10.}}}{127}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(b)}{\ignorespaces {External electron and $\gamma $}}}{98}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(b)}{\ignorespaces {NC $\nu $-$e^-$ Scattering}}}{32}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(b)}{\ignorespaces {Neutral current vertex.}}}{31}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(b)}{\ignorespaces {One electron and one $\gamma $}}}{101}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(b)}{\ignorespaces {One electron and one $\gamma $}}}{103}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(b)}{\ignorespaces {Phase 2}}}{146}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(b)}{\ignorespaces {Position $B$}}}{200}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(b)}{\ignorespaces {}}}{141}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(b)}{\ignorespaces {}}}{144}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(b)}{\ignorespaces {}}}{145}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(b)}{\ignorespaces {}}}{177}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(b)}{\ignorespaces {}}}{181}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(b)}{\ignorespaces {}}}{197}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(b)}{\ignorespaces {}}}{210}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(b)}{\ignorespaces {}}}{81}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(c)}{\ignorespaces {$\beta $-decay and Compton Scattering}}}{111}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(c)}{\ignorespaces {1eN$\gamma $ 300\keV threshold}}}{128}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(c)}{\ignorespaces {CC $\nu $-$N$ Scattering}}}{32}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(c)}{\ignorespaces {Double Compton Scattering}}}{113}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(c)}{\ignorespaces {Position $C$}}}{200}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(d)}{\ignorespaces {1e1$\gamma $ (internal) 300\keV threshold}}}{128}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(d)}{\ignorespaces {NC $\nu $-$N$ Scattering}}}{32}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(d)}{\ignorespaces {Position $D$}}}{200}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(e)}{\ignorespaces {1eN$\gamma $ 400\keV threshold}}}{128}}
\@writefile{lof}{\contentsline {subfigure}{\numberline{(f)}{\ignorespaces {1e1$\gamma $ (internal) 400\keV threshold}}}{128}}
\@writefile{lof}{\select@language{english}}
\@writefile{lof}{\select@language{english}}
\@writefile{lot}{\addvspace {10\p@ }}
\@writefile{lot}{\addvspace {10\p@ }}
\@writefile{lot}{\addvspace {10\p@ }}
\@writefile{lot}{\addvspace {10\p@ }}
\@writefile{lot}{\addvspace {10\p@ }}
\@writefile{lot}{\addvspace {10\p@ }}
\@writefile{lot}{\addvspace {10\p@ }}
\@writefile{lot}{\addvspace {10\p@ }}
\@writefile{lot}{\addvspace {10\p@ }}
\@writefile{lot}{\addvspace {10\p@ }}
\@writefile{lot}{\addvspace {10\p@ }}
\@writefile{lot}{\addvspace {10\p@ }}
\@writefile{lot}{\addvspace {10\p@ }}
\@writefile{lot}{\contentsline {table}{\numberline {\relax 2.1}{\ignorespaces Best fit oscillation parameters assuming normal hierarchy.}}{37}}
\@writefile{lot}{\contentsline {table}{\numberline {\relax 2.2}{\ignorespaces Current limits on neutrino mass.}}{44}}
\@writefile{lot}{\contentsline {table}{\numberline {\relax 4.1}{\ignorespaces $Q_{\beta \beta }$ and natural abundances for isotopes commonly considered for double-$\beta $ decay experiments.}}{63}}
\@writefile{lot}{\contentsline {table}{\numberline {\relax 4.2}{\ignorespaces Most precise direct measurements of \2nu  .}}{72}}
\@writefile{lot}{\contentsline {table}{\numberline {\relax 4.3}{\ignorespaces Best limits on the \0nu   half-life of the isotopes for which \2nu   has been directly observed.}}{72}}
\@writefile{lot}{\contentsline {table}{\numberline {\relax 6.1}{\ignorespaces Exposure of the NEMO-3 \emph  {standard} analysis run set.}}{108}}
\@writefile{lot}{\contentsline {table}{\numberline {\relax 7.1}{\ignorespaces NEMO-3 external background model.}}{122}}
\@writefile{lot}{\contentsline {table}{\numberline {\relax 7.2}{\ignorespaces HPGe measurement of internal backgrounds in the \Ca  {48} source.}}{123}}
\@writefile{lot}{\contentsline {table}{\numberline {\relax 7.3}{\ignorespaces Analysis channels used in the background measurement.}}{124}}
\@writefile{lot}{\contentsline {table}{\numberline {\relax 7.4}{\ignorespaces Overview of the parameters used in the global fit.}}{125}}
\@writefile{lot}{\contentsline {table}{\numberline {\relax 7.5}{\ignorespaces Results of internal background measurement.}}{136}}
\@writefile{lot}{\contentsline {table}{\numberline {\relax 7.6}{\ignorespaces Results of external background measurement.}}{137}}
\@writefile{lot}{\contentsline {table}{\numberline {\relax 7.7}{\ignorespaces Results of radon background measurement.}}{138}}
\@writefile{lot}{\contentsline {table}{\numberline {\relax 8.1}{\ignorespaces Measurement of the activity of \Bi  {207} sources with NEMO-3 and HPGe detector data.}}{142}}
\@writefile{lot}{\contentsline {table}{\numberline {\relax 8.2}{\ignorespaces Activity of the \Sr  {90}/\Y  {90} system measured in the global fit and in the 2eN$\gamma $ channel.}}{145}}
\@writefile{lot}{\contentsline {table}{\numberline {\relax 8.3}{\ignorespaces Systematic uncertainty on the \2nu   measurement due to data mis-modelling in ``same side'' and ``opposite side'' events.}}{149}}
\@writefile{lot}{\contentsline {table}{\numberline {\relax 8.4}{\ignorespaces Total systematic uncertainty on the \2nu   half-life measurement.}}{150}}
\@writefile{lot}{\contentsline {table}{\numberline {\relax 8.5}{\ignorespaces Systematic uncertainties used in \0nu   limits.}}{153}}
\@writefile{lot}{\contentsline {table}{\numberline {\relax 8.6}{\ignorespaces Limits on \0nu   modes.}}{154}}
\@writefile{lot}{\contentsline {table}{\numberline {\relax 9.1}{\ignorespaces Experimental parameters and $0\nu \beta \beta $ sensitivity for the SuperNEMO experiment and its Demonstrator module.}}{166}}
\@writefile{lot}{\select@language{english}}
\@writefile{lot}{\select@language{english}}
\@writefile{toc}{\contentsline {chapter}{Bibliography}{219}}
\@writefile{toc}{\contentsline {chapter}{List of Figures}{15}}
\@writefile{toc}{\contentsline {chapter}{List of Tables}{19}}
\@writefile{toc}{\contentsline {chapter}{\numberline {10}The SuperNEMO tracker}{173}}
\@writefile{toc}{\contentsline {chapter}{\numberline {11}Electronics for surface commissioning}{187}}
\@writefile{toc}{\contentsline {chapter}{\numberline {12}Gas mixing system for the Demonstrator module}{205}}
\@writefile{toc}{\contentsline {chapter}{\numberline {13}Conclusion}{215}}
\@writefile{toc}{\contentsline {chapter}{\numberline {1}Introduction}{23}}
\@writefile{toc}{\contentsline {chapter}{\numberline {2}Neutrino Phenomenology}{27}}
\@writefile{toc}{\contentsline {chapter}{\numberline {3}Double-$\beta $ decay}{47}}
\@writefile{toc}{\contentsline {chapter}{\numberline {4}Double-$\beta $ decay experiments and techniques}{61}}
\@writefile{toc}{\contentsline {chapter}{\numberline {5}The NEMO-3 detector}{75}}
\@writefile{toc}{\contentsline {chapter}{\numberline {6}General analysis techniques}{93}}
\@writefile{toc}{\contentsline {chapter}{\numberline {7}Measurement of backgrounds in the \Ca  {48} source}{109}}
\@writefile{toc}{\contentsline {chapter}{\numberline {8}\Ca  {48} double-$\beta $ decay results}{139}}
\@writefile{toc}{\contentsline {chapter}{\numberline {9}The SuperNEMO experiment}{165}}
\@writefile{toc}{\contentsline {part}{\numberline {II}Tracker commissioning for the SuperNEMO detector}{163}}
\@writefile{toc}{\contentsline {part}{\numberline {I}Search for double-$\beta $ decay of $^{48}$Ca with the NEMO-3 detector}{73}}
\@writefile{toc}{\contentsline {section}{\numberline {1.1}Author's contributions}{24}}
\@writefile{toc}{\contentsline {section}{\numberline {10.1}The physics of Geiger counters}{174}}
\@writefile{toc}{\contentsline {section}{\numberline {10.2}The SuperNEMO tracker cells}{176}}
\@writefile{toc}{\contentsline {section}{\numberline {10.3}The tracker frame}{180}}
\@writefile{toc}{\contentsline {section}{\numberline {10.4}Construction of the tracker}{182}}
\@writefile{toc}{\contentsline {section}{\numberline {10.5}Tracker electronics and DAQ}{183}}
\@writefile{toc}{\contentsline {section}{\numberline {11.1}HV supply and distribution}{188}}
\@writefile{toc}{\contentsline {section}{\numberline {11.2}Cosmic ray trigger}{191}}
\@writefile{toc}{\contentsline {section}{\numberline {11.3}DAQ cards}{192}}
\@writefile{toc}{\contentsline {section}{\numberline {11.4}VME controller and read-out cycle}{195}}
\@writefile{toc}{\contentsline {section}{\numberline {11.5}Testing the tracker commissioning DAQ system on the cassette test tank}{198}}
\@writefile{toc}{\contentsline {section}{\numberline {12.1}Principle of operation}{205}}
\@writefile{toc}{\contentsline {section}{\numberline {12.2}Materials}{210}}
\@writefile{toc}{\contentsline {section}{\numberline {12.3}Tests}{212}}
\@writefile{toc}{\contentsline {section}{\numberline {12.4}Flow rate and \Rn  {222} activity in the tracker}{213}}
\@writefile{toc}{\contentsline {section}{\numberline {2.1}Historical overview}{27}}
\@writefile{toc}{\contentsline {section}{\numberline {2.2}Neutrinos in the SM}{30}}
\@writefile{toc}{\contentsline {section}{\numberline {2.3}Neutrino oscillations}{31}}
\@writefile{toc}{\contentsline {section}{\numberline {2.4}Neutrino Mass}{37}}
\@writefile{toc}{\contentsline {section}{\numberline {2.5}Constraints on neutrino mass}{41}}
\@writefile{toc}{\contentsline {section}{\numberline {2.6}Summary and outlook}{44}}
\@writefile{toc}{\contentsline {section}{\numberline {3.1}$\beta $ decay}{47}}
\@writefile{toc}{\contentsline {section}{\numberline {3.2}Two-neutrino double-$\beta $ decay}{50}}
\@writefile{toc}{\contentsline {section}{\numberline {3.3}Neutrinoless double-$\beta $ decay}{52}}
\@writefile{toc}{\contentsline {section}{\numberline {3.4}Nuclear matrix elements}{56}}
\@writefile{toc}{\contentsline {section}{\numberline {4.1}Detector technologies}{65}}
\@writefile{toc}{\contentsline {section}{\numberline {4.2}Experiments with \Ca  {48}}{69}}
\@writefile{toc}{\contentsline {section}{\numberline {4.3}Current status of double-$\beta $ decay measurements}{71}}
\@writefile{toc}{\contentsline {section}{\numberline {5.1}Source foils}{75}}
\@writefile{toc}{\contentsline {section}{\numberline {5.2}Tracker}{81}}
\@writefile{toc}{\contentsline {section}{\numberline {5.3}Calorimeter}{84}}
\@writefile{toc}{\contentsline {section}{\numberline {5.4}Electronics, DAQ and trigger}{85}}
\@writefile{toc}{\contentsline {section}{\numberline {5.5}Energy and time calibration}{88}}
\@writefile{toc}{\contentsline {section}{\numberline {5.6}Magnetic field}{90}}
\@writefile{toc}{\contentsline {section}{\numberline {5.7}Passive shielding}{90}}
\@writefile{toc}{\contentsline {section}{\numberline {5.8}Anti-radon facility}{90}}
\@writefile{toc}{\contentsline {section}{\numberline {6.1}Monte Carlo simulation}{93}}
\@writefile{toc}{\contentsline {section}{\numberline {6.2}Event reconstruction}{94}}
\@writefile{toc}{\contentsline {section}{\numberline {6.3}Particle identification}{95}}
\@writefile{toc}{\contentsline {section}{\numberline {6.4}Time of flight}{97}}
\@writefile{toc}{\contentsline {section}{\numberline {6.5}Statistical analysis}{102}}
\@writefile{toc}{\contentsline {section}{\numberline {6.6}Half-life calculation}{107}}
\@writefile{toc}{\contentsline {section}{\numberline {6.7}Analysis data set}{108}}
\@writefile{toc}{\contentsline {section}{\numberline {7.1}Background production processes}{109}}
\@writefile{toc}{\contentsline {section}{\numberline {7.2}Sources of background}{112}}
\@writefile{toc}{\contentsline {section}{\numberline {7.3}NEMO-3 background model}{121}}
\@writefile{toc}{\contentsline {section}{\numberline {7.4}HPGe measurements of internal backgrounds}{123}}
\@writefile{toc}{\contentsline {section}{\numberline {7.5}Background fitting procedure}{124}}
\@writefile{toc}{\contentsline {section}{\numberline {7.6}General event selection}{126}}
\@writefile{toc}{\contentsline {section}{\numberline {7.7}Background measurement channels}{129}}
\@writefile{toc}{\contentsline {section}{\numberline {7.8}Background measurement results}{135}}
\@writefile{toc}{\contentsline {section}{\numberline {8.1}2eN$\gamma $ event selection}{139}}
\@writefile{toc}{\contentsline {section}{\numberline {8.2}2eN$\gamma $ distributions}{142}}
\@writefile{toc}{\contentsline {section}{\numberline {8.3}Systematic uncertainty on the \2nu   measurement}{145}}
\@writefile{toc}{\contentsline {section}{\numberline {8.4}\2nu   half-life result and discussion}{151}}
\@writefile{toc}{\contentsline {section}{\numberline {8.5}Limits on \0nu   and discussion of results}{153}}
\@writefile{toc}{\contentsline {section}{\numberline {9.1}The Demonstrator module}{166}}
\@writefile{toc}{\contentsline {section}{\numberline {9.2}Research and development}{168}}
\@writefile{toc}{\contentsline {section}{\numberline {9.3}Timescale and sensitivity}{171}}
\@writefile{toc}{\contentsline {subsection}{\numberline {11.4.1}Read-out cycle duration and dead time}{196}}
\@writefile{toc}{\contentsline {subsection}{\numberline {12.1.1}Two-stage ethanol vapour addition}{208}}
\@writefile{toc}{\contentsline {subsection}{\numberline {2.1.1}Radioactive decay}{27}}
\@writefile{toc}{\contentsline {subsection}{\numberline {2.1.2}Proposal and observation of the neutrino}{28}}
\@writefile{toc}{\contentsline {subsection}{\numberline {2.1.3}Parity violation in weak interactions}{28}}
\@writefile{toc}{\contentsline {subsection}{\numberline {2.1.4}Neutrino flavour}{29}}
\@writefile{toc}{\contentsline {subsection}{\numberline {2.3.1}The solar neutrino anomaly}{33}}
\@writefile{toc}{\contentsline {subsection}{\numberline {2.3.2}Neutrino mixing}{34}}
\@writefile{toc}{\contentsline {subsection}{\numberline {2.3.3}Oscillations in matter}{35}}
\@writefile{toc}{\contentsline {subsection}{\numberline {2.3.4}Oscillation parameters}{36}}
\@writefile{toc}{\contentsline {subsection}{\numberline {2.3.5}Mass hierarchy}{37}}
\@writefile{toc}{\contentsline {subsection}{\numberline {2.4.1}Dirac mass term}{38}}
\@writefile{toc}{\contentsline {subsection}{\numberline {2.4.2}Majorana mass term}{39}}
\@writefile{toc}{\contentsline {subsection}{\numberline {2.4.3}See-saw mechanism}{40}}
\@writefile{toc}{\contentsline {subsection}{\numberline {2.5.1}Beta decay of tritium}{41}}
\@writefile{toc}{\contentsline {subsection}{\numberline {2.5.2}Cosmology}{43}}
\@writefile{toc}{\contentsline {subsection}{\numberline {2.5.3}Neutrinoless double-$\beta $ decay}{43}}
\@writefile{toc}{\contentsline {subsection}{\numberline {3.1.1}The semi-empirical mass formula}{48}}
\@writefile{toc}{\contentsline {subsection}{\numberline {3.3.1}Mass mechanism}{53}}
\@writefile{toc}{\contentsline {subsection}{\numberline {3.3.2}${\mathchoice {{\setbox \z@ \hbox {$\mathsurround \z@ \displaystyle R$}\setbox \tw@ \hbox {$\mathsurround \z@ \displaystyle /$}\dimen 4\wd \z@ \dimen@ \ht \tw@ \advance \dimen@ -\dp \tw@ \advance \dimen@ -\ht \z@ \advance \dimen@ \dp \z@ \divide \dimen@ \tw@ \advance \dimen@ -0\ht \tw@ \advance \dimen@ -0\dp \tw@ \dimen@ii 0\wd \z@ \raise -\dimen@ \hbox to\dimen 4{\hss \kern \dimen@ii \box \tw@ \kern -\dimen@ii \hss }\hbox to\z@ {\hss \hbox to\dimen 4{\hss \box \z@ \hss }}}}{{\setbox \z@ \hbox {$\mathsurround \z@ \textstyle R$}\setbox \tw@ \hbox {$\mathsurround \z@ \textstyle /$}\dimen 4\wd \z@ \dimen@ \ht \tw@ \advance \dimen@ -\dp \tw@ \advance \dimen@ -\ht \z@ \advance \dimen@ \dp \z@ \divide \dimen@ \tw@ \advance \dimen@ -0\ht \tw@ \advance \dimen@ -0\dp \tw@ \dimen@ii 0\wd \z@ \raise -\dimen@ \hbox to\dimen 4{\hss \kern \dimen@ii \box \tw@ \kern -\dimen@ii \hss }\hbox to\z@ {\hss \hbox to\dimen 4{\hss \box \z@ \hss }}}}{{\setbox \z@ \hbox {$\mathsurround \z@ \scriptstyle R$}\setbox \tw@ \hbox {$\mathsurround \z@ \scriptstyle /$}\dimen 4\wd \z@ \dimen@ \ht \tw@ \advance \dimen@ -\dp \tw@ \advance \dimen@ -\ht \z@ \advance \dimen@ \dp \z@ \divide \dimen@ \tw@ \advance \dimen@ -0\ht \tw@ \advance \dimen@ -0\dp \tw@ \dimen@ii 0\wd \z@ \raise -\dimen@ \hbox to\dimen 4{\hss \kern \dimen@ii \box \tw@ \kern -\dimen@ii \hss }\hbox to\z@ {\hss \hbox to\dimen 4{\hss \box \z@ \hss }}}}{{\setbox \z@ \hbox {$\mathsurround \z@ \scriptscriptstyle R$}\setbox \tw@ \hbox {$\mathsurround \z@ \scriptscriptstyle /$}\dimen 4\wd \z@ \dimen@ \ht \tw@ \advance \dimen@ -\dp \tw@ \advance \dimen@ -\ht \z@ \advance \dimen@ \dp \z@ \divide \dimen@ \tw@ \advance \dimen@ -0\ht \tw@ \advance \dimen@ -0\dp \tw@ \dimen@ii 0\wd \z@ \raise -\dimen@ \hbox to\dimen 4{\hss \kern \dimen@ii \box \tw@ \kern -\dimen@ii \hss }\hbox to\z@ {\hss \hbox to\dimen 4{\hss \box \z@ \hss }}}}}_p$ supersymmetry}{54}}
\@writefile{toc}{\contentsline {subsection}{\numberline {3.3.3}Right handed currents}{55}}
\@writefile{toc}{\contentsline {subsection}{\numberline {3.3.4}Majoron emission}{56}}
\@writefile{toc}{\contentsline {subsection}{\numberline {3.4.1}Comparison of calculations}{58}}
\@writefile{toc}{\contentsline {subsection}{\numberline {4.1.1}Semiconductor experiments}{65}}
\@writefile{toc}{\contentsline {subsection}{\numberline {4.1.2}Bolometer experiments}{67}}
\@writefile{toc}{\contentsline {subsection}{\numberline {4.1.3}Scintillator experiments}{67}}
\@writefile{toc}{\contentsline {subsection}{\numberline {4.1.4}Time projection chamber experiments}{68}}
\@writefile{toc}{\contentsline {subsection}{\numberline {4.1.5}Tracker and calorimeter experiments}{69}}
\@writefile{toc}{\contentsline {subsection}{\numberline {5.1.1}\Ca  {48} source}{79}}
\@writefile{toc}{\contentsline {subsection}{\numberline {5.4.1}Calorimeter electronics}{86}}
\@writefile{toc}{\contentsline {subsection}{\numberline {5.4.2}Tracker electronics}{86}}
\@writefile{toc}{\contentsline {subsection}{\numberline {5.4.3}Trigger system}{87}}
\@writefile{toc}{\contentsline {subsection}{\numberline {5.5.1}Radioactive sources}{88}}
\@writefile{toc}{\contentsline {subsection}{\numberline {5.5.2}Laser calibration system}{89}}
\@writefile{toc}{\contentsline {subsection}{\numberline {6.3.1}Electrons}{95}}
\@writefile{toc}{\contentsline {subsection}{\numberline {6.3.2}Photons}{95}}
\@writefile{toc}{\contentsline {subsection}{\numberline {6.3.3}Alpha emission from \Bi  {214} - \Po  {214} decays}{96}}
\@writefile{toc}{\contentsline {subsection}{\numberline {6.4.1}Internal hypothesis}{98}}
\@writefile{toc}{\contentsline {subsection}{\numberline {6.4.2}External hypothesis}{100}}
\@writefile{toc}{\contentsline {subsection}{\numberline {6.5.1}Fitting MC predictions to data}{103}}
\@writefile{toc}{\contentsline {subsection}{\numberline {6.5.2}Limit setting}{105}}
\@writefile{toc}{\contentsline {subsection}{\numberline {7.1.1}Background classification}{110}}
\@writefile{toc}{\contentsline {subsection}{\numberline {7.2.1}Decay chains}{114}}
\@writefile{toc}{\contentsline {subsection}{\numberline {7.2.2}\Rn  {222}}{117}}
\@writefile{toc}{\contentsline {subsection}{\numberline {7.2.3}Other natural background sources}{119}}
\@writefile{toc}{\contentsline {subsection}{\numberline {7.2.4}Artificial contaminants in source foil}{119}}
\@writefile{toc}{\contentsline {subsection}{\numberline {7.2.5}\2nu   as a background to \0nu  }{120}}
\@writefile{toc}{\contentsline {subsection}{\numberline {7.2.6}Leakage}{121}}
\@writefile{toc}{\contentsline {subsection}{\numberline {7.6.1}Hot runs}{126}}
\@writefile{toc}{\contentsline {subsection}{\numberline {7.6.2}Energy threshold}{126}}
\@writefile{toc}{\contentsline {subsection}{\numberline {7.7.1}1eN$\gamma $}{129}}
\@writefile{toc}{\contentsline {subsection}{\numberline {7.7.2}1e1$\gamma $ (internal)}{130}}
\@writefile{toc}{\contentsline {subsection}{\numberline {7.7.3}1e2$\gamma $}{131}}
\@writefile{toc}{\contentsline {subsection}{\numberline {7.7.4}1e1$\alpha $}{133}}
\@writefile{toc}{\contentsline {subsection}{\numberline {7.7.5}External background measurement channels}{133}}
\@writefile{toc}{\contentsline {subsection}{\numberline {8.1.1}Validation of 2eN$\gamma $ selection on \Bi  {207} calibration source data}{141}}
\@writefile{toc}{\contentsline {subsection}{\numberline {8.3.1}Optimisation of \2nu   fitting window}{149}}
\@writefile{toc}{\contentsline {subsubsection}{$\gamma $'s}{99}}
\@writefile{toc}{\contentsline {subsubsection}{$x$-axis scan}{199}}
\@writefile{toc}{\contentsline {subsubsection}{${\mathchoice {{\setbox \z@ \hbox {$\mathsurround \z@ \displaystyle R$}\setbox \tw@ \hbox {$\mathsurround \z@ \displaystyle /$}\dimen 4\wd \z@ \dimen@ \ht \tw@ \advance \dimen@ -\dp \tw@ \advance \dimen@ -\ht \z@ \advance \dimen@ \dp \z@ \divide \dimen@ \tw@ \advance \dimen@ -0\ht \tw@ \advance \dimen@ -0\dp \tw@ \dimen@ii 0\wd \z@ \raise -\dimen@ \hbox to\dimen 4{\hss \kern \dimen@ii \box \tw@ \kern -\dimen@ii \hss }\hbox to\z@ {\hss \hbox to\dimen 4{\hss \box \z@ \hss }}}}{{\setbox \z@ \hbox {$\mathsurround \z@ \textstyle R$}\setbox \tw@ \hbox {$\mathsurround \z@ \textstyle /$}\dimen 4\wd \z@ \dimen@ \ht \tw@ \advance \dimen@ -\dp \tw@ \advance \dimen@ -\ht \z@ \advance \dimen@ \dp \z@ \divide \dimen@ \tw@ \advance \dimen@ -0\ht \tw@ \advance \dimen@ -0\dp \tw@ \dimen@ii 0\wd \z@ \raise -\dimen@ \hbox to\dimen 4{\hss \kern \dimen@ii \box \tw@ \kern -\dimen@ii \hss }\hbox to\z@ {\hss \hbox to\dimen 4{\hss \box \z@ \hss }}}}{{\setbox \z@ \hbox {$\mathsurround \z@ \scriptstyle R$}\setbox \tw@ \hbox {$\mathsurround \z@ \scriptstyle /$}\dimen 4\wd \z@ \dimen@ \ht \tw@ \advance \dimen@ -\dp \tw@ \advance \dimen@ -\ht \z@ \advance \dimen@ \dp \z@ \divide \dimen@ \tw@ \advance \dimen@ -0\ht \tw@ \advance \dimen@ -0\dp \tw@ \dimen@ii 0\wd \z@ \raise -\dimen@ \hbox to\dimen 4{\hss \kern \dimen@ii \box \tw@ \kern -\dimen@ii \hss }\hbox to\z@ {\hss \hbox to\dimen 4{\hss \box \z@ \hss }}}}{{\setbox \z@ \hbox {$\mathsurround \z@ \scriptscriptstyle R$}\setbox \tw@ \hbox {$\mathsurround \z@ \scriptscriptstyle /$}\dimen 4\wd \z@ \dimen@ \ht \tw@ \advance \dimen@ -\dp \tw@ \advance \dimen@ -\ht \z@ \advance \dimen@ \dp \z@ \divide \dimen@ \tw@ \advance \dimen@ -0\ht \tw@ \advance \dimen@ -0\dp \tw@ \dimen@ii 0\wd \z@ \raise -\dimen@ \hbox to\dimen 4{\hss \kern \dimen@ii \box \tw@ \kern -\dimen@ii \hss }\hbox to\z@ {\hss \hbox to\dimen 4{\hss \box \z@ \hss }}}}}_p$ supersymmetry}{157}}
\@writefile{toc}{\contentsline {subsubsection}{1e1$\gamma $ (external)}{133}}
\@writefile{toc}{\contentsline {subsubsection}{Beijing experiment}{70}}
\@writefile{toc}{\contentsline {subsubsection}{CANDLES}{71}}
\@writefile{toc}{\contentsline {subsubsection}{Calibration systems}{169}}
\@writefile{toc}{\contentsline {subsubsection}{Calorimeter}{169}}
\@writefile{toc}{\contentsline {subsubsection}{Cathode ring read-out}{201}}
\@writefile{toc}{\contentsline {subsubsection}{Crossing electron}{101}}
\@writefile{toc}{\contentsline {subsubsection}{Crossing electron}{135}}
\@writefile{toc}{\contentsline {subsubsection}{Discussion of results}{204}}
\@writefile{toc}{\contentsline {subsubsection}{ELEGANT VI}{71}}
\@writefile{toc}{\contentsline {subsubsection}{Electrons}{99}}
\@writefile{toc}{\contentsline {subsubsection}{External backgrounds}{112}}
\@writefile{toc}{\contentsline {subsubsection}{External backgrounds}{137}}
\@writefile{toc}{\contentsline {subsubsection}{External electron and $\gamma $}{102}}
\@writefile{toc}{\contentsline {subsubsection}{Geiger plateau scan}{199}}
\@writefile{toc}{\contentsline {subsubsection}{Hoover Dam experiment}{70}}
\@writefile{toc}{\contentsline {subsubsection}{Internal backgrounds}{110}}
\@writefile{toc}{\contentsline {subsubsection}{Internal backgrounds}{136}}
\@writefile{toc}{\contentsline {subsubsection}{Majoron emission}{161}}
\@writefile{toc}{\contentsline {subsubsection}{Mass mechanism}{154}}
\@writefile{toc}{\contentsline {subsubsection}{Mateosian-Goldhaber experiment}{70}}
\@writefile{toc}{\contentsline {subsubsection}{NEMO-3}{24}}
\@writefile{toc}{\contentsline {subsubsection}{Radon backgrounds}{112}}
\@writefile{toc}{\contentsline {subsubsection}{Radon backgrounds}{138}}
\@writefile{toc}{\contentsline {subsubsection}{Radon emanation}{170}}
\@writefile{toc}{\contentsline {subsubsection}{Right-handed currents}{159}}
\@writefile{toc}{\contentsline {subsubsection}{Rotation of the \Ca  {48} source from the nominal position}{79}}
\@writefile{toc}{\contentsline {subsubsection}{Source}{168}}
\@writefile{toc}{\contentsline {subsubsection}{SuperNEMO}{25}}
\@writefile{toc}{\contentsline {subsubsection}{TGV}{70}}
\@writefile{toc}{\contentsline {subsubsection}{Tracker}{170}}
\@writefile{toc}{\contentsline {subsubsection}{\Sr  {90} in the \Ca  {48} source}{120}}
\@writefile{toc}{\select@language{english}}
\@writefile{toc}{\select@language{english}}
\bibdata{bibliography/thesis}
\bibstyle{bibliography/bibstyles/atlasnote}
\catcode `"\active 
\citation{0034-4885-75-10-106301}
\citation{0034-4885-75-10-106301}
\citation{0034-4885-75-10-106301}
\citation{0034-4885-75-10-106301}
\citation{0034-4885-75-10-106301}
\citation{0034-4885-75-10-106301}
\citation{0034-4885-75-10-106301}
\citation{0034-4885-75-10-106301}
\citation{0034-4885-75-10-106301}
\citation{1742-6596-375-4-042011}
\citation{1742-6596-447-1-012066}
\citation{1993JPhG...19..139S,PhysRevC.53.695}
\citation{Aalseth:2002dt}
\citation{Abgrall:2013rze}
\citation{Adamson2006119}
\citation{Agostini:2012nm}
\citation{Agostini:2012nm}
\citation{Agostini:2013mzu}
\citation{Albert:2013gpz}
\citation{Albert:2013gpz}
\citation{Allanach:2014lca}
\citation{Allanach:2014lca}
\citation{Andreotti:2010vj}
\citation{Andreotti:2010vj}
\citation{Argyriades:2008pr}
\citation{Argyriades:2008pr}
\citation{Argyriades:2009ph}
\citation{Argyriades:2009ph}
\citation{Argyriades:2009vq}
\citation{Argyriades:2009vq}
\citation{Argyriades:2009vq}
\citation{Argyriades:2009vq}
\citation{Argyriades:2009vq}
\citation{Argyriades:2009vq}
\citation{Argyriades:2009vq}
\citation{Argyriades:2009vq}
\citation{Argyriades:2009vq}
\citation{Argyriades:2009vq}
\citation{Argyriades:2009vq}
\citation{Arnold:2004xq}
\citation{Arnold:2004xq}
\citation{Arnold:2005rz}
\citation{Arnold:2005rz}
\citation{Arnold:2011gq,motty}
\citation{Arnold:2011gq}
\citation{Arnold:2011gq}
\citation{Arnold:2013dha}
\citation{Arnold:2013dha}
\citation{Arnold:2013dha}
\citation{Arnold:2013dha}
\citation{Arnold:2013dha}
\citation{Arnold:2013dha}
\citation{Arnold:2013dha}
\citation{Arnold:2013dha}
\citation{Bamert199525,Rodejohann:2012xd}
\citation{Barbier:2004ez}
\citation{BarnabéHeider2012141,Shirai201328,Gomez-Cadenas:2013lta}
\citation{BarnabéHeider2012141}
\citation{Biller:2013wua}
\citation{Biller:2013wua}
\citation{Bowles:1997br}
\citation{Bowles:1997br}
\citation{Brown:1978pb}
\citation{Brudanin200063}
\citation{Brudanin200063}
\citation{Brudanin200063}
\citation{Brun:1987ma}
\citation{Caurier201262}
\citation{Chadwick:1914zz}
\citation{Collie}
\citation{Collie}
\citation{Cowan:1992xc}
\citation{Cowan:2013pha}
\citation{Cowan:2013pha}
\citation{Davis:1968cp}
\citation{Dev:2014xea}
\citation{Drexlin:2013lha,Wieslander:2009zz}
\citation{Gallex}
\citation{Giunti-Kim-2007}
\citation{GlenCowan}
\citation{GoeppertMayer:1935qp}
\citation{Goldhaber:1958nb}
\citation{Gomez-Cadenas:2013lta}
\citation{Helene:1982pb}
\citation{Hinshaw:2012aka}
\citation{Hirsch1998224}
\citation{Kayser:2005cd}
\citation{Kayser:2005cd}
\citation{Kayser:2005cd}
\citation{KlapdorKleingrothaus:2000sn}
\citation{KlapdorKleingrothaus:2004wj}
\citation{Knöpfle201331}
\citation{Kragh:1493569}
\citation{Kuo1968241}
\citation{Kuo1968241}
\citation{LBNE,NOvA}
\citation{LBNE,NOvA}
\citation{LEP_Z_res}
\citation{LEP_Z_res}
\citation{LEP_Z_res}
\citation{LeeYang:1956}
\citation{Maki:1962mu,Pontecorvo:1967fh}
\citation{Maki:1962mu}
\citation{Maneschg2008448}
\citation{Minuit}
\citation{Muto19849}
\citation{Muto19849}
\citation{NIST}
\citation{NIST}
\citation{NIST}
\citation{NNDC}
\citation{Nishino:2012ipa}
\citation{Nucciotti:2010tx}
\citation{Ogawa:1989qz,Tsuboi1984293}
\citation{Ogawa:1989qz,Tsuboi1984293}
\citation{Ogawa:1989qz}
\citation{Osipowicz:2001sq}
\citation{PhysRev.146.810}
\citation{PhysRev.50.48}
\citation{PhysRevC.21.1568}
\citation{PhysRevC.31.1012}
\citation{PhysRevC.31.1012}
\citation{PhysRevC.42.1120,Suhonen1998123,RevModPhys.77.427}
\citation{PhysRevC.42.1120}
\citation{PhysRevC.42.1120}
\citation{PhysRevC.42.1120}
\citation{PhysRevC.65.061301}
\citation{PhysRevC.65.061301}
\citation{PhysRevC.68.035501}
\citation{PhysRevC.75.034303,Caurier201262}
\citation{PhysRevC.75.034303}
\citation{PhysRevC.78.058501}
\citation{PhysRevC.78.058501}
\citation{PhysRevC.78.058501}
\citation{PhysRevC.78.058501}
\citation{PhysRevC.85.034316}
\citation{PhysRevC.85.034316}
\citation{PhysRevC.86.021601}
\citation{PhysRevC.87.014315}
\citation{PhysRevC.87.014320,PhysRevD.60.115007}
\citation{PhysRevC.87.014320,PhysRevD.86.055006}
\citation{PhysRevC.87.014320,PhysRevLett.100.052503}
\citation{PhysRevC.87.014320,supernemoPheno}
\citation{PhysRevD.55.54}
\citation{PhysRevD.64.095005}
\citation{PhysRevD.80.051301}
\citation{PhysRevD.86.055006}
\citation{PhysRevLett.105.252503}
\citation{PhysRevLett.109.032505}
\citation{PhysRevLett.77.5186}
\citation{PhysRevLett.77.5186}
\citation{PhysRevLett.77.5186}
\citation{Ponkratenko:2000um}
\citation{Pontecorvo:1957cp}
\citation{ROOT}
\citation{RevModPhys.77.427}
\citation{RevModPhys.80.481}
\citation{RevModPhys.80.481}
\citation{RevModPhys.80.481}
\citation{Richter1991325}
\citation{Richter1991325}
\citation{Rodejohann:2012xd,Bamert199525}
\citation{Rodejohann:2012xd,supernemoPheno}
\citation{Rodejohann:2012xd}
\citation{Rodejohann:2012xd}
\citation{Rodejohann:2012xd}
\citation{Rodejohann:2012xd}
\citation{Rodejohann:2012xd}
\citation{Rodejohann:2012xd}
\citation{Rodejohann:2012xd}
\citation{SAGE}
\citation{SNO}
\citation{SchechterValle}
\citation{SchechterValle}
\citation{SchechterValle}
\citation{SchechterValle}
\citation{Schmidt:2013gdc}
\citation{Shirai201328}
\citation{Suhonen1998123,PhysRevC.42.1120}
\citation{Suhonen1998123}
\citation{Suhonen1998123}
\citation{Suhonen1998123}
\citation{Suhonen1998123}
\citation{SuperK}
\citation{TOF_note}
\citation{Thomas:2009ae,Hinshaw:2012aka}
\citation{Thomas:2009ae}
\citation{Tsuboi1984293}
\citation{Vavra20031}
\citation{Weizscker:1935zz}
\citation{Wilkinson1992195}
\citation{Wilkinson1996516,PhysRev.72.602}
\citation{Wilson2011313}
\citation{Wilson:2013iop}
\citation{Wilson:Fermi}
\citation{Wolfenstein_MSW,Mikheev:1986wj}
\citation{Wu:1957}
\citation{You199153}
\citation{Zech1989608,0954-3899-28-10-313}
\citation{Zheng1990343}
\citation{bipo}
\citation{busto}
\citation{decapua-flow}
\citation{doi:10.1146/annurev-nucl-102711-094904}
\citation{doi:10.1146/annurev-nucl-102711-094904}
\citation{doi:10.1146/annurev-nucl-102711-094904}
\citation{exo-ton}
\citation{geiger-disso,PhysRev.72.602}
\citation{geiger-disso}
\citation{igex}
\citation{kamland-zen,PhysRevLett.109.032505,Agostini:2013mzu}
\citation{kamland-zen}
\citation{kamland-zen}
\citation{kamland-zen}
\citation{kamland-zen}
\citation{kamland-zen}
\citation{kamland-zen}
\citation{lsnd}
\citation{mainz}
\citation{majorana}
\citation{manchester-data}
\citation{manchester-data}
\citation{manchester-data}
\citation{manu-dead-time}
\citation{manu-derived-sig}
\citation{manu-derived-sig}
\citation{manu-tracker-signal}
\citation{manu-tracker-signal}
\citation{manu-wire-test}
\citation{manu}
\citation{manu}
\citation{me-press}
\citation{mine-comm-elec}
\citation{miniboone}
\citation{motty}
\citation{motty}
\citation{motty}
\citation{motty}
\citation{motty}
\citation{motty}
\citation{motty}
\citation{motty}
\citation{motty}
\citation{motty}
\citation{motty}
\citation{motty}
\citation{motty}
\citation{motty}
\citation{motty}
\citation{munu_discovery}
\citation{muonconfirmation,tauevidence}
\citation{nutau_discovery}
\citation{pdg}
\citation{pdg}
\citation{pdg}
\citation{pdg}
\citation{perry-sn-hv}
\citation{piaHPGe}
\citation{piaHPGe}
\citation{piaHPGe}
\citation{piaHPGe}
\citation{post_erratum,RevModPhys.77.427}
\citation{post_erratum}
\citation{post_erratum}
\citation{pre_erratum}
\citation{rnDiffusion}
\citation{roger_angularDist}
\citation{samer}
\citation{sophieNd150}
\citation{supernemoPheno}
\citation{supernemoPheno}
\citation{troitsk,mainz}
\citation{troitsk}
\citation{victorDia1,victorDia2}
\citation{victor_rotation}
\citation{victor_rotation}
\citation{victor_ss_os,ste_ss_os}
\citation{wilks1938}
\global\@altsecnumformattrue
\newlabel{chap:conclusion}
\newlabel{eq:0nu_lambda_res_conc}
\newlabel{eq:0nu_lambda_res_conc}
\newlabel{eq:0nu_lambda_res}
\newlabel{eq:0nu_lambda_res}
\newlabel{eq:0nu_res_conc}
\newlabel{eq:0nu_res}
\newlabel{eq:NLLR}
\newlabel{eq:antoine}
\newlabel{eq:betaplus}
\newlabel{eq:binding}
\newlabel{eq:dirac_mass}
\newlabel{eq:e_beta}
\newlabel{eq:eff-maj-mass}
\newlabel{eq:exp_decay}
\newlabel{eq:gas-eq}
\newlabel{eq:gen_mass}
\newlabel{eq:generic-sens}
\newlabel{eq:likelihood}
\newlabel{eq:maj_conj}
\newlabel{eq:maj_mass}
\newlabel{eq:mass_eigenvals_limit}
\newlabel{eq:mass_eigenvals}
\newlabel{eq:mass_in_SM_states}
\newlabel{eq:mass_matrix}
\newlabel{eq:osc_prob}
\newlabel{eq:semf}
\newlabel{eq:tritium_decay}
\newlabel{eq:tritium_mass}
\newlabel{fig:0nu-lims-comp}
\newlabel{fig:0nu-lims-sub}
\newlabel{fig:0nu-lims}
\newlabel{fig:1e1a_Plots}
\newlabel{fig:1e1g_extProb}
\newlabel{fig:1e1g_ext_Plots}
\newlabel{fig:1e1g_intProb}
\newlabel{fig:1e1g_int_Plots}
\newlabel{fig:1e1g_int_angle}
\newlabel{fig:1e2g_int_Plots}
\newlabel{fig:1eNg_Plots}
\newlabel{fig:2e_cut_eff}
\newlabel{fig:2e_geom}
\newlabel{fig:2e_geom}
\newlabel{fig:2e_intProb}
\newlabel{fig:2e_runNumber}
\newlabel{fig:2e_trackL}
\newlabel{fig:2e_vtxZ}
\newlabel{fig:2e_vtx_disc}
\newlabel{fig:2nu-feyn}
\newlabel{fig:CompComp}
\newlabel{fig:CompMoll}
\newlabel{fig:IntCompton}
\newlabel{fig:IntConversion}
\newlabel{fig:IntMoller}
\newlabel{fig:NLLR}
\newlabel{fig:NemoTopViewDiag}
\newlabel{fig:PairProd}
\newlabel{fig:RadonLevel}
\newlabel{fig:ThDecayChain}
\newlabel{fig:UDecayChain}
\newlabel{fig:Y90_hl}
\newlabel{fig:antoine-vs-p}
\newlabel{fig:antoine-vs-t}
\newlabel{fig:bb-spectra}
\newlabel{fig:bbmass_smallestmass}
\newlabel{fig:black-box}
\newlabel{fig:calo_block}
\newlabel{fig:camembert}
\newlabel{fig:comm-elec-sketch}
\newlabel{fig:comm-plateau-scan}
\newlabel{fig:cosee}
\newlabel{fig:cu_rot}
\newlabel{fig:demo-explo}
\newlabel{fig:detector}
\newlabel{fig:disc_sketch}
\newlabel{fig:eth-frac}
\newlabel{fig:ex_1e1g_ext}
\newlabel{fig:ex_2e}
\newlabel{fig:ex_BiPo}
\newlabel{fig:ex_xe}
\newlabel{fig:extProb_dists}
\newlabel{fig:feyn-0nu-rhc-lambda}
\newlabel{fig:feyn-mass-mech}
\newlabel{fig:flow-diagram}
\newlabel{fig:flow-gas-out}
\newlabel{fig:gammaCaloOcc}
\newlabel{fig:gas-photo}
\newlabel{fig:geiger}
\newlabel{fig:hl-50mev}
\newlabel{fig:hotRuns}
\newlabel{fig:hv-sketch}
\newlabel{fig:int_gausConst}
\newlabel{fig:main-wall-calo}
\newlabel{fig:manchester-cath-eff}
\newlabel{fig:manchester-plateau-scan}
\newlabel{fig:manchester-prop-t}
\newlabel{fig:manchester-setup-photo}
\newlabel{fig:manchester-x-scan}
\newlabel{fig:nemo3-backgrounds:ExtBGs}
\newlabel{fig:nemo3-backgrounds:IntBGs}
\newlabel{fig:neutrino-theory:CC-Electron}
\newlabel{fig:neutrino-theory:CC-Nucleon}
\newlabel{fig:neutrino-theory:CC-vtx}
\newlabel{fig:neutrino-theory:LEP_Z_Res}
\newlabel{fig:neutrino-theory:MassHierarchy}
\newlabel{fig:neutrino-theory:NC-Electron}
\newlabel{fig:neutrino-theory:NC-Nucleon}
\newlabel{fig:neutrino-theory:NC-vtx}
\newlabel{fig:neutrino-theory:TritiumSpectrum}
\newlabel{fig:neutrino-theory:neutrino-interactions}
\newlabel{fig:nmes}
\newlabel{fig:nu_vtx}
\newlabel{fig:plateau-scan}
\newlabel{fig:read-out-rate}
\newlabel{fig:sector_tracker}
\newlabel{fig:semf-curves}
\newlabel{fig:singleElectronE}
\newlabel{fig:sn-c-section-sketch}
\newlabel{fig:sn-cell-insertion}
\newlabel{fig:sn-cell-sig}
\newlabel{fig:sn-cell-sketch}
\newlabel{fig:sn-der-an}
\newlabel{fig:sn-robot}
\newlabel{fig:sn-sens}
\newlabel{fig:source_in_data}
\newlabel{fig:source_photo}
\newlabel{fig:sumE_P1}
\newlabel{fig:sumE_P2}
\newlabel{fig:sumE}
\newlabel{fig:syst_scan}
\newlabel{fig:thr}
\newlabel{fig:time-vs-ncards}
\newlabel{fig:timing}
\newlabel{fig:twnu_nme_res}
\newlabel{fig:victor_rotation}
\newlabel{fig:x-scan-results}
\newlabel{fig:xe_Plots}
\newlabel{fig:xe_extProb}
\newlabel{oscillations}
\newlabel{sec:0nu-lims}
\newlabel{sec:0nu}
\newlabel{sec:2e-dists}
\newlabel{sec:2nu-disc}
\newlabel{sec:2nu_syst}
\newlabel{sec:HPGe_meas}
\newlabel{sec:ana_cls}
\newlabel{sec:ana_fit}
\newlabel{sec:ana_hl}
\newlabel{sec:analysis-tech}
\newlabel{sec:antirad}
\newlabel{sec:back_1e1a_chan}
\newlabel{sec:beta-decay}
\newlabel{sec:bkgd_meas}
\newlabel{sec:ch-elec}
\newlabel{sec:dbd-ca-48-exp}
\newlabel{sec:dbd-det-tech}
\newlabel{sec:dbd-exp}
\newlabel{sec:dbd-results-summary}
\newlabel{sec:dbd_results}
\newlabel{sec:dbd}
\newlabel{sec:det_ca_source}
\newlabel{sec:det_rad_calib}
\newlabel{sec:ext_model}
\newlabel{sec:fitting_proc}
\newlabel{sec:genCriteria}
\newlabel{sec:gen_selection}
\newlabel{sec:hotRuns}
\newlabel{sec:maj_mass_term}
\newlabel{sec:nemo-3-det}
\newlabel{sec:nme}
\newlabel{sec:nu-flavour}
\newlabel{sec:nu_mix}
\newlabel{sec:nupheno}
\newlabel{sec:nus_in_sm}
\newlabel{sec:osc_matter}
\newlabel{sec:pid_e}
\newlabel{sec:radon}
\newlabel{sec:res_calib_data}
\newlabel{sec:signal_selection}
\newlabel{sec:sn-demo}
\newlabel{sec:sn-det}
\newlabel{sec:sn-gas}
\newlabel{sec:sn-gg}
\newlabel{sec:sn-rd}
\newlabel{sec:sn-sn-cell}
\newlabel{sec:sn-timescale}
\newlabel{sec:sn-tracker-const}
\newlabel{sec:sn-tracker-elec}
\newlabel{sec:sn-tracker-frame}
\newlabel{sec:sn_n3_elec}
\newlabel{sec:solar}
\newlabel{sec:tof-def}
\newlabel{sec:tracker}
\newlabel{sec:trigger}
\newlabel{sec:twonu}
\newlabel{sub@fig:1e1g_extProb}
\newlabel{sub@fig:1e1g_intProb}
\newlabel{sub@fig:2e_cut_eff}
\newlabel{sub@fig:2e_intProb}
\newlabel{sub@fig:2e_trackL}
\newlabel{sub@fig:2e_vtxZ}
\newlabel{sub@fig:2e_vtx_disc}
\newlabel{sub@fig:CompComp}
\newlabel{sub@fig:CompMoll}
\newlabel{sub@fig:IntCompton}
\newlabel{sub@fig:IntConversion}
\newlabel{sub@fig:IntMoller}
\newlabel{sub@fig:PairProd}
\newlabel{sub@fig:antoine-vs-p}
\newlabel{sub@fig:antoine-vs-t}
\newlabel{sub@fig:comm-plateau-scan}
\newlabel{sub@fig:cosee}
\newlabel{sub@fig:cu_rot}
\newlabel{sub@fig:ex_1e1g_ext}
\newlabel{sub@fig:ex_xe}
\newlabel{sub@fig:gammaCaloOcc}
\newlabel{sub@fig:hotRuns}
\newlabel{sub@fig:manchester-plateau-scan}
\newlabel{sub@fig:neutrino-theory:CC-Electron}
\newlabel{sub@fig:neutrino-theory:CC-Nucleon}
\newlabel{sub@fig:neutrino-theory:CC-vtx}
\newlabel{sub@fig:neutrino-theory:NC-Electron}
\newlabel{sub@fig:neutrino-theory:NC-Nucleon}
\newlabel{sub@fig:neutrino-theory:NC-vtx}
\newlabel{sub@fig:read-out-rate}
\newlabel{sub@fig:singleElectronE}
\newlabel{sub@fig:sumE_P1}
\newlabel{sub@fig:sumE_P2}
\newlabel{sub@fig:time-vs-ncards}
\newlabel{sub@fig:victor_rotation}
\newlabel{sub@fig:xe_extProb}
\newlabel{sub@}
\newlabel{sub@}
\newlabel{sub@}
\newlabel{sub@}
\newlabel{tab:0nu-best-meas}
\newlabel{tab:0nu_lims}
\newlabel{tab:0nu_syst}
\newlabel{tab:2nu-best-meas}
\newlabel{tab:isotope-q-abundance}
\newlabel{tab:nu_mass_lim}
\newlabel{tab:run_time}
\newlabel{tab:ss_os_syst}
\newlabel{tab:tot_syst}
\newlabel{table:Bi207_results}
\newlabel{table:HPGe_meas}
\newlabel{table:channels}
\newlabel{table:ext_meas}
\newlabel{table:external_model}
\newlabel{table:int_meas}
\newlabel{table:osc_parameters}
\newlabel{table:pars}
\newlabel{table:rdn_meas}
\newlabel{table:sn-specs}
\newlabel{table:y90_act_1e_2e}
\newlabel{}{{\relax 10.2a}{177}}
\newlabel{}{{\relax 10.2b}{177}}
\newlabel{}{{\relax 10.4a}{181}}
\newlabel{}{{\relax 10.4b}{181}}
\providecommand*\caption@xref[2]{\@setref\relax\@undefined{#1}}
\select@language{english}
\select@language{english}
\setcounter{ContinuedFloat}{0}
\setcounter{ContinuedFloat}{0}
\setcounter{ContinuedFloat}{0}
\setcounter{ContinuedFloat}{0}
\setcounter{ContinuedFloat}{0}
\setcounter{ContinuedFloat}{0}
\setcounter{ContinuedFloat}{0}
\setcounter{ContinuedFloat}{0}
\setcounter{ContinuedFloat}{0}
\setcounter{ContinuedFloat}{0}
\setcounter{ContinuedFloat}{0}
\setcounter{ContinuedFloat}{0}
\setcounter{ContinuedFloat}{0}
\setcounter{ContinuedFloat}{0}
\setcounter{KVtest}{1}
\setcounter{KVtest}{1}
\setcounter{KVtest}{1}
\setcounter{KVtest}{1}
\setcounter{KVtest}{1}
\setcounter{KVtest}{1}
\setcounter{KVtest}{1}
\setcounter{KVtest}{1}
\setcounter{KVtest}{1}
\setcounter{KVtest}{1}
\setcounter{KVtest}{1}
\setcounter{KVtest}{1}
\setcounter{KVtest}{1}
\setcounter{KVtest}{1}
\setcounter{chapter}{0}
\setcounter{chapter}{10}
\setcounter{chapter}{11}
\setcounter{chapter}{12}
\setcounter{chapter}{13}
\setcounter{chapter}{1}
\setcounter{chapter}{2}
\setcounter{chapter}{3}
\setcounter{chapter}{4}
\setcounter{chapter}{5}
\setcounter{chapter}{6}
\setcounter{chapter}{7}
\setcounter{chapter}{8}
\setcounter{chapter}{9}
\setcounter{cp@cntr}{0}
\setcounter{cp@cntr}{0}
\setcounter{cp@cntr}{0}
\setcounter{cp@cntr}{0}
\setcounter{cp@cntr}{0}
\setcounter{cp@cntr}{0}
\setcounter{cp@cntr}{0}
\setcounter{cp@cntr}{0}
\setcounter{cp@cntr}{0}
\setcounter{cp@cntr}{0}
\setcounter{cp@cntr}{0}
\setcounter{cp@cntr}{0}
\setcounter{cp@cntr}{0}
\setcounter{cp@cntr}{0}
\setcounter{enumiii}{0}
\setcounter{enumiii}{0}
\setcounter{enumiii}{0}
\setcounter{enumiii}{0}
\setcounter{enumiii}{0}
\setcounter{enumiii}{0}
\setcounter{enumiii}{0}
\setcounter{enumiii}{0}
\setcounter{enumiii}{0}
\setcounter{enumiii}{0}
\setcounter{enumiii}{0}
\setcounter{enumiii}{0}
\setcounter{enumiii}{0}
\setcounter{enumiii}{0}
\setcounter{enumii}{0}
\setcounter{enumii}{0}
\setcounter{enumii}{0}
\setcounter{enumii}{0}
\setcounter{enumii}{0}
\setcounter{enumii}{0}
\setcounter{enumii}{0}
\setcounter{enumii}{0}
\setcounter{enumii}{0}
\setcounter{enumii}{0}
\setcounter{enumii}{0}
\setcounter{enumii}{0}
\setcounter{enumii}{0}
\setcounter{enumii}{0}
\setcounter{enumiv}{0}
\setcounter{enumiv}{0}
\setcounter{enumiv}{0}
\setcounter{enumiv}{0}
\setcounter{enumiv}{0}
\setcounter{enumiv}{0}
\setcounter{enumiv}{0}
\setcounter{enumiv}{0}
\setcounter{enumiv}{0}
\setcounter{enumiv}{0}
\setcounter{enumiv}{0}
\setcounter{enumiv}{0}
\setcounter{enumiv}{0}
\setcounter{enumiv}{163}
\setcounter{enumi}{0}
\setcounter{enumi}{0}
\setcounter{enumi}{0}
\setcounter{enumi}{0}
\setcounter{enumi}{0}
\setcounter{enumi}{0}
\setcounter{enumi}{0}
\setcounter{enumi}{0}
\setcounter{enumi}{0}
\setcounter{enumi}{0}
\setcounter{enumi}{0}
\setcounter{enumi}{0}
\setcounter{enumi}{0}
\setcounter{enumi}{0}
\setcounter{equation}{0}
\setcounter{equation}{0}
\setcounter{equation}{0}
\setcounter{equation}{0}
\setcounter{equation}{0}
\setcounter{equation}{12}
\setcounter{equation}{12}
\setcounter{equation}{16}
\setcounter{equation}{18}
\setcounter{equation}{19}
\setcounter{equation}{1}
\setcounter{equation}{27}
\setcounter{equation}{2}
\setcounter{equation}{3}
\setcounter{figure}{0}
\setcounter{figure}{0}
\setcounter{figure}{0}
\setcounter{figure}{0}
\setcounter{figure}{10}
\setcounter{figure}{10}
\setcounter{figure}{16}
\setcounter{figure}{3}
\setcounter{figure}{5}
\setcounter{figure}{6}
\setcounter{figure}{6}
\setcounter{figure}{7}
\setcounter{figure}{8}
\setcounter{figure}{9}
\setcounter{fmfgraph}{0}
\setcounter{fmfgraph}{1}
\setcounter{fmfgraph}{1}
\setcounter{fmfgraph}{1}
\setcounter{fmfgraph}{1}
\setcounter{fmfgraph}{1}
\setcounter{fmfgraph}{1}
\setcounter{fmfgraph}{1}
\setcounter{fmfgraph}{1}
\setcounter{fmfgraph}{1}
\setcounter{fmfgraph}{1}
\setcounter{fmfgraph}{1}
\setcounter{fmfgraph}{1}
\setcounter{fmfgraph}{1}
\setcounter{footnote}{0}
\setcounter{footnote}{0}
\setcounter{footnote}{0}
\setcounter{footnote}{0}
\setcounter{footnote}{0}
\setcounter{footnote}{0}
\setcounter{footnote}{0}
\setcounter{footnote}{0}
\setcounter{footnote}{1}
\setcounter{footnote}{1}
\setcounter{footnote}{1}
\setcounter{footnote}{2}
\setcounter{footnote}{3}
\setcounter{footnote}{3}
\setcounter{iterlist}{0}
\setcounter{iterlist}{0}
\setcounter{iterlist}{0}
\setcounter{iterlist}{0}
\setcounter{iterlist}{0}
\setcounter{iterlist}{0}
\setcounter{iterlist}{0}
\setcounter{iterlist}{0}
\setcounter{iterlist}{0}
\setcounter{iterlist}{0}
\setcounter{iterlist}{0}
\setcounter{iterlist}{0}
\setcounter{iterlist}{0}
\setcounter{iterlist}{0}
\setcounter{lofdepth}{1}
\setcounter{lofdepth}{1}
\setcounter{lofdepth}{1}
\setcounter{lofdepth}{1}
\setcounter{lofdepth}{1}
\setcounter{lofdepth}{1}
\setcounter{lofdepth}{1}
\setcounter{lofdepth}{1}
\setcounter{lofdepth}{1}
\setcounter{lofdepth}{1}
\setcounter{lofdepth}{1}
\setcounter{lofdepth}{1}
\setcounter{lofdepth}{1}
\setcounter{lofdepth}{1}
\setcounter{lotdepth}{2}
\setcounter{lotdepth}{2}
\setcounter{lotdepth}{2}
\setcounter{lotdepth}{2}
\setcounter{lotdepth}{2}
\setcounter{lotdepth}{2}
\setcounter{lotdepth}{2}
\setcounter{lotdepth}{2}
\setcounter{lotdepth}{2}
\setcounter{lotdepth}{2}
\setcounter{lotdepth}{2}
\setcounter{lotdepth}{2}
\setcounter{lotdepth}{2}
\setcounter{lotdepth}{2}
\setcounter{mpfootnote}{0}
\setcounter{mpfootnote}{0}
\setcounter{mpfootnote}{0}
\setcounter{mpfootnote}{0}
\setcounter{mpfootnote}{0}
\setcounter{mpfootnote}{0}
\setcounter{mpfootnote}{0}
\setcounter{mpfootnote}{0}
\setcounter{mpfootnote}{0}
\setcounter{mpfootnote}{0}
\setcounter{mpfootnote}{0}
\setcounter{mpfootnote}{0}
\setcounter{mpfootnote}{0}
\setcounter{mpfootnote}{0}
\setcounter{page}{109}
\setcounter{page}{139}
\setcounter{page}{162}
\setcounter{page}{173}
\setcounter{page}{186}
\setcounter{page}{205}
\setcounter{page}{215}
\setcounter{page}{219}
\setcounter{page}{233}
\setcounter{page}{26}
\setcounter{page}{46}
\setcounter{page}{60}
\setcounter{page}{73}
\setcounter{page}{92}
\setcounter{paragraph}{0}
\setcounter{paragraph}{0}
\setcounter{paragraph}{0}
\setcounter{paragraph}{0}
\setcounter{paragraph}{0}
\setcounter{paragraph}{0}
\setcounter{paragraph}{0}
\setcounter{paragraph}{0}
\setcounter{paragraph}{0}
\setcounter{paragraph}{0}
\setcounter{paragraph}{0}
\setcounter{paragraph}{0}
\setcounter{paragraph}{0}
\setcounter{paragraph}{0}
\setcounter{parentequation}{0}
\setcounter{parentequation}{0}
\setcounter{parentequation}{0}
\setcounter{parentequation}{0}
\setcounter{parentequation}{0}
\setcounter{parentequation}{0}
\setcounter{parentequation}{0}
\setcounter{parentequation}{0}
\setcounter{parentequation}{0}
\setcounter{parentequation}{0}
\setcounter{parentequation}{0}
\setcounter{parentequation}{0}
\setcounter{parentequation}{0}
\setcounter{parentequation}{0}
\setcounter{part}{0}
\setcounter{part}{0}
\setcounter{part}{0}
\setcounter{part}{0}
\setcounter{part}{1}
\setcounter{part}{1}
\setcounter{part}{1}
\setcounter{part}{1}
\setcounter{part}{2}
\setcounter{part}{2}
\setcounter{part}{2}
\setcounter{part}{2}
\setcounter{part}{2}
\setcounter{part}{2}
\setcounter{pp@next@reset}{0}
\setcounter{pp@next@reset}{0}
\setcounter{pp@next@reset}{0}
\setcounter{pp@next@reset}{0}
\setcounter{pp@next@reset}{0}
\setcounter{pp@next@reset}{0}
\setcounter{pp@next@reset}{0}
\setcounter{pp@next@reset}{0}
\setcounter{pp@next@reset}{0}
\setcounter{pp@next@reset}{0}
\setcounter{pp@next@reset}{0}
\setcounter{pp@next@reset}{0}
\setcounter{pp@next@reset}{0}
\setcounter{pp@next@reset}{0}
\setcounter{r@tfl@t}{0}
\setcounter{r@tfl@t}{0}
\setcounter{r@tfl@t}{0}
\setcounter{r@tfl@t}{0}
\setcounter{r@tfl@t}{0}
\setcounter{r@tfl@t}{0}
\setcounter{r@tfl@t}{0}
\setcounter{r@tfl@t}{0}
\setcounter{r@tfl@t}{0}
\setcounter{r@tfl@t}{0}
\setcounter{r@tfl@t}{0}
\setcounter{r@tfl@t}{0}
\setcounter{r@tfl@t}{0}
\setcounter{r@tfl@t}{0}
\setcounter{section}{0}
\setcounter{section}{0}
\setcounter{section}{1}
\setcounter{section}{3}
\setcounter{section}{3}
\setcounter{section}{4}
\setcounter{section}{4}
\setcounter{section}{5}
\setcounter{section}{5}
\setcounter{section}{5}
\setcounter{section}{6}
\setcounter{section}{7}
\setcounter{section}{8}
\setcounter{section}{8}
\setcounter{subfigure@save}{0}
\setcounter{subfigure@save}{2}
\setcounter{subfigure@save}{2}
\setcounter{subfigure@save}{2}
\setcounter{subfigure@save}{2}
\setcounter{subfigure@save}{2}
\setcounter{subfigure@save}{2}
\setcounter{subfigure@save}{2}
\setcounter{subfigure@save}{2}
\setcounter{subfigure@save}{2}
\setcounter{subfigure@save}{2}
\setcounter{subfigure@save}{4}
\setcounter{subfigure@save}{4}
\setcounter{subfigure@save}{4}
\setcounter{subfigure}{0}
\setcounter{subfigure}{0}
\setcounter{subfigure}{0}
\setcounter{subfigure}{0}
\setcounter{subfigure}{0}
\setcounter{subfigure}{0}
\setcounter{subfigure}{0}
\setcounter{subfigure}{0}
\setcounter{subfigure}{0}
\setcounter{subfigure}{0}
\setcounter{subfigure}{0}
\setcounter{subfigure}{0}
\setcounter{subfigure}{0}
\setcounter{subfigure}{0}
\setcounter{subparagraph}{0}
\setcounter{subparagraph}{0}
\setcounter{subparagraph}{0}
\setcounter{subparagraph}{0}
\setcounter{subparagraph}{0}
\setcounter{subparagraph}{0}
\setcounter{subparagraph}{0}
\setcounter{subparagraph}{0}
\setcounter{subparagraph}{0}
\setcounter{subparagraph}{0}
\setcounter{subparagraph}{0}
\setcounter{subparagraph}{0}
\setcounter{subparagraph}{0}
\setcounter{subparagraph}{0}
\setcounter{subsection}{0}
\setcounter{subsection}{0}
\setcounter{subsection}{0}
\setcounter{subsection}{0}
\setcounter{subsection}{0}
\setcounter{subsection}{0}
\setcounter{subsection}{0}
\setcounter{subsection}{0}
\setcounter{subsection}{0}
\setcounter{subsection}{0}
\setcounter{subsection}{0}
\setcounter{subsection}{0}
\setcounter{subsection}{0}
\setcounter{subsection}{1}
\setcounter{subsubsection}{0}
\setcounter{subsubsection}{0}
\setcounter{subsubsection}{0}
\setcounter{subsubsection}{0}
\setcounter{subsubsection}{0}
\setcounter{subsubsection}{0}
\setcounter{subsubsection}{0}
\setcounter{subsubsection}{0}
\setcounter{subsubsection}{0}
\setcounter{subsubsection}{0}
\setcounter{subsubsection}{0}
\setcounter{subsubsection}{0}
\setcounter{subsubsection}{0}
\setcounter{subsubsection}{0}
\setcounter{subtable@save}{0}
\setcounter{subtable@save}{0}
\setcounter{subtable@save}{0}
\setcounter{subtable@save}{0}
\setcounter{subtable@save}{0}
\setcounter{subtable@save}{0}
\setcounter{subtable@save}{0}
\setcounter{subtable@save}{0}
\setcounter{subtable@save}{0}
\setcounter{subtable@save}{0}
\setcounter{subtable@save}{0}
\setcounter{subtable@save}{0}
\setcounter{subtable@save}{0}
\setcounter{subtable@save}{0}
\setcounter{subtable}{0}
\setcounter{subtable}{0}
\setcounter{subtable}{0}
\setcounter{subtable}{0}
\setcounter{subtable}{0}
\setcounter{subtable}{0}
\setcounter{subtable}{0}
\setcounter{subtable}{0}
\setcounter{subtable}{0}
\setcounter{subtable}{0}
\setcounter{subtable}{0}
\setcounter{subtable}{0}
\setcounter{subtable}{0}
\setcounter{subtable}{0}
\setcounter{table}{0}
\setcounter{table}{0}
\setcounter{table}{0}
\setcounter{table}{0}
\setcounter{table}{0}
\setcounter{table}{0}
\setcounter{table}{0}
\setcounter{table}{0}
\setcounter{table}{1}
\setcounter{table}{1}
\setcounter{table}{2}
\setcounter{table}{3}
\setcounter{table}{6}
\setcounter{table}{7}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
