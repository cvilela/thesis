#!/bin/bash

echo "Running mf on all feynman diagrams"

for file in `ls *.mf`
do
    echo $file
    echo mf \'\\mode=localfont\; input ${file}\'
    mf \\mode=localfont\; input ${file}
done
