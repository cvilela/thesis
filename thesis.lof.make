\select@language {english}
\select@language {english}
\addvspace {10\p@ }
\addvspace {10\p@ }
\contentsline {figure}{\numberline {\relax 2.1}{\ignorespaces Measurement of the hadronic cross-section at the Z mass pole by the LEP experiments.}}{30}
\contentsline {figure}{\numberline {\relax 2.2}{\ignorespaces Standard Model vertices involving neutrinos.}}{31}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces {Charged current vertex.}}}{31}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces {Neutral current vertex.}}}{31}
\contentsline {figure}{\numberline {\relax 2.3}{\ignorespaces Neutrino interactions with matter: charged and neutral current scattering off nucleons or atomic electrons.}}{32}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces {CC $\nu $-$e^-$ Scattering}}}{32}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces {NC $\nu $-$e^-$ Scattering}}}{32}
\contentsline {subfigure}{\numberline {(c)}{\ignorespaces {CC $\nu $-$N$ Scattering}}}{32}
\contentsline {subfigure}{\numberline {(d)}{\ignorespaces {NC $\nu $-$N$ Scattering}}}{32}
\contentsline {figure}{\numberline {\relax 2.4}{\ignorespaces Normal and inverted scenarios for the neutrino mass spectrum.}}{38}
\contentsline {figure}{\numberline {\relax 2.5}{\ignorespaces Electron energy spectrum in the $\beta $ decay of tritium}}{42}
\contentsline {figure}{\numberline {\relax 2.6}{\ignorespaces Effective \0nu mass against smallest neutrino mass.}}{45}
\addvspace {10\p@ }
\contentsline {figure}{\numberline {\relax 3.1}{\ignorespaces Masses of even $A$ nuclei, as described by the SEMF.}}{49}
\contentsline {figure}{\numberline {\relax 3.2}{\ignorespaces Feynman diagram for the two-neutrino double-$\beta $ decay.}}{50}
\contentsline {figure}{\numberline {\relax 3.3}{\ignorespaces Majorana mass term due to \0nu .}}{52}
\contentsline {figure}{\numberline {\relax 3.4}{\ignorespaces Feynman diagram for the mass mechanism \0nu .}}{53}
\contentsline {figure}{\numberline {\relax 3.5}{\ignorespaces Summed energy spectrum of double-$\beta $ decay.}}{54}
\contentsline {figure}{\numberline {\relax 3.6}{\ignorespaces Feynman diagram for the $\mathinner {\delimiter "426830A {\lambda }\delimiter "526930B }$ mode of \0nu arising from left-right symmetric models.}}{55}
\contentsline {figure}{\numberline {\relax 3.7}{\ignorespaces Comparison between the nuclear matrix elements for 11 isotopes calculated in five different frameworks.}}{59}
\contentsline {figure}{\numberline {\relax 3.8}{\ignorespaces Comparison between the \0nu half-lives of 11 isotopes assuming an effective Majorana mass of $\mathinner {\delimiter "426830A {m_{\beta \beta }}\delimiter "526930B }=50$\meV .}}{59}
\addvspace {10\p@ }
\addvspace {10\p@ }
\contentsline {figure}{\numberline {\relax 5.1}{\ignorespaces Cutaway drawing of the NEMO-3 detector.}}{76}
\contentsline {figure}{\numberline {\relax 5.2}{\ignorespaces Top-view diagram of the NEMO-3 detector.}}{76}
\contentsline {figure}{\numberline {\relax 5.3}{\ignorespaces Arrangement of the different sources in the NEMO-3 detector.}}{77}
\contentsline {figure}{\numberline {\relax 5.4}{\ignorespaces Map of the \Ca {48} and neighbouring sources as seen in NEMO-3 data.}}{78}
\contentsline {figure}{\numberline {\relax 5.5}{\ignorespaces Structure of the \CaF 2 source discs.}}{80}
\contentsline {figure}{\numberline {\relax 5.6}{\ignorespaces Photograph of the \Ca {48} source.}}{80}
\contentsline {figure}{\numberline {\relax 5.7}{\ignorespaces Rotated source foil strips.}}{81}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces {}}}{81}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces {}}}{81}
\contentsline {figure}{\numberline {\relax 5.8}{\ignorespaces Plan view of the tracker in one NEMO-3 sector.}}{82}
\contentsline {figure}{\numberline {\relax 5.9}{\ignorespaces Schematic drawing of a NEMO-3 calorimeter module.}}{84}
\addvspace {10\p@ }
\contentsline {figure}{\numberline {\relax 6.1}{\ignorespaces Example of a two-electron event.}}{96}
\contentsline {figure}{\numberline {\relax 6.2}{\ignorespaces Example of an event with a delayed $\alpha $ track.}}{97}
\contentsline {figure}{\numberline {\relax 6.3}{\ignorespaces Example of events with external origin.}}{98}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces {Crossing electron}}}{98}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces {External electron and $\gamma $}}}{98}
\contentsline {figure}{\numberline {\relax 6.4}{\ignorespaces Distributions of p-values for the internal time of flight hypothesis.}}{101}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces {Two-electron}}}{101}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces {One electron and one $\gamma $}}}{101}
\contentsline {figure}{\numberline {\relax 6.5}{\ignorespaces Distributions of p-values for the external time of flight hypothesis.}}{103}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces {Crossing electron}}}{103}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces {One electron and one $\gamma $}}}{103}
\contentsline {figure}{\numberline {\relax 6.6}{\ignorespaces Example of NLLR distributions.}}{106}
\addvspace {10\p@ }
\contentsline {figure}{\numberline {\relax 7.1}{\ignorespaces Internal background production processes.}}{111}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces {$\beta $-decay and M{\o }ller Scattering}}}{111}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces {$\beta $-decay and Internal Conversion}}}{111}
\contentsline {subfigure}{\numberline {(c)}{\ignorespaces {$\beta $-decay and Compton Scattering}}}{111}
\contentsline {figure}{\numberline {\relax 7.2}{\ignorespaces External background production processes.}}{113}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces {Pair Production}}}{113}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces {Compton + M{\o }ller Scattering}}}{113}
\contentsline {subfigure}{\numberline {(c)}{\ignorespaces {Double Compton Scattering}}}{113}
\contentsline {figure}{\numberline {\relax 7.3}{\ignorespaces Thorium decay chain.}}{115}
\contentsline {figure}{\numberline {\relax 7.4}{\ignorespaces Uranium decay chain.}}{116}
\contentsline {figure}{\numberline {\relax 7.5}{\ignorespaces Radon level in the NEMO-3 detector.}}{118}
\contentsline {figure}{\numberline {\relax 7.6}{\ignorespaces Exponential decay of \Sr {90}/\Y {90} in the \Ca {48} source.}}{121}
\contentsline {figure}{\numberline {\relax 7.7}{\ignorespaces Abnormal calorimeter activity leading to the exclusion of runs 8779 -- 8786.}}{127}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces {}}}{127}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces {Event rate for events where one $\gamma $ hits the calorimeter hot spot in sectors 9 and 10.}}}{127}
\contentsline {figure}{\numberline {\relax 7.8}{\ignorespaces Distributions of total energy in 1eN$\gamma $ and 1e1$\gamma $ (internal) events for three energy thresholds.}}{128}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces {1eN$\gamma $ 200\keV threshold}}}{128}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces {1e1$\gamma $ (internal) 200\keV threshold}}}{128}
\contentsline {subfigure}{\numberline {(c)}{\ignorespaces {1eN$\gamma $ 300\keV threshold}}}{128}
\contentsline {subfigure}{\numberline {(d)}{\ignorespaces {1e1$\gamma $ (internal) 300\keV threshold}}}{128}
\contentsline {subfigure}{\numberline {(e)}{\ignorespaces {1eN$\gamma $ 400\keV threshold}}}{128}
\contentsline {subfigure}{\numberline {(f)}{\ignorespaces {1e1$\gamma $ (internal) 400\keV threshold}}}{128}
\contentsline {figure}{\numberline {\relax 7.9}{\ignorespaces Electron energy distribution in 1eN$\gamma $ events.}}{130}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces {1eN$\gamma $ Phase 1}}}{130}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces {1eN$\gamma $ Phase 2}}}{130}
\contentsline {figure}{\numberline {\relax 7.10}{\ignorespaces Distribution of the cosine of the opening angle between the electron and the $\gamma $ in 1e1$\gamma $ (internal) events.}}{131}
\contentsline {figure}{\numberline {\relax 7.11}{\ignorespaces Distribution of the summed energy of the electron and the $\gamma $ in 1e1$\gamma $ (internal) events.}}{132}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces {1e1$\gamma $ (internal) Phase 1}}}{132}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces {1e1$\gamma $ (internal) Phase 2}}}{132}
\contentsline {figure}{\numberline {\relax 7.12}{\ignorespaces Distribution of the summed energy of the electron and the two $\gamma $'s in 1e2$\gamma $ events.}}{132}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces {1e2$\gamma $ Phase 1}}}{132}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces {1e2$\gamma $ Phase 2}}}{132}
\contentsline {figure}{\numberline {\relax 7.13}{\ignorespaces Distribution of the length of the delayed $\alpha $ track in 1e1$\alpha $ events.}}{134}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces {1e1$\alpha $ Phase 1}}}{134}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces {1e1$\alpha $ Phase 2}}}{134}
\contentsline {figure}{\numberline {\relax 7.14}{\ignorespaces Distribution of the summed energy of the electron and the $\gamma $ in 1e1$\gamma $ (external) events.}}{134}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces {1e1$\gamma $ (external) Phase 1}}}{134}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces {1e1$\gamma $ (external) Phase 2}}}{134}
\contentsline {figure}{\numberline {\relax 7.15}{\ignorespaces Summed calorimeter energy distribution in crossing electron events.}}{135}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces {Crossing electron Phase 1}}}{135}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces {Crossing electron Phase 2}}}{135}
\contentsline {figure}{\numberline {\relax 7.16}{\ignorespaces Comparison between measured internal background activities and HPGe measurements.}}{136}
\addvspace {10\p@ }
\contentsline {figure}{\numberline {\relax 8.1}{\ignorespaces Mis-modelling of the distance between electron vertices in ``same side'' events and difference in efficiency of the vertex distance cuts in data and MC.}}{141}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces {}}}{141}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces {}}}{141}
\contentsline {figure}{\numberline {\relax 8.2}{\ignorespaces Distribution of events against the run number.}}{143}
\contentsline {figure}{\numberline {\relax 8.3}{\ignorespaces Distributions of geometrical variables in 2eN$\gamma $ events.}}{144}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces {}}}{144}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces {}}}{144}
\contentsline {figure}{\numberline {\relax 8.4}{\ignorespaces Distributions of kinematic variables in 2eN$\gamma $ events.}}{145}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces {}}}{145}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces {}}}{145}
\contentsline {figure}{\numberline {\relax 8.5}{\ignorespaces Distribution of the summed electron energies in 2eN$\gamma $ events.}}{146}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces {Phase 1}}}{146}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces {Phase 2}}}{146}
\contentsline {figure}{\numberline {\relax 8.6}{\ignorespaces Optimisation of the \2nu fitting window.}}{150}
\contentsline {figure}{\numberline {\relax 8.7}{\ignorespaces Comparison of the measured $\left | M^{2\nu }\right |$ to theoretical calculations.}}{152}
\contentsline {figure}{\numberline {\relax 8.8}{\ignorespaces 2eN$\gamma $ summed electron distribution with \0nu limits.}}{155}
\contentsline {figure}{\numberline {\relax 8.9}{\ignorespaces 2eN$\gamma $ background subtracted data with \0nu limits.}}{155}
\contentsline {figure}{\numberline {\relax 8.10}{\ignorespaces Comparison of the $\mathinner {\delimiter "426830A {m_{\beta \beta }}\delimiter "526930B }$ limit measured in this analysis to the best available limits with \Ca {48}, \Mo {100} and \Xe {136}.}}{158}
\addvspace {10\p@ }
\contentsline {figure}{\numberline {\relax 9.1}{\ignorespaces Exploded view drawing of the SuperNEMO Demonstrator module.}}{167}
\contentsline {figure}{\numberline {\relax 9.2}{\ignorespaces Construction of the SuperNEMO main wall calorimeter modules.}}{168}
\contentsline {figure}{\numberline {\relax 9.3}{\ignorespaces Sensitivity of the SuperNEMO experiment as a function of exposure.}}{172}
\addvspace {10\p@ }
\contentsline {figure}{\numberline {\relax 10.1}{\ignorespaces Sketch of the signal generating process in Geiger counters.}}{175}
\contentsline {figure}{\numberline {\relax 10.2}{\ignorespaces Sketch of a SuperNEMO tracker cell.}}{177}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces {}}}{177}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces {}}}{177}
\contentsline {figure}{\numberline {\relax 10.3}{\ignorespaces Example set of signals from a Geiger discharge in a SuperNEMO tracker cell.}}{179}
\contentsline {figure}{\numberline {\relax 10.4}{\ignorespaces Sketch of a SuperNEMO tracker C-section.}}{181}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces {}}}{181}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces {}}}{181}
\contentsline {figure}{\numberline {\relax 10.5}{\ignorespaces Photograph of the wiring robot in the University of Manchester clean-room.}}{183}
\contentsline {figure}{\numberline {\relax 10.6}{\ignorespaces Photograph of the C0 frame at MSSL, partially populated with cells.}}{184}
\contentsline {figure}{\numberline {\relax 10.7}{\ignorespaces Derivative of the anode signal.}}{185}
\addvspace {10\p@ }
\contentsline {figure}{\numberline {\relax 11.1}{\ignorespaces Schematic diagram of the tracker commissioning electronics.}}{189}
\contentsline {figure}{\numberline {\relax 11.2}{\ignorespaces The NEMO-3 HV distribution and decoupling system.}}{190}
\contentsline {figure}{\numberline {\relax 11.3}{\ignorespaces Timing diagram for the tracker commissioning DAQ system.}}{193}
\contentsline {figure}{\numberline {\relax 11.4}{\ignorespaces Read-out dead time and rate of the tracker commissioning DAQ system.}}{196}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces {Read-out time}}}{196}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces {$\Delta t$ between consecutive read-out cycles}}}{196}
\contentsline {figure}{\numberline {\relax 11.5}{\ignorespaces Photograph of the electronics and cosmic ray trigger at the University of Manchester}}{197}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces {}}}{197}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces {}}}{197}
\contentsline {figure}{\numberline {\relax 11.6}{\ignorespaces Sketch of a cassette in the test tank.}}{199}
\contentsline {figure}{\numberline {\relax 11.7}{\ignorespaces Results of the $x$-axis scan of the cassette.}}{200}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces {Position $A$}}}{200}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces {Position $B$}}}{200}
\contentsline {subfigure}{\numberline {(c)}{\ignorespaces {Position $C$}}}{200}
\contentsline {subfigure}{\numberline {(d)}{\ignorespaces {Position $D$}}}{200}
\contentsline {figure}{\numberline {\relax 11.8}{\ignorespaces Scan of the Geiger plateau.}}{202}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces {Commissioning DAQ}}}{202}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces {Cassette testing DAQ\cite {manchester-data}}}}{202}
\contentsline {figure}{\numberline {\relax 11.9}{\ignorespaces Efficiency of the avalanche propagation of six cells in a cassette.}}{203}
\contentsline {figure}{\numberline {\relax 11.10}{\ignorespaces Distribution of the propagation time to the read out cathode ring for six cells in a cassette.}}{204}
\addvspace {10\p@ }
\contentsline {figure}{\numberline {\relax 12.1}{\ignorespaces Flow diagram for the Demonstrator's gas mixing system.}}{207}
\contentsline {figure}{\numberline {\relax 12.2}{\ignorespaces Diagram of a tentative gas outlet system of the Demonstrator.}}{208}
\contentsline {figure}{\numberline {\relax 12.3}{\ignorespaces Fractional volumes of ethanol obtained at equilibrium under controlled temperature and pressure conditions.}}{210}
\contentsline {subfigure}{\numberline {(a)}{\ignorespaces {}}}{210}
\contentsline {subfigure}{\numberline {(b)}{\ignorespaces {}}}{210}
\contentsline {figure}{\numberline {\relax 12.4}{\ignorespaces Photograph of the Demonstrator's gas mixing system.}}{211}
\contentsline {figure}{\numberline {\relax 12.5}{\ignorespaces Ethanol volume fraction in the gas measured by the depletion rate of liquid ethanol.}}{213}
\addvspace {10\p@ }
