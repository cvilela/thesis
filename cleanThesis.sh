#!/bin/bash

rm *.600pk
rm *.log
rm *.mf
rm *.tfm
rm *.cut
rm *.aux
rm *.make
rm *.bb
rm *.d
rm *.blg
rm *.fls
rm *.lof
rm *.lot
rm *.pdf
rm *.toc
rm *~
rm */*/*~
rm */*/*.aux