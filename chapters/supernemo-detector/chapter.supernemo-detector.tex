\chapter{The SuperNEMO experiment}
\label{sec:sn-det}
SuperNEMO is a next-generation double-$\beta$ decay experiment based on the proven technique of NEMO-3. While the basic detection strategy is kept the same, a number of improvements were attained for each of the detector's main components in an extensive R\&D programme. Materials selected for the construction of the experiment were screened for radioactive contaminants, including \Rn{222} emanation measurements. The resulting design is modular in nature, allowing for straightforward (but costly) scalability, and foresees an increase in the source mass by one order of magnitude. This, combined with the improved experimental characteristics, will allow for a two order of magnitude gain in sensitivity to \0nu processes. 

In its baseline configuration, SuperNEMO will consist of 20 modules, each hosting \mbox{5 -- 7 kg} of source isotope. Each module consists of a thin source foil with a surface density of 40 mg$/\text{cm}^2$ in a tracking volume surrounded by a calorimeter. The isotope \Se{82} was chosen as the primary source for SuperNEMO for its relatively low rate of \2nu, the irreducible background for \0nu searches. Other sources considered for use in SuperNEMO include \Nd{150} and \Ca{48}, both of which are currently difficult to enrich in substantial quantities. The tracker and calorimeter closely follow the NEMO-3 design, with major improvements, particularly in terms of energy resolution and radiopurity. 

Unlike the NEMO-3 detector, SuperNEMO modules have a planar geometry and a more complete coverage of the source by the calorimeter, resulting in an enhanced \0nu signal efficiency. The modular design allows for gradual deployment of the detector as modules are completed and also for the distribution of modules across several underground laboratories, if required. 

The first SuperNEMO module has as its main goal demonstrating the improved experimental parameters achieved through R\&D, in particular the very low background level. A summary of the characteristics of the full-scale SuperNEMO and of the Demonstrator module is given in Table \ref{table:sn-specs}. A detailed description of the Demonstrator module is given in Section \ref{sec:sn-demo} and the main steps of the SuperNEMO R\&D programme are outlined in Section \ref{sec:sn-rd}. A brief overview of the timescale of the experiment is given in Section \ref{sec:sn-timescale}.

\begin{table}[htpb]
  \centering
  \begin{tabular}{l c c}
    & SuperNEMO & Demonstrator \\ \hline
    Source & \Se{82} (\Nd{150}, \Ca{48}) & \Se{82} \\
    Source mass & 100 kg & 7 kg \\
%    \0nu detection efficiency  &  \multicolumn{2}{c}{30\%} \\
    Energy resolution & \multicolumn{2}{c}{4 \% @ 3 MeV (FWHM) } \\
    \Rn{222} in tracker volume & \multicolumn{2}{c}{ $<$ 0.15 \mBq/m$^3$} \\
    \Tl{208} in source foil    & \multicolumn{2}{c}{ $<$ 2 \uBq/kg} \\
    \Bi{214} in source foil    & \multicolumn{2}{c}{ $<$ 10 \uBq/kg} \\
    Sensitivity to $0\nu\beta\beta$ (90 \% CL) & $t^{1/2}_{0\nu} >$ 10$^{26}$ yr & $t^{1/2}_{0\nu} >$ 6.6 $\times$ 10$^{24}$ yr \\
    &   $\braket{m_{\beta\beta}} <$ 40 -- 100 meV &  $\braket{m_{\beta\beta}} <$ 200 -- 400 meV\\
  \end{tabular}
  \caption[Experimental parameters and $0\nu\beta\beta$ sensitivity for the SuperNEMO experiment and its Demonstrator module.]{Experimental parameters and $0\nu\beta\beta$ sensitivity for the SuperNEMO experiment and its Demonstrator module.}
\label{table:sn-specs}
\end{table}

\section{The Demonstrator module}
\label{sec:sn-demo}
The Demonstrator module is currently under construction and will take NEMO-3's place in LSM. Besides the main objective of demonstrating the very low background level required for the full-scale experiment, the Demonstrator is expected to produce competitive double-$\beta$ decay results on its own, as shown in Table \ref{table:sn-specs}. To this end, the Demonstrator's source will have a higher surface density than what is planned for the full-scale detector, at 50 mg$/\text{cm}^2$, yielding 7 kg of \Se{82}.

The detector consists of a source frame placed between two tracking chambers which in turn are surrounded by calorimeter modules (Figure \ref{fig:demo-explo}). This structure occupies a volume of \mbox{6 $\times$ 2 $\times$ 4 m$^3$} which is surrounded by a magnetic field coil and shielding. The coil and parts of the shielding are reused from NEMO-3.

\begin{figure}[htpb]
  \def\svgwidth{\mediumfigwidth}
  \import{chapters/supernemo-detector/}{SuperNEMOExploded.pdf_tex}
  \caption[Exploded view drawing of the SuperNEMO Demonstrator module.]{Exploded view drawing of the SuperNEMO Demonstrator module.}
  \label{fig:demo-explo}
\end{figure}

The main calorimeter walls consist of 520 calorimeter modules, similar in design to their NEMO-3 counterparts but with much improved energy resolution. The scintillators comprise a mixed set of PS based and polyvinyl-toluene (PVT) based blocks. The scintillator blocks are directly coupled to a mixture of newly developed 8$^{\prime\prime}$ high quantum efficiency Hamamatsu PMTs and 5$^{\prime\prime}$ PMTs recovered from NEMO-3 (Figure \ref{fig:main-wall-calo}). Eight-module \emph{bricks} form the basic structure of the calorimeter walls, with modules in each group of eight sharing their structural support, magnetic shielding and gas seal.
\begin{figure}[htpb]
  \includegraphics[width=0.8\textwidth]{chapters/supernemo-detector/main-wall-assembly.jpg}
  \caption[Construction of the SuperNEMO main wall calorimeter modules.]{Construction of the SuperNEMO main wall calorimeter modules. Scintillator blocks are shown directly coupled to NEMO-3 5$^{\prime\prime}$ PMTs. The module in the centre of the photograph is being wrapped in PTFE tape.}
  \label{fig:main-wall-calo}
\end{figure}


The tracking chambers contain a total of 2034 cells operating in Geiger mode, with the tracking volume being enclosed on the sides, top and bottom by calorimeter blocks. These blocks are of a lower specification than those on the main walls, being instrumented with PMTs recovered from NEMO-3. In particular, the blocks on the top and bottom frames of the tracker are intended to be used primarily to veto $\gamma$'s.

As in the case of NEMO-3, the thin strips of source isotope are strung vertically in the middle of the tracking chamber. The frame designed to accommodate the source foil also includes a system for deploying radioactive calibration sources. 

\section{Research and development}
\label{sec:sn-rd}
Given the experience obtained with the NEMO-3 experiment, an extensive R\&D programme was designed to improve several aspects of the technique for SuperNEMO. A brief overview of these efforts is given in this section.

\subsubsection{Source}
The main R\&D activities regarding the source foils were related to the reduction of the internal backgrounds. The target \Bi{214} and \Tl{208} activities in the SuperNEMO source foils are one order of magnitude lower than the level attained in NEMO-3.

One of the main efforts was the development of a standalone detector able to measure the activity of \Bi{214} and \Tl{208} in thin strips of material down to the required limit for SuperNEMO\cite{bipo}. This is achieved by the detection of BiPo delayed coincidence events, which allows the activities of contaminants present in both the thorium and uranium series downstream of the Rn isotopes to be constrained.

In addition, new isotope purification techniques were developed, as well as alternative support structures in which the amount of extraneous material in the foils is minimised. These efforts are ongoing.

\subsubsection{Calibration systems}
As part of the development of the source foil supporting frame, a system was designed to deploy radioactive calibration sources in the detector volume, as was done in NEMO-3. The new system has greatly reduced the amount of material inside the tracking volume by substituting the copper tubes of NEMO-3 with guide wires. The calibration sources are suspended from a wire on a pulley system with a plumb on the end. The weight slides on the guide wires, keeping it in a well defined position as it descends into the detector volume, carrying with it the calibration sources.

A new light injection system was developed, similar to that of NEMO-3. LED's are used as a light source for the new system, which has been shown to measure the energy scale fluctuations of calorimeter modules with a precision of 1\% over the period of one month.

\subsubsection{Calorimeter}
A substantial effort was put into the optimisation of the calorimeter modules in order to improve the energy resolution by a factor of two compared to NEMO-3. The main contributions to the enhanced energy resolution of the SuperNEMO calorimeter blocks are the use of PVT instead of PS as the scintillator base and the direct coupling of the PMTs to the scintillator. The latter requires one face of the scintillator blocks to be moulded into the hemispherical shape of the PMT photocathode.

As a compromise between cost and performance part of the calorimeter is instrumented with lower specification blocks, with PS-based scintillator blocks and 5'' PMTs from NEMO-3. These blocks are located in the regions of the walls with lower expected occupancy (\emph{e.g.}, around the edges of the walls), while the areas of high occupancy are populated with the SuperNEMO specification modules.

\subsubsection{Radon emanation}
The emanation of \Rn{222} into the tracking volume proved to be an important source of background in NEMO-3, particularly for isotopes which have their Q-value below that of the $\beta$-decay of \Bi{214} (3.3\MeV), such as \Se{82}. To minimise this background, detector materials in contact with the tracking gas have been screened for their radon emanation in addition to the bulk radiopurity measurements with HPGe detectors. 

A specialised detector was developed which is able to measure activities of \Rn{222} down to a few\uBq/m$^3$. This is achieved by passing large volumes of gas through an activated charcoal trap, and transferring the adsorbed \Rn{222} into an electrostatic detector. This apparatus is being used to measure the \Rn{222} emanation in the tracker at various stages of construction and also the \Rn{222} emanating from the gas mixing system's materials into the tracking gas.

Another concern is the diffusion of \Rn{222} from more contaminated parts (such as PMT glass) into the tracking volume. A \Rn{222} sealing strategy was identified, where a combination of RTV silicone and styrene-butadiene rubber (SBR) are used. While the first produces a good tracking-gas seal, it performs poorly as a \Rn{222} barrier, which is achieved by the latter. An experimental setup was developed to measure the diffusion of radon through thin sheets of material\cite{rnDiffusion}. This apparatus has been used to identify \Rn{222} sealing materials, in particular regarding the possibility of installing an anti-\Rn{222} screen separating the tracker volume from the calorimeter walls.

As a result of this R\&D work the activity of \Rn{222} inside the tracking volume is expected to be reduced by a factor of 30 compared to NEMO-3.

\subsubsection{Tracker}
The geometry of the tracker cells was optimised, resulting in cells with a larger radius, minimising the amount of material in the tracker. To reduce the contamination of the tracker wires, which present a very large surface to the detector volume, an automated system was developed to string the tracker cells largely without human manipulation of the wires. A new technique for the read-out of tracker signals has been developed and will be trialled in the Demonstrator module. A detailed description of the Demonstrator tracker is given in Chapter \ref{sec:tracker}.

\section{Timescale and sensitivity}
\label{sec:sn-timescale}
The experiment is currently in an advanced stage of construction.

The first quarter of the tracker frame has been assembled and fully populated with wires and calorimeter modules. Measurements of the \Rn{222} level in the fully populated frame will be made and the tracker will be commissioned at the surface, under cosmic rays. The preparations for surface commissioning are described in detail in Chapter \ref{sec:ch-elec}.

The modules for the main calorimeter walls are currently in production. Modules will be assembled into 2 $\times$ 4 \emph{bricks} and transported to the underground laboratory, where the \emph{bricks} will be put together to form the main calorimeter walls.

The first prototypes of the source foil are currently being measured for \Bi{214} and \Tl{208} contamination in the dedicated BiPo detector mentioned above.

Integration of the different Demonstrator components at LSM will occur in 2015. The target sensitivity of the Demonstrator is expected to be achieved within 2.5 years of operation. Upon successful deployment of the Demonstrator, further modules will be built and installed in an extension of LSM currently under construction. The sensitivity achieved by SuperNEMO as a function of exposure is shown in \mbox{Figure \ref{fig:sn-sens}}. The full-scale experiment is expected to be sensitive to a half-life of 10$^{26}$ years within five years of operation.
\begin{figure}[htpb]
  \def\svgwidth{\largefigwidth}
  \import{chapters/supernemo-detector/}{Sensitivity.pdf_tex}
  \caption[Sensitivity of the SuperNEMO experiment as a function of exposure.]{Sensitivity of the SuperNEMO experiment as a function of exposure\cite{motty}. The exposure attained by the full scale experiment within five years is shown in blue.}
  \label{fig:sn-sens}
\end{figure}
