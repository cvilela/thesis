\chapter{Double-$\beta$ decay}
\label{sec:dbd}
Double-$\beta$ decay is a very rare form of radioactive decay, being a low-energy second-order weak interaction process. The process consists of two $\beta$ decays (Section \ref{sec:beta-decay}) occurring at once. One mode of the decay is allowed by the SM, with two neutrinos being emitted with the two electrons (Section \ref{sec:twonu}). This process has been observed in several nuclei. Modes where the decay happens without the emission of the neutrinos are predicted by several models beyond the SM, and generally violate the lepton number by two units (Section \ref{sec:0nu}). One major difficulty in the prediction of double-$\beta$ decay rates is the calculation of the nuclear matrix elements. The different approaches used to perform these calculations are summarised in Section \ref{sec:nme}.

\section{$\beta$ decay}
\label{sec:beta-decay}
$\beta$ decay is a common radioactive transition which involves the change of flavour of a quark in a nucleon such that a neutron is transformed into a proton or vice-versa resulting in the transmutation to a different element. This transformation occurs via the weak charged current and always results in the emission of a neutrino or an anti-neutrino. The three ways in which this type of decay occurs are: $\beta^-$ decay, where an electron is emitted; $\beta^+$ decay, with the emission of a positron; and electron capture (EC), with no emission of charged particles.

In $\beta^-$ decay, a neutron is transformed into a proton, with the emission of one electron and one $\bar{\nu}_e$:
\begin{equation}
  n \rightarrow p^+ + e^- + \bar{\nu}_e \, ,
\end{equation}
\noindent while in $\beta^+$ decay a proton is transformed into a neutron, with a positron and a $\nu_e$ being emitted:
\begin{equation}
  p^+ \rightarrow n + e^+ + \nu_e \, .
  \label{eq:betaplus}
\end{equation}

EC decays differ from the two decays described above, in that the charged lepton is an electron captured from the atom's inner atomic orbitals rather than being emitted in the decay: 
\begin{equation}
  p^+ + e^- \rightarrow n + \nu_e \, .
\end{equation}

The hole in the atom's inner shells created by the capture of the electron often results in X-rays or Auger electrons accompanying the decay as electrons in its higher energy orbitals transition to the lower levels to fill the vacancy. The similarity between the nucleonic part of this decay and that of the $\beta^+$ decay shown in Equation \ref{eq:betaplus} results in EC decay occurring in every isotope which undergoes $\beta^+$ decay and in some cases where $\beta^+$ decay is energetically forbidden it can be the only decay mode of the isotope.
\subsection{The semi-empirical mass formula}
For $\beta$ decays to occur the mass of the daughter isotope must be smaller than its parent's, as energy is required for the creation and emission of the leptons. Nuclear masses are described, to a good approximation, by the semi-empirical mass formula (SEMF)\cite{Weizscker:1935zz}:
\begin{equation}
  M(A,Z) = Z  m_p + (A-Z)  m_n - E_B \, ,
\label{eq:semf}
\end{equation}
\noindent where $A$ and $Z$ are the atomic and mass numbers, respectively; $m_p$ and $m_n$ the masses of the proton and the neutron; and $E_B$ is the binding energy of the nucleus, given by:
\begin{equation}
  E_B = a_V A - a_s A^{2/3} - a_c \frac{Z^2}{A^{1/3}} - a_A \frac{(A - 2 Z)^2}{A} - \delta (A, Z) \, .
  \label{eq:binding}
\end{equation}

The terms in the expression for the binding energy written above represent corrections to the simplistic value given by the first two terms in Equation \ref{eq:semf}. The correction terms are, by the order they are shown in Equation \ref{eq:binding}: the volume term; the surface term; the Coulomb term; the asymmetry term; and the pairing term. The latter term, which takes into account the pairing between the nucleons' spins is given by:
\begin{equation}
  \delta (A,Z) = 
  \begin{cases}
    -\frac{a_P}{A^{1/2}} & \text{even $Z$ and $A$}  \\
    0         & \text{odd $A$}  \\
    +\frac{a_P}{A^{1/2}} & \text{odd $Z$, even $A$}
  \end{cases}
  \, .
\end{equation}

\begin{figure}[htpb]
  \def\svgwidth{\mediumfigwidth}
  \import{chapters/double-beta-decay/}{SEMFCurves.pdf_tex}
  \caption[Masses of even $A$ nuclei, as described by the SEMF.]{Masses of even $A$ nuclei, as described by the SEMF. Two parabolas are shown, one which gives the masses of odd $Z$ nuclei (white circles) and the other for even $Z$ nuclei (black circles). Examples of allowed decays are shown\cite{motty}.}
  \label{fig:semf-curves}
\end{figure}

For nuclei with the same $A$, the masses obtained with the SEMF lie in parabolas as a function of $Z$, due to the Coulomb and asymmetry terms. Masses of nuclei with odd $A$ are described by a single parabola, while in the case of even $A$ nuclei, the pairing term splits the masses into two parabolas, depending on whether $Z$ is even or odd (Figure \ref{fig:semf-curves}).

\section{Two-neutrino double-$\beta$ decay}
\label{sec:twonu}
In Figure \ref{fig:semf-curves} it is shown that some nuclei with even $A$ and $Z$ are stable against the $\beta$ decays described above (\emph{e.g.}, the nucleus labelled (c)). They can, however, undergo double-$\beta$ decay: while a single $\beta$ decay is forbidden by energy considerations, a process which changes $Z$ by two units is allowed:
\begin{equation}
  (Z,A) \rightarrow (Z,A+2) + 2 e^- + 2\bar{\nu}_e \, .
\end{equation}
This process (\2nu), which occurs via a $\beta^-$ decay to an intermediate, virtual nucleus followed by a $\beta^-$ decay to the final state, was first described by Maria Goeppert-Mayer in 1935\cite{GoeppertMayer:1935qp}. The Feynman diagram for \2nu is shown in Figure \ref{fig:2nu-feyn}, where it is clear that the process is allowed by the SM, with no violation of its quantum numbers or non-standard vertices.
\begin{figure}[htpb]
  \parbox{100mm}{
    \centering
    \vspace*{5mm}
    \unitlength=1mm
    \begin{fmffile}{2b2n}
      \begin{fmfgraph*}(75,40)
         \fmfstraight
         \fmfleft{in1,in2}
         \fmfright{op1,e1,n1,n2,e2,op2}
         \fmf{plain}{in1,nw1,d0,d1,op1}
         \fmf{plain}{in2,nw2,d2,d3,op2}
         \fmf{phantom}{in1,op1}
         \fmf{phantom}{in2,op2}
         \fmffreeze
         \fmf{photon,tension=2.0,label=$W^{-}$, label.side=left}{nw1,ve1}
         \fmf{photon,tension=2.0,label=$W^{-}$, label.side=right}{nw2,ve2}
         \fmf{phantom}{ve1,ve2}
         \fmf{fermion,tension=0.5}{n1,ve1}
         \fmf{fermion,tension=0.5}{ve1,e1}
         \fmf{fermion,tension=0.5}{n2,ve2}
         \fmf{fermion,tension=0.5}{ve2,e2}
         \fmflabel{$n$}{in1}
         \fmflabel{$n$}{in2}
         \fmflabel{$p$}{op1}
         \fmflabel{$p$}{op2}
         \fmflabel{$e^-$}{e1}
         \fmflabel{$e^-$}{e2}
         \fmflabel{$\bar{\nu}_e$}{n1}
         \fmflabel{$\bar{\nu}_e$}{n2}
         \fmfi{fermion}{vpath (__in1,__op1) shifted (thick*(0,-2))}
         \fmfi{plain}{vpath (__in1,__op1) shifted (thick*(0,-4))}
         \fmfi{fermion}{vpath (__in2,__op2) shifted (thick*(0,2))}
         \fmfi{plain}{vpath (__in2,__op2) shifted (thick*(0,4))}
      \end{fmfgraph*}
    \end{fmffile}
    \vspace*{5mm}
  }
  \caption[Feynman diagram for the two-neutrino double-$\beta$ decay.]{Feynman diagram for the two-neutrino double-$\beta$ decay.}
  \label{fig:2nu-feyn}
\end{figure}
Experiments designed to investigate double-$\beta$ decay measure, in general, the sum of the energies of the two electrons. Since part of the decay's energy is carried away by the undetected neutrinos, the sum of the electrons' energies is distributed over a continuous spectrum (Figure \ref{fig:bb-spectra}). The end-point of the spectrum is at the Q-value for the decay, defined as the energy available to the decay products:
\begin{equation}
  Q_{\beta\beta} = M(A,Z) - M(A,Z+2) - 2 m_e \, .
\end{equation}

The \2nu transition rate is given by Fermi's golden rule:
\begin{equation}
  \left( t^{1/2}_{2\nu} \right) ^{-1} = G^{2\nu} g_A^4 \left| M^{2\nu} \right| ^2 \, ,
\end{equation}
\noindent where $t^{1/2}_{2\nu}$ is the decay's half-life, $G^{2\nu}$ is the phase-space factor, $g_A^4$ is the axial-vector coupling constant and $\left|M^{2\nu}\right|$ is the nuclear matrix element.

The phase space factor can be calculated exactly. These factors vary widely for different isotopes and in particular depend strongly on the transition's $Q_{\beta\beta}$, with\cite{doi:10.1146/annurev-nucl-102711-094904}:
\begin{equation}
 G^{2\nu}  \propto Q_{\beta\beta}^{11} \, .
\end{equation}

The nuclear matrix elements, however, present a difficult calculation problem, with different approximations giving a wide range of values. Measured half-lives of \2nu are used to constrain effective nuclear model parameters used in the calculations. While this allows for improvements in the precision of calculations of the nuclear matrix elements for neutrinoless double-$\beta$ decay, in general the relationship between the calculations for the two modes is not straightforward.

In the case of \Ca{48}, the calculations are simpler compared to the other, heavier, double-$\beta$ decay isotopes. While precise shell model descriptions of the \Ca{48} nucleus are computationally accessible, having provided relatively accurate predictions of its energy levels\cite{post_erratum}, the choice of model for the effective interaction between nucleons results in a wide range of predictions. This is discussed in more detail in Section \ref{sec:2nu-disc} of this thesis.

Other double-$\beta$ decay modes exist in the SM, namely double-$\beta^+$, double-EC and EC$\beta^+$. However, these modes have smaller Q-values and hence smaller transition amplitudes, and challenging final state topologies, making their observation difficult\cite{doi:10.1146/annurev-nucl-102711-094904}.

\section{Neutrinoless double-$\beta$ decay}
\label{sec:0nu}
In several beyond the SM theories, double-$\beta$ decay can proceed without the emission of neutrinos:
\begin{equation}
  (Z,A) \rightarrow (Z,A+2) + 2 e^- \, .
\end{equation}

Such a process would violate lepton number (a SM accidental symmetry) by two units. While this in itself motivates \0nu searches, the observation of neutrino masses through flavour oscillations coupled with a theoretical preference for the Majorana nature of the neutrino (Section \ref{sec:maj_mass_term}) provide an ever stronger incentive for the investigation of this process: the existence of massive Majorana neutrinos would induce \0nu via an exchange of neutrinos between the $W$ bosons in the so called mass mechanism.

While the exchange of Majorana neutrinos is the most well motivated \0nu mode, this decay is also predicted by R-parity violating ($\slashed{R}_p$) supersymmetry and left-right symmetric extensions to the SM\cite{Rodejohann:2012xd, supernemoPheno}.

Even though the Majorana nature of neutrinos does not appear to be, \emph{a priori}, a requisite for \0nu modes other than the mass mechanism, it has been demonstrated\cite{SchechterValle} that the existence of any \0nu mode would imply an effective Majorana mass term, as shown in Figure \ref{fig:black-box}. Generic estimates of the mass arising from such a mechanism result in masses too small to explain the neutrino mass splittings observed in oscillation experiments\cite{Rodejohann:2012xd}. This reinforces the standard interpretation of \0nu as being likely to be mediated by the exchange of massive Majorana neutrinos, if it occurs.
\begin{figure}[htpb]
  \def\svgwidth{\mediumfigwidth}
  \import{chapters/double-beta-decay/}{BlackBox.pdf_tex}
  \caption[Majorana mass term due to \0nu.]{Majorana mass term due to \0nu. A ``black-box'' \0nu process is shown converting a $\bar{\nu}$ into a $\nu$, generating a Majorana mass term\cite{SchechterValle}.}
  \label{fig:black-box}
\end{figure}

Other non-standard modes of \0nu occur in theories which predict massless (or very light) neutral particles (Majorons) that couple to neutrinos. In this case the decay involves the emission of one or more Majoron, with the kinematics of the electrons emitted in this decay being significantly different from those of the decay modes mentioned above \cite{Rodejohann:2012xd,Bamert199525}.

% - [[ HERE PUT TABLE FROM RODEJOHANN RE ISOTOPES ]]

\subsection{Mass mechanism}
In the standard interpretation of \0nu, the decay proceeds through the exchange of light Majorana neutrinos, as shown in Figure \ref{fig:feyn-mass-mech}.
% - Feynman diagram
\begin{figure}[htpb]
  \parbox{100mm}{
    \centering
    \vspace*{5mm}
    \unitlength=1mm
    \begin{fmffile}{2b0nMassMechanism}
      \begin{fmfgraph*}(75,40)
         \fmfstraight
         \fmfleft{in1,in2}
         \fmfright{op1,e1,n1,n2,e2,op2}
         \fmf{plain}{in1,nw1,d0,d1,op1}
         \fmf{plain}{in2,nw2,d2,d3,op2}
         \fmf{phantom}{in1,op1}
         \fmf{phantom}{in2,op2}
         \fmffreeze
         \fmf{photon,tension=2.0,label=$W_L$, label.side=left}{nw1,ve1}
         \fmf{photon,tension=2.0,label=$W_L$, label.side=right}{nw2,ve2}
         \fmf{fermion, label=$\nu_L$, label.side=left}{vx,ve1}
         \fmf{fermion, label=$\bar{\nu}_R$}{vx,ve2}
         \fmfv{decor.shape=cross}{vx}
         \fmf{phantom,tension=0.5}{n1,ve1}
         \fmf{fermion,tension=0.5}{ve1,e1}
         \fmf{phantom,tension=0.5}{n2,ve2}
         \fmf{fermion,tension=0.5}{ve2,e2}
         \fmflabel{$n$}{in1}
         \fmflabel{$n$}{in2}
         \fmflabel{$p$}{op1}
         \fmflabel{$p$}{op2}
         \fmflabel{$e^-$}{e1}
         \fmflabel{$e^-$}{e2}
         \fmfi{fermion}{vpath (__in1,__op1) shifted (thick*(0,-2))}
         \fmfi{plain}{vpath (__in1,__op1) shifted (thick*(0,-4))}
         \fmfi{fermion}{vpath (__in2,__op2) shifted (thick*(0,2))}
         \fmfi{plain}{vpath (__in2,__op2) shifted (thick*(0,4))}
      \end{fmfgraph*}
    \end{fmffile}
    \vspace*{5mm}
  }
  \caption[Feynman diagram for the mass mechanism \0nu.]{Feynman diagram for the mass mechanism \0nu.}
  \label{fig:feyn-mass-mech}
\end{figure}
The decay rate for this process acquires a dependency on the effective mass of the exchanged neutrino due to the spin-flip required for coupling to the SM left-handed $W$:
\begin{equation}
  \left(t^{1/2}_{0\nu}\right)^{-1} = G^{0\nu} g_A^4 \left| M^{0\nu} \right| ^2 \braket{m_{\beta\beta}}^2 \, ,
\end{equation}
\noindent where $G^{0\nu}$ and $\left| M^{0\nu} \right|$ are the phase space factor and nuclear matrix element, and the effective Majorana mass, $\braket{m_{\beta\beta}}$ is a sum of the neutrino masses weighted by the elements of the PMNS matrix, as given in Equation \ref{eq:eff-maj-mass}. The dependence of the phase space factor on $Q_{\beta\beta}$ is weaker for this decay mode compared to \2nu\cite{doi:10.1146/annurev-nucl-102711-094904}:
\begin{equation}
 G^{0\nu}  \propto Q_{\beta\beta}^{5} \, .
\end{equation}

As with the case of \2nu, the phase space factor depends strongly on the isotope considered, with values spanning more than one order of magnitude, with \Nd{150} having the largest factor and \Ge{76} the smallest\cite{0034-4885-75-10-106301}. The effect of this (and of differences in the nuclear matrix elements) on the sensitivity to $\braket{m_{\beta\beta}}$ is illustrated in Figure \ref{fig:hl-50mev}.

Due to the absence of neutrinos in the final state to carry away energy, the energies of the electrons emitted in the decay sum up to $Q_{\beta\beta}$, producing a sharp peak in the spectrum (Figure \ref{fig:bb-spectra}).

\begin{figure}[htpb]
  \def\svgwidth{\mediumfigwidth}
  \import{chapters/double-beta-decay/}{EnergySpectrum.pdf_tex}
  \caption[Summed energy spectrum of double-$\beta$ decay.]{Summed energy spectrum of double-$\beta$ decay. The \0nu mode is shown with an exaggerated rate of 1\% of the \2nu rate. Both spectra are smeared to mimic an energy resolution of 2\% \cite{RevModPhys.80.481}.}
  \label{fig:bb-spectra}
\end{figure}

Extracting limits on the effective Majorana mass from experimental half-life limits requires the evaluation of the nuclear matrix elements. This is discussed in \mbox{Section \ref{sec:nme}}.

\subsection{$\slashed{R}_p$ supersymmetry}
To accommodate the very long experimental limits on the life-time of the proton\cite{Nishino:2012ipa}, interactions in the minimal supersymmetric model conserve R-parity, defined as:
\begin{equation}
R_p = (-1)^{3 B + L + 2 s} \, ,
\end{equation}
\noindent where $B$ is the baryon number, $L$ is the lepton number and $s$ is the spin. This quantity takes the value 1 for SM particles and -1 for their supersymmetric partners.

The requirement that $R_p$ is conserved in all interactions forbids lepton number violating processes. However, if R-parity is broken ($\slashed{R}_p$), \0nu can proceed, for example, via the exchange of a neutralino or a gluino, with the transition amplitude governed by the trilinear coupling $\lambda_{111}'$\cite{Rodejohann:2012xd}. The strictest limits on this coupling have been obtained in \0nu searches\cite{Barbier:2004ez}. This is discussed further in \mbox{Section \ref{sec:0nu-lims}}.


\subsection{Right handed currents}
In left-right symmetric models, proposed as an alternative to the maximally parity-violating $V-A$ structure of the weak interaction, \0nu is possible without the neutrino spin-flip required in the mass mechanism. This decay mode is therefore not dependent on the neutrino mass, but on the couplings between the right- and left-handed fermions.

Two possibilities exist: the SM $W$ might itself be a superposition of right-handed and left-handed states, governed by a mixing angle $\epsilon$; or a new (heavy) right-handed $W'$ boson might exist\cite{Rodejohann:2012xd}.

In the first case, the rate of \0nu depends on the $\braket{\lambda}$ parameter, which represents the coupling between right-handed quarks and right-handed leptons (Figure \ref{fig:feyn-0nu-rhc-lambda}). This decay mode differs significantly from the other modes discussed here in that it produces a large asymmetry in the individual energies of the emitted electrons.
\begin{figure}[htpb]
  \parbox{100mm}{
    \centering
    \vspace*{5mm}
    \unitlength=1mm
    \begin{fmffile}{2b0nRHCurrent}
      \begin{fmfgraph*}(75,40)
         \fmfstraight
         \fmfleft{in1,in2}
         \fmfright{op1,e1,n1,n2,e2,op2}
         \fmf{plain}{in1,nw1,d0,d1,op1}
         \fmf{plain}{in2,nw2,d2,d3,op2}
         \fmf{phantom}{in1,op1}
         \fmf{phantom}{in2,op2}
         \fmffreeze
         \fmf{photon,tension=2.0,label=$W_R$, label.side=left}{nw1,ve1}
         \fmf{photon,tension=2.0,label=$W_L$, label.side=right}{nw2,ve2}
         \fmf{fermion, label=$\nu_R$, label.side=left}{vx,ve1}
         \fmf{fermion, label=$\bar{\nu}_R$}{vx,ve2}
         \fmfv{decor.shape=cross}{vx}
         \fmf{phantom,tension=0.5}{n1,ve1}
         \fmf{fermion,tension=0.5}{ve1,e1}
         \fmf{phantom,tension=0.5}{n2,ve2}
         \fmf{fermion,tension=0.5}{ve2,e2}
         \fmflabel{$n$}{in1}
         \fmflabel{$n$}{in2}
         \fmflabel{$p$}{op1}
         \fmflabel{$p$}{op2}
         \fmflabel{$e^-$}{e1}
         \fmflabel{$e^-$}{e2}
         \fmfi{fermion}{vpath (__in1,__op1) shifted (thick*(0,-2))}
         \fmfi{plain}{vpath (__in1,__op1) shifted (thick*(0,-4))}
         \fmfi{fermion}{vpath (__in2,__op2) shifted (thick*(0,2))}
         \fmfi{plain}{vpath (__in2,__op2) shifted (thick*(0,4))}
      \end{fmfgraph*}
    \end{fmffile}
    \vspace*{5mm}
  }
  \caption[Feynman diagram for the $\braket{\lambda}$ mode of \0nu arising from left-right symmetric models.]{Feynman diagram for the $\braket{\lambda}$ mode of \0nu arising from left-right symmetric models.}
  \label{fig:feyn-0nu-rhc-lambda}
\end{figure}

The other \0nu mode arising from left-right symmetric models is described by the $\braket{\eta}$ parameter, which is related to the coupling between left-handed quarks and right-handed leptons.

A more detailed discussion on the constraints on these models is given in \mbox{Section \ref{sec:0nu-lims}}.

\subsection{Majoron emission}
Majorons ($\chi^0$) have been proposed as massless Goldstone bosons for theories where a $B-L$ symmetry is spontaneously broken. Most of these models predicted additional couplings to the $Z$ boson and have thus been excluded by LEP measurements (Section \ref{sec:nu-flavour}). Due to the smallness of the neutrino masses, the surviving models require substantial fine tuning in order to have observational consequences. However, alternative models have been proposed where \0nu occurs with the emission of one or two Majorons, where these particles are not required to be massless or Goldstone bosons and in some cases carry a non-zero $L$, resulting in lepton number conserving \0nu\cite{Bamert199525,Rodejohann:2012xd}. 

The kinematics of such decays are described in terms of a spectral index $n$, related to the phase-space factor by:
\begin{equation}
  G^{0\nu\chi^0} \propto (Q_{\beta\beta} - E_{e_1} - E_{e_2})^n \, ,
\end{equation}
\noindent where $E_{e_i}$ are the energies of the electrons.

The \0nu mode with the emission of one Majoron with spectral index $n=1$ is investigated in this thesis (Section \ref{sec:0nu-lims}):
\begin{equation}
  (Z,A) \rightarrow (Z,A+2) + 2 e^- + \chi^0 \, .
\end{equation}

\section{Nuclear matrix elements}
\label{sec:nme}
To extract the physical parameters of interest from \0nu searches, namely $\braket{m_{\beta\beta}}$ for the mass mechanism and the coupling strengths relevant for other mechanisms, the quantification of the nuclear matrix elements is necessary. The lack of other observables related to these amplitudes means that they must be calculated in the theoretical framework of nuclear structure. A precise knowledge of the nuclear matrix elements is also important for the identification of the isotopes which are most suitable for constraining the parameters of interest.

Calculations of nuclear matrix elements generally consist of two parts: the calculation of the two-body transition matrix element for the two interacting nucleons; and the construction of multi-body wave functions for the initial, final and, in cases where the closure approximation is not used, intermediate (virtual) nuclei.

Given the complexity of such calculations, approximations and simplifications must be assumed, different choices of which result in a range of values for each nuclear matrix element. Five approaches have been developed to tackle these calculations\cite{0034-4885-75-10-106301}:
\begin{itemize}
  \item the interacting shell model (ISM);
  \item the quasi-particle random phase approximation (QRPA);
  \item the interacting boson model (IBM);
  \item the projected Hartree-Fock-Bogoluibov method (PHFB);
  \item the energy density functional method (EDF).
\end{itemize}

In the ISM a limited number of nuclear orbits are considered, but all correlations within the space are included. Calculations in the ISM framework tend to result in smaller matrix elements when compared to the other approximations. These calculations are only adequate for smaller nuclei such as \Ca{48} as the computational power required for isotopes with larger valence spaces makes these calculations impractical and nuclear deformation is not well modelled. This approach has had success in predicting the nuclear energy levels in agreement with spectroscopy data\cite{post_erratum}.

The QRPA method is complementary to the ISM in that it is particularly suitable for larger nuclei. In this approach, calculations are performed over large valence spaces, but not all possible configurations are taken into account and the interactions between nucleons are approximated. This method depends on a number of free parameters that must be constrained elsewhere. In particular the determination of the particle-particle strength of the Hamiltonian $g_{pp}$ from \2nu measurements can be used to reduce the uncertainty on the \0nu matrix element calculations.

The IBM approach is similar to the ISM. However, in this method the low-lying nuclear states are treated as bosons in states with $L = $ 0 or 2 ($s$ and $d$ boson states, respectively). While the IBM and ISM methods are expected to produce the same results in the limit of spherical nuclei, calculations have resulted in disagreement between the two methods\cite{0034-4885-75-10-106301}.

The PHFB method has the advantage of using few parameters for the calculation of both the \2nu and the \0nu matrix elements. This approach involves a simplification of the nuclear Hamiltonian in which only the quadrupole terms are taken into account and only neutron pairs with even angular momentum and positive parity are described. The EDF method is an improvement on the PHFB method in which the Gogny interaction\cite{PhysRevC.21.1568} is used.


\subsection{Comparison of calculations}
A comparison between the matrix elements for 11 isotopes calculated with the methods outlined above is shown in Figure \ref{fig:nmes}. The ISM calculations are shown to give similar results for all isotopes with the exception of \Ca{48}, with the values being systematically smaller compared to other techniques. Even disregarding the ISM calculations for the heavier nuclei, the different methods give a wide range of results for most nuclei, with the possible exception of the Te isotopes where the calculations for the other four methods agree with each other. It should also be noted that given the dispersion of values for each isotope, it is not clear which isotopes have the most favourable nuclear matrix elements for \0nu searches, with the transition amplitudes being heavily affected by differences in the phase space factors (Figure \ref{fig:hl-50mev}).
 
\begin{figure}[htpb]
  \def\svgwidth{\largefigwidth}
  \import{chapters/double-beta-decay/}{MatrixElements.pdf_tex}
  \caption[Comparison between the nuclear matrix elements for 11 isotopes calculated in five different frameworks.]{Comparison between the nuclear matrix elements for 11 isotopes calculated in five different frameworks\cite{motty} (with nuclear matrix elements from \cite{0034-4885-75-10-106301}). Two QRPA sets calculations are given: one by the T\"ubingen group (T) and the other by the Jyv\"askyl\"a group (J).}
  \label{fig:nmes}
\end{figure}

\begin{figure}[htpb]
  \def\svgwidth{\largefigwidth}
  \import{chapters/double-beta-decay/}{vergados-plot.pdf_tex}
  \caption[Comparison between the \0nu half-lives of 11 isotopes assuming an effective Majorana mass of $\braket{m_{\beta\beta}}=50$\meV.]{Comparison between the \0nu half-lives of 11 isotopes assuming an effective Majorana mass of $\braket{m_{\beta\beta}}=50$\meV. The methods used for the nuclear matrix element calculations are given in the legend\cite{0034-4885-75-10-106301}. }
  \label{fig:hl-50mev}
\end{figure}
