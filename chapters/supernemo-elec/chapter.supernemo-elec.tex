\chapter{Electronics for surface commissioning}
\label{sec:ch-elec}
As described in Section \ref{sec:sn-tracker-const}, the SuperNEMO tracker is built in C-section units (\mbox{$\frac{1}{4}$ of the tracker}). Before the C-sections are shipped to LSM, where they are integrated with each other and the other components of the detector, the C-sections undergo surface commissioning using cosmic rays at MSSL.

Given that in the final configuration the open faces of the tracker volume are sealed by the main calorimeter walls, temporary sealing plates were built such that the tracker can be filled with gas. This allows for leak testing of the permanent components of the frame, for the \Rn{222} emanation measurements to take place and for the tracker to be operated independently of the other detector modules. The sealing plates are also part of the transportation structure which allows the tracker C-sections to be transported over to the French-Italian Alps without contamination of their internal volume.

The surface commissioning of the tracker will take place with the C-sections on their side, \emph{i.e.}, with the $x$ coordinate (as shown in Figure \ref{fig:sn-c-section-sketch}) along the vertical direction. The C-section calorimeter modules are tested independently of the tracker, prior to installation in the C-section, in a dedicated test stand at UCL.

This chapter describes the electronics for the surface commissioning of C-sections. The main components of the system are:
\begin{itemize}
  \item the HV supply and distribution;
  \item the cosmic ray trigger;
  \item the VME DAQ cards;
  \item the DAQ computer and the VME controller.
\end{itemize}

The HV distribution system is recommissioned from NEMO-3, while the power supply itself has been newly acquired for the commissioning. The DAQ system has also been partially recovered from NEMO-3, namely the bespoke read-out VME cards, with the VME controller being a new off-the-shelf component. The cosmic ray trigger is comprised of two reused 1 $\times$ 1 m$^2$ scintillator planes from the MINOS calibration detector\cite{Adamson2006119}, a custom made board hosting a field-programmable gate array chip (FPGA) and Nuclear Instrumentation Modules (NIM). The system is completed by a rack-mounted computer which communicates with the trigger board and the VME controller. A detailed diagram of the entire system is shown in \mbox{Figure \ref{fig:comm-elec-sketch}}.

\begin{figure}[htpb]
  \def\svgwidth{\textwidth}
  \import{chapters/supernemo-elec/}{electronics-diagram.pdf_tex}
  \caption[Schematic diagram of the tracker commissioning electronics.]{Schematic diagram of the tracker commissioning electronics. The physical arrangement of the C-section and the cosmic trigger scintillator planes is shown on the bottom of the diagram, with the scintillator planes in blue, PMTs in grey and the tracker cells as drawn in Figure \ref{fig:sn-cell-sketch}, with the field shaping wires omitted. On the top, the detailed interactions between the system's components are described. The types of connections used are described in the legend, as is the provenance of the main components.}
  \label{fig:comm-elec-sketch}
\end{figure}


\section{HV supply and distribution}
The HV for the surface commissioning system is provided by a \emph{CAEN SY1527} modular system, equipped with negative and positive polarity modules. The negative polarity modules provide HV to the cosmic ray trigger PMTs while the positive polarity channels are used to power the tracker cells.

The positive polarity module installed in the \emph{CAEN} system provides 24 HV channels, each with a maximum voltage of 3.5 kV and with a power rating of 8 W. In normal operation, with a voltage under 2 kV, tracker cells draw up to 4 \micro A of current. When in an unstable, self-discharging mode the current drawn by each cell can increase by one order of magnitude. Each HV channel can therefore feed up to 50 cells. This allows for an entire C-section to be comfortably powered by a single \emph{CAEN} module, including spare capacity for other purposes, if necessary.

The system can be controlled locally or remotely via an Ethernet connection. The latter allows for interactive control through a Telnet session or for automation with a {\texttt C} library provided by \emph{CAEN}.

Safe high voltage (SHV) cables connect the \emph{CAEN} module to the backplane of a NEMO-3 HV distribution crate (Figure \ref{fig:hv-sketch}), which is populated with up to 20 HV distribution cards, although only 16 are expected to be necessary for the commissioning of the C-sections. Each HV distribution card (HV motherboard) can be supplied by up to three different HV channels, with the mapping of the HV channels to motherboards being defined by the adjustable backplane of the HV distribution crate.

\begin{figure}[htpb]
  \def\svgwidth{\textwidth}
  \import{chapters/supernemo-elec/}{hv-diagram.pdf_tex}
  \caption[The NEMO-3 HV distribution and decoupling system.]{The NEMO-3 HV distribution and decoupling system. Clockwise from the top left corner: sketch of a NEMO-3 HV crate populated with the number of cards required for the commissioning of C-sections; detailed diagram of an HV motherboard in one of the possible HV distribution configurations; sketch of a daughterboard showing a common HV being distributed across eight channels; diagram of the anode signal decoupling circuit.}
  \label{fig:hv-sketch}
\end{figure}

The HV motherboards are populated with daughterboards which distribute the HV to individual cells and decouple the signal pulses from the HV lines. The daughterboards are arranged in sets of three: two for the pick-up cathode rings and one for the anode wire\footnote{Even though the pick-up rings were kept at ground in NEMO-3, the electronics were designed such that a voltage up to 300 V could be applied to them.}. Each daughterboard contains eight decoupling circuits, each of them supplying one tracker cell with HV (or ground, in the case of the pick-up rings) and extracting the relevant signal. The decoupling circuits are high-pass $RC$ filters with a cut-off frequency of around 400 Hz which allow the signal pulses through to the read-out electronics but block the HV. The decoupled signals from both the anode and cathode daughterboards are routed to connectors on the front face of the HV motherboards, which are in turn connected to the DAQ boards with coaxial signal cables (Figure \ref{fig:comm-elec-sketch}).

The high voltage coaxial cables that carry the HV to the cells are directly soldered on to the decoupling circuits on the daughterboards. The detector ends of the cables were adapted to use in the SuperNEMO tracker feedthroughs fitted to the C-sections.

\section{Cosmic ray trigger}
The cosmic ray trigger prepared for the commissioning of the tracker consists of two MINOS calibration detector scintillator planes. These will be placed above and below the C-section, with a coincident signal on both scintillator planes indicating the traversal of the C-section by a charged particle. Even though each scintillator plane is segmented in thin strips, the light from all strips is collected in a single optical fibre bundle which is interfaced with one PMT. The PMT analogue signals are put through a NIM discriminator unit which in turn produces NIM logic pulses with a predefined width when the analogue signals exceed a preset threshold. The discriminated signals are then fed to a NIM coincidence unit which produces a single NIM pulse if the two signals overlap. Before being sent to the FPGA trigger board the coincidence trigger pulse is converted from a 50 $\Omega$ NIM standard signal to a high-impedance TTL pulse (Figure \ref{fig:comm-elec-sketch}). The PMTs which instrument the scintillator planes operate at around \mbox{1800 V}, with a coincidence rate of around \mbox{50 Hz}.

A custom made trigger board hosting an FPGA receives the TTL trigger pulse and produces a set of signals required by the NEMO-3 DAQ cards\cite{samer}. The board is interfaced with the DAQ cards via the VME backplane, with the VME controller by coaxial cables and with the DAQ computer via USB (Figure \ref{fig:comm-elec-sketch}). Through the VME backplane the FPGA sends a set of three pulses (\texttt{STOP-T/O}, \texttt{STOP-A} and \texttt{STOP-M}) with predefined delays to all the DAQ cards. The last of these signals, \texttt{STOP-M} is also sent to the VME controller where it is used to initiate the read-out of data to disk. Upon receiving a trigger signal the FPGA is set in an inhibited mode in which it does not react to further trigger pulses. The system is reverted back to its active mode only after the read-out is complete and a \texttt{CLEAR INHIBIT} pulse is sent from the VME controller. The FPGA is powered via the USB connection to the computer, which is also used to configure the delays of the \texttt{STOP-A} and \texttt{STOP-M} pulses relative to the trigger signal.

\section{DAQ cards}
\label{sec:sn_n3_elec}
The NEMO-3 tracker DAQ cards match one-to-one with the HV motherboards, with their front faces having the same connectors and compatible pin maps.

Each DAQ card contains ten pairs of bespoke application-specific integrated circuits (ASICs), each pair consisting of an analogue ASIC and a digital one. Each pair of ASICs reads out four tracker cells, giving a DAQ card the same capacity as the HV distribution boards: 40 cells.

The decoupled signals are routed from the front face of the cards to the analogue ASICs where they are amplified and fed to a comparator together with a reference threshold voltage. On each ASIC a parameter can be adjusted which scales the threshold voltages on all four channels for both the anode and cathode ring signals. If the signals exceed the threshold, a TTL pulse is sent to the corresponding digital ASIC's channel.

A digital ASIC consists of a total of 16 time-to-digital converters (TDCs): four for each tracker cell. Three of the TDCs are 12-bit and, given the 50 MHz clock of the DAQ cards, can record times up to 82\us. The third TDC has a higher capacity of 17 bits or 2.6 ms. The first TDCs are used to record the arrival times of the prompt anode signals (TDC$_{\text{Anode}}$) and the propagation time for each cathode ring (TDC$_{\text{C1}}$ and TDC$_{\text{C2}}$) while the latter (TDC$_{\alpha}$) is used to record delayed tracker hits for tagging BiPo type events.

\begin{figure}[htpb]
  \def\svgwidth{\textwidth}
  \import{chapters/supernemo-elec/}{timing.pdf_tex}
  \caption[Timing diagram for the tracker commissioning DAQ system.]{Timing diagram for the tracker commissioning DAQ system. The signal lines of the DAQ components are shown colour coded, as defined in the legend. The causal relationship between the pulses is shown with dotted arrows. Two sample sets of pulses from a tracker cell are shown: one prompt and one delayed.}
  \label{fig:timing}
\end{figure}

As a passing particle is detected by the trigger system a \texttt{STOP-T/O} pulse is sent to all DAQ cards in the crate. This signal interrupts the periodic time-out which resets all TDCs in the cards every 100\us. This time-out minimises the chances of tracker hits unrelated to a triggered event (\emph{e.g.}, a spurious discharge in a cell) being read out as part of the event. After this, two more signals are produced by the trigger system and fed to all cards: a \texttt{STOP-A} pulse, with a preset delay ($\Delta_{\texttt{STOP-A}}$) of around 7\us; and a \texttt{STOP-M} pulse, delayed by a much longer time ($\Delta_{\texttt{STOP-M}}$) of around 700\us (Figure \ref{fig:timing}).

If an anode signal arrives before the \texttt{STOP-A} signal is received, all 12-bit TDCs are started: TDC$_{\text{Anode}}$; TDC$_{\text{C1}}$; and TDC$_{\text{C2}}$. TDC$_{\text{Anode}}$ is stopped by the arrival of \texttt{STOP-A}, while TDC$_{\text{C}i}$ are stopped by the detection of the relevant signals from the cell's pick-up rings. Therefore, the arrival time of the anode pulse relative to the trigger signal, \emph{i.e.}, the drift time of avalanches which allows for the radial position of the hit to be calculated, is given by $t_{Anode} = \Delta_{\texttt{STOP-A}} - t_{\text{TDC}_{\text{Anode}}}$, where $t_{\text{TDC}_{\text{Anode}}}$ is the time measured by TDC$_{\text{Anode}}$. The arrival times of the pick-up rings relative to the anode pulse, which allow for the calculation of the longitudinal coordinate of the tracker hit, are directly given by the time recorded by the relevant TDCs: $t_{Prop_i} = t_{\text{TDC}_{\text{C}i}}$. If no cathode ring pulses are detected, the TDC$_{\text{C}i}$ are stopped by the arrival of \texttt{STOP-M}.

If, on the other hand, a hit on the anode wire occurs after \texttt{STOP-A}, the 17-bit TDC$_\alpha$ is started instead of TDC$_\text{Anode}$, being stopped by the arrival of \texttt{STOP-M}. In this case, no radial position information of the hit is obtained. Instead, the tracker hit is assumed not to be directly correlated to the external trigger with the delay of the hit relative to the trigger being given by: $t_{\alpha} = \Delta_{\texttt{STOP-M}} - t_{\text{TDC}_\alpha}$. This variable is used to identify delayed $\alpha$ emissions from BiPo events. The behaviour and positional information from the cathode ring TDCs is the same as in the prompt-hit case described above. It should be noted that a given channel cannot have both prompt and delayed hits in a single event. The physical dead time of a tracker cell is typically longer than the read-out window ($\Delta_{\texttt{STOP-M}}$) and the DAQ cards are not designed to keep to a second hit in the same event. 

Each digital ASIC contains a status register consisting of a bit-mask which specifies which, if any, of the TDCs have been started in the group of four channels. Furthermore, each DAQ card has a status register indicating which, if any, of the ASICs has been activated in the triggered event. This information is used in the software read-out loop to decide which data is written to disk.

\section{VME controller and read-out cycle}
The DAQ rack-mounted computer communicates with the rest of the DAQ system via a USB connection to the \emph{CAEN} controller on the VME crate. Upon receiving the \texttt{STOP-M} signal from the FPGA trigger board, the VME controller changes the state of a line which is fed back to itself, latching the \texttt{STOP-M} signal (Figures \ref{fig:comm-elec-sketch} and \ref{fig:timing}). This is necessary due to the relatively slow software read-out loop, compared to the fast FPGA: producing a \texttt{STOP-M} pulse long enough that it is consistently detected in the software read-out loop is cumbersome at the level of the FPGA.

When the system is in operation, the read-out software remains in a loop, repeatedly querying the state of the \texttt{STOP-M} latch. If the latch has been activated, the read out of the cards is initiated. 

The software instructs the VME controller to retrieve the statuses and TDC counts stored in all cards. While the NEMO-3 DAQ cards do not support the VME \emph{block transfer} mode, which allows for fast data transmission, the \emph{CAEN} VME controller provides a \emph{Multiread} method which allows several registers to be probed in a single USB exchange. It was found that separately querying the status registers and then querying only the registers on which useful data was stored (the contents of activated TDCs) offered no significant advantage over querying all the registers at once (including those with meaningless data). This is thought to be due to both the operating system's process scheduling and the overhead required for USB communication.

Once all the data from the DAQ cards has been transferred to the computer, the status registers are inspected and data regarding channels on which an anode wire pulse has been detected are written to disk in a predefined textfile format.

After the data have been written to disk the read-out code instructs the VME controller to deactivate the \texttt{STOP-M} latch and to send out the \texttt{CLEAR INHIBIT} line which in turn deactivates the \texttt{INHIBIT} line on the trigger board FPGA, leaving the system in its initial, receptive, state (Figure \ref{fig:timing}).

\subsection{Read-out cycle duration and dead time}
Most of the time during a read-out cycle is spent retrieving the data from the DAQ cards. The dead time resulting from a long read-out cycle is not of particular concern for commissioning purposes as the physical dead time of a cell is itself long and the trigger rate from cosmic rays is relatively low. It is nevertheless important to understand the reduction of the system's efficiency resulting from the read-out dead time.

The duration of the read-out cycle was estimated by obtaining time-stamps at the beginning and end of each cycle. The read-out cycle duration was found to increase linearly with the number of DAQ cards in the system, with roughly 1 ms added to the read-out time per card (Figure \ref{fig:time-vs-ncards}).

% With a trigger rate of 50 Hz and an expected number of cards of 16, the dead time due to read-out will result in a significantly reduced rate of read out triggers.
Initial tests, with a trigger rate of 53.4 Hz and using 13 DAQ cards, gave a read-out rate of 28.9 Hz, in good agreement with a simple toy MC simulation (Figure \ref{fig:read-out-rate}). With the full set of 16 DAQ cards the expected read-out time is 19.5 ms, giving a predicted read-out rate of 26.2 Hz.

\begin{figure}[htpb]
        \centering
        \subfloat[Read-out time]{
          \def\svgwidth{\smallfigwidth}
          \import{chapters/supernemo-elec/}{time_vs_nCards.pdf_tex}
          \label{fig:time-vs-ncards}
        }
        \hfill
        \subfloat[$\Delta t$ between consecutive read-out cycles]{
          \def\svgwidth{\smallfigwidth}
          \import{chapters/supernemo-elec/}{plot_dt_data_sim.pdf_tex}
          \label{fig:read-out-rate}
        }
        \caption[Read-out dead time and rate of the tracker commissioning DAQ system.]{Read-out dead time and rate of the tracker commissioning DAQ system. In (a) the read-out time is shown against the number of DAQ cards used and in (b) the time interval between consecutive events read out with 13 cards is compared to a toy MC prediction, showing good agreement.}
\end{figure}

\section{Testing the tracker commissioning DAQ system on the cassette test tank}
\begin{figure}[htpb]
  \centering
  \subfloat[]{
    \def\svgwidth{0.55\textwidth}
    \import{chapters/supernemo-elec/}{manchester-elec-photo.pdf_tex}
  }
  \subfloat[]{
    \def\svgwidth{0.42\textwidth}
    \import{chapters/supernemo-elec/}{P1000292.JPG.pdf_tex}
  }
  \caption[Photograph of the electronics and cosmic ray trigger at the University of Manchester]{Photograph of the UCL electronics and cosmic ray trigger at the University of Manchester. The electronics rack is shown in (a). On the top of the rack the NEMO-3 HV distribution crate is seen, populated with a single HV motherboard, with the DAQ computer below it. The VME crate can be seen below the computer. It is populated with one NEMO-3 DAQ card (left), the FPGA trigger board (middle) and the VME controller (right). The NIM crate where the trigger logic is implemented was housed in a separate rack. It is shown on the bottom of the figure. The arrangement of the small scintillator planes on top of the cassette test tank is shown in (b). The planes are in their \emph{nominal} position, roughly in the middle of the tank on the transverse direction ($x$) and about $\frac{3}{4}$ of the cells' length away from the read out cathode ring in the longitudinal direction ($z$). Only half of the test tank is visible, with the other half being on the far side of the blackened window.}
  \label{fig:manchester-setup-photo}
\end{figure}


The tracker commissioning DAQ system was tested on two tracker prototypes. Initial tests were performed on a single cell prototype at UCL\cite{mine-comm-elec}. This prototype was not built to the final SuperNEMO specifications, with arrangement of the field shaping wires and cell radius being those of NEMO-3 tracker cells. Given the different cell geometry and the limitation of a single read-out channel of the UCL prototype, the commissioning system developed at UCL was transported to the University of Manchester to be tested on SuperNEMO production cells in the cassette test tank.

The testing was carried out over three sessions, during which a significant amount of time was spent troubleshooting the system. The main issues encountered were related to the mapping of the pins on the test tank feedthrough and inappropriate grounding of the cells.

For each testing session, the commissioning electronics were connected to a cassette which had previously been conditioned and had passed the criteria for insertion into the Demonstrator tracker (Section \ref{sec:sn-tracker-const}).

The configuration of the system for the cassette tests differed significantly from the C-section commissioning setup in two aspects:
\begin{itemize}
	\item the cassette test tank is equipped with a feedthrough which is only connected to the anode wire and one of the two pick-up rings, therefore the read-out of one of the cathode pulses is not possible;
	\item smaller scintillator planes were used for the cosmic ray trigger, with a coincidence rate of around 1 Hz and only illuminating a few cells at a time.
\end{itemize}

The two small scintillator planes were placed on top of the test tank, overlapping with each other as much as possible. The planes were nominally positioned in the middle of the test tank in the transverse direction ($x$) and about $\frac{3}{4}$ of the test tank's length away from the read out cathode ring in the longitudinal direction ($z$). 

Photographs of the electronics rack and the arrangement of the cosmic trigger scintillators on the test tank are shown in Figure \ref{fig:manchester-setup-photo}.

\subsubsection{$x$-axis scan}
To validate both the mapping of the physical position of the cells to the DAQ channels and the effectiveness of the cosmic ray trigger, a scan of the cells was performed by moving the scintillator planes along the $x$-axis and taking data in four positions (Figure \ref{fig:manchester-x-scan}).

\begin{figure}[htpb]
  \def\svgwidth{0.7\textwidth}
  \import{chapters/supernemo-elec/}{manchester-setup.pdf_tex}
  \caption[Sketch of a cassette in the test tank.]{Sketch of a cassette in the test tank. The four positions of the scintillator planes for the $x$-axis scan are shown.}
  \label{fig:manchester-x-scan}
\end{figure}
\begin{figure}[htpb]
        \centering
        \subfloat[Position $A$]{
          \def\svgwidth{\smallfigwidth}
          \import{chapters/supernemo-elec/}{occupancy-run19.pdf_tex}
        }
        \subfloat[Position $B$]{
          \def\svgwidth{\smallfigwidth}
          \import{chapters/supernemo-elec/}{occupancy-run20.pdf_tex}
        }

        \subfloat[Position $C$]{
          \def\svgwidth{\smallfigwidth}
          \import{chapters/supernemo-elec/}{occupancy-run21.pdf_tex}
        }
        \subfloat[Position $D$]{
          \def\svgwidth{\smallfigwidth}
          \import{chapters/supernemo-elec/}{occupancy-run22.pdf_tex}
        }
        \caption[Results of the $x$-axis scan of the cassette.]{Results of the $x$-axis scan of the cassette. The hit rate for each cell is given in Hz by the colour scale. The four positions $A$, $B$, $C$ and $D$ correspond to the ones shown in Figure \ref{fig:manchester-x-scan}.}
        \label{fig:x-scan-results}
\end{figure}

The hit rates of the 18 tracker cells in the four configurations of the scintillator planes is shown in Figure \ref{fig:x-scan-results}. The area of highest incidence of hits can be seen shifting from left to right, according to the position of the scintillator planes. This correlation indicates that the trigger is selecting events with cosmic rays traversing the cassette, as expected. These results also validate the map between the DAQ channels and the position of the tracker cells in the cassette. 


\subsubsection{Geiger plateau scan}
As described in Section \ref{sec:sn-gg}, the Geiger plateau is an important characteristic of Geiger detectors. In particular for the SuperNEMO tracker, it is critical that the plateaus of the many tracker cells overlap sufficiently such that all cells can be operated near their optimal point with a limited range of voltages applied to the anode wires. It is thus important to measure the Geiger plateau of the tracker cells during the commissioning of the detector.

To test this ability of the system, a scan was performed on a cassette, with the same voltage applied to all wires, increasing in steps from 1600 to 2000 V. The hit rate on each cell was measured for each step, with the results being shown in Figure \ref{fig:comm-plateau-scan}. Given the uneven illumination of the cells in the cassette (as shown in Figure \ref{fig:x-scan-results}) the plateau curve for each cell was normalised to its nominal hit rate, taken to be the average hit rate at voltages between 1750 and 1900 V. The resulting plateaus can be compared to a scan performed previously with the test tank DAQ system (Figure \ref{fig:manchester-plateau-scan}). It should be noted that the latter system is triggered by individual hits on cells and not by an external cosmic ray trigger. As such, the measured hit rates can be directly compared between different cells with no normalisation required for the plateau to become apparent. Both scans give compatible results, with similar starting points and widths. The earlier scan shows a larger number of cells becoming unstable at the high end of the applied voltage compared to the scan performed later with the commissioning DAQ system. The more stable behaviour of the cassette is likely to be due to the longer conditioning time experienced by the cassette at the time of the tests with the commissioning DAQ system.

\begin{figure}[htpb]
  \centering
  \subfloat[Commissioning DAQ]{
    \def\svgwidth{1.1\largefigwidth}
    \import{chapters/supernemo-elec/}{Manchester-comm-plateau-scan-newcolours.pdf_tex}
    \label{fig:comm-plateau-scan}
  }
  
  \subfloat[Cassette testing DAQ\cite{manchester-data}]{
    \includegraphics[width=\largefigwidth]{chapters/supernemo-elec/Sncm_012_plateau.png}
    \label{fig:manchester-plateau-scan}
  }
  \caption[Scan of the Geiger plateau.]{Scan of the Geiger plateau. Two scans of the Geiger plateau of the same cassette are shown: (a) was measured with the DAQ system described here and (b) was obtained with the system used for testing the cassettes during production. The hit rates of each cell in (a) have been normalised to the average hit rate between 1600 and 1900 V.}
  \label{fig:plateau-scan}
\end{figure}

\subsubsection{Cathode ring read-out}
An important part of the tests of the commissioning DAQ system with the SuperNEMO cells was to demonstrate the ability of the system to adequately read out the signals on the cathode rings. These signals allow for the measurement of the efficiency and speed of the propagation of the avalanches along the cell, both being important characteristics of the tracker cells.

As mentioned above, the feedthrough on the cassette test tank allows for the read out of the cathode rings on one end of the cassette only, limiting the scope of these tests. Due to detector grounding issues which have been identified at the time of writing, but had not been resolved when the cassette data was obtained, only six of the 18 instrumented cathode rings in the cassette were correctly read out.

As no pulse shape analysis is possible with the commissioning DAQ system, the only indication of the successful propagation of the Geiger avalanche to both ends of the detector is given by the cathode ring pulses. The efficiency of the propagation is then given, for each cell, by the ratio of hits in which the cathode ring pulses have been detected to the total number of hits. The efficiency of the avalanche propagation to the available cathode for the six read out cells is shown in \mbox{Figure \ref{fig:manchester-cath-eff}}. All cells show a good efficiency, expected to be above 90\%.

\begin{figure}[htpb]
  \centering
  \def\svgwidth{\mediumfigwidth}
  \import{chapters/supernemo-elec/}{Manchester-comm-cath-eff.pdf_tex}
  \caption[Efficiency of the avalanche propagation of six cells in a cassette.]{Efficiency of the avalanche propagation of six cells in a cassette.}
  \label{fig:manchester-cath-eff}
\end{figure}

Another measurement of interest is the time of propagation of the avalanches to the cathode rings. To a good approximation, the ratio of this time to the total propagation time gives the longitudinal position of the hit in units of the effective length of the cells. With the scintillator planes in their \emph{nominal} position the read-out hits are expected to be roughly $\frac{3}{4}$ of a cell's length away from the read out cathodes (Figure \ref{fig:manchester-setup-photo}). The total propagation time cannot be directly measured with the setup described here, given that only one cathode pulse is read out. However, the cassette being used for the tests had previously been characterised with the test tank electronics which allow for measurement of the total propagation time by the analysis of the anode pulse shape. The average total propagation time for cells \mbox{1 -- 6} on the cassette was 42.4\us\cite{manchester-data}. For tracker hits occurring in the range of $z$ covered by the scintillator planes (a region a few cm wide at about $\frac{3}{4}$ of the cassette's length) it is expected that the propagation time to the read out cathode rings is $t_{Prop_1} \approx \frac{3}{4} \times 42.4 \text{\us} = 31.8 \text{\us}$.


The propagation times to the available cathode read out with the commissioning DAQ system are shown in Figure \ref{fig:manchester-prop-t}. The average propagation time for cells \mbox{1 -- 6} is 33.1\us, consistent with the expectation given above.

\begin{figure}[htpb]
  \centering
  \def\svgwidth{\largefigwidth}
  \import{chapters/supernemo-elec/}{Manchester-comm-propt.pdf_tex}
  \caption[Distribution of the propagation time to the read out cathode ring for six cells in a cassette.]{Distribution of the propagation time to the read out cathode ring for six cells in a cassette.}
  \label{fig:manchester-prop-t}
\end{figure}

\subsubsection{Discussion of results}
The tests of the commissioning DAQ system on the cassette test tank at the University of Manchester have demonstrated that the system can read out a multiple-cell detector. In particular, the following capabilities of the system were shown:
\begin{itemize}
  \item the selection of cosmic ray tracker hits with the trigger system;
  \item the measurement of the cells' Geiger plateau;
  \item the measurement of the efficiency and time of the avalanche propagation (for one cathode ring only).
\end{itemize}

The system is now ready for large scale implementation, to instrument the 504 cells of a C-section for surface commissioning. In this setup, with a large number of cells, and the remaining cathode rings instrumented, the following characteristics of the tracker can be probed, in addition to the above:
\begin{itemize}
  \item the drift model converting the arrival of the anode wire pulse into a radial coordinate, by fitting straight-line tracks to $\sim$ 9 hit events;
  \item the total propagation time, by reading out the two cathode pulses, and subsequent calibration of the reconstruction of longitudinal hit positions.
\end{itemize}
