\chapter{Neutrino Phenomenology}
\label{sec:nupheno}

\section{Historical overview}

Our understanding of neutrinos has been driven by experimental observations. This is true both historically and today, with unexpected observations arising from the neutrino sector which required the description of neutrino masses to be added to the otherwise well tested theoretical framework of particle physics. In this section, a brief overview of the discoveries that led to the Standard Model (SM) description of neutrinos will be given.

\subsection{Radioactive decay}

The observation of the blackening of photographic plates when exposed to uranium salts led Henri Becquerel to the discovery of radioactive phenomena. By the beginning of the 20\ts{th} century it had been established that three different types of rays were responsible for radioactivity. Rutherford called them $\alpha$, $\beta$ and $\gamma$ rays. Becquerel had also measured the charge to mass ratio of $\beta$ rays and found it to be the same as \mbox{J.J. Thompson's} measurement of the same quantity in cathode rays. This established the nature of $\beta$ rays as electrons.

While $\alpha$ and $\gamma$ rays were emitted according to spectral lines, $\beta$ rays were found by James Chadwick \cite{Chadwick:1914zz} (and later confirmed by Ellis and Wooster) to be emitted in a continuous energy spectrum. This observation seemed at the time to be inconsistent with the well established principle of conservation of energy. Niels Bohr went as far as proposing that energy was only conserved statistically, though this too was in disagreement with the observation of an upper limit in the energy spectrum of $\beta$ decay \cite{Kragh:1493569}.

\subsection{Proposal and observation of the neutrino}

In 1930 Wolfgang Pauli proposed, as a tentative solution for the problem of the continuous spectrum of $\beta$ decay, a neutral particle which interacted only weakly and so would have gone undetected in previous experiments. The emission of this particle in addition to the electron in $\beta$ decays would explain the missing energy in the electron spectrum \cite{Brown:1978pb}.

By 1934 Enrico Fermi had incorporated the neutrino in the first successful description of $\beta$ decay by extending Dirac's model of the electromagnetic interaction\cite{Wilson:Fermi}. This theory also described other weak processes such as pion decay. It was, however ultra-violet divergent and as such it was seen as a useful effective theory which predicted low energy phenomena well but was unlikely to do so at higher energy scales.

Neutrinos remained a hypothetical construct until 1956 when Reines and Cowan detected these particles by placing a 200 litre CdCl\textsubscript{2}-loaded water detector in the vicinity of a research reactor at the Savannah River Site \cite{Cowan:1992xc}.

\subsection{Parity violation in weak interactions}

Two other noteworthy discoveries were made in the 1950s which are important to understand the way in which neutrinos (and the weak interaction) were incorporated in the SM of particle physics.

In 1956 Chien-Shiung Wu observed parity violation in weak interaction by measuring the angular distribution of nuclear de-excitation photons and $\beta$ decay electrons in \ts{60}Co while changing the spin orientation of the parent nuclei\cite{Wu:1957}. A bias was found in the angular distribution of the electrons when compared to the photons. Up to then all interactions had been assumed to be invariant under parity transformations. 

Lee and Yang had proposed the violation of this principle as a tentative solution to the so called $\theta$-$\tau$ puzzle\cite{LeeYang:1956}, where two particles were observed to be identical in every aspect except for their parity. This could be explained if the weak interaction was not invariant under the symmetry and hence parity was not a conserved quantity. 

The other experiment which proved fundamental for the understanding of neutrino interactions was the measurement of the helicity of neutrinos emitted in electron capture decays of \ts{152m}Eu. In 1958 Maurice Goldhaber demonstrated that all neutrinos emitted in the \ts{152m}Eu to \ts{152}Sm\ts{*} were left-handed by measuring the polarisation of de-excitation photons in decays where the photon and the neutrino had been emitted back-to-back \cite{Goldhaber:1958nb}.

These two observations led to the SM description of neutrinos as massless particles and the weak interaction as maximally parity-violating.

\subsection{Neutrino flavour}
\label{sec:nu-flavour}
The experiments detailed above gave the fundamental picture of neutrinos that was incorporated into the SM. All of them relate to neutrinos produced in association to electrons ($\nu_{e}$). Given the existence of other charged leptons (the $\mu$ was discovered in 1936 and the $\tau$ in 1975 \cite{muonconfirmation, tauevidence}) neutrino flavours associated with these were expected.

Indeed the existence of the $\nu_{\mu}$ was confirmed by the observation of $\mu$ tracks produced in charged current reactions in a spark chamber placed in the first accelerator neutrino beam ever produced\cite{munu_discovery}. This work was done in 1962 by Lederman, Schwartz and Steinberger.

The discovery of the $\nu_{\tau}$ happened only much later, in the year 2000 \cite{nutau_discovery}. In the DONUT experiment, protons accelerated to 800 GeV in the Tevatron were aimed at a tungsten alloy target, producing a neutrino beam with 3$\%$ $\nu_{\tau}$. Neutrino interactions were registered in a nuclear emulsion target and $\nu_{\tau}$ interactions were identified by selecting tracks with a kink after a few millimiters, a signature of $\tau$ decay.

A further constraint on the number of neutrino flavours came from precision measurements of the $Z$ decay width at LEP \cite{LEP_Z_res}. Comparing the width predicted by the SM to the observed one, the number of invisible (neutrino) decay modes can be inferred (Figure \ref{fig:neutrino-theory:LEP_Z_Res}). The results agree with three neutrino flavours. This measurement applies only to neutrino flavours which interact with the $Z$ boson and with a mass smaller than $\frac{m_{Z}}{2}$. Heavy neutrinos ($m_{\nu} >$ 45 GeV) or neutrinos which do not interact weakly (sterile) are not ruled out.

\begin{figure}[htpb]
  \def\svgwidth{\mediumfigwidth}
  \import{chapters/neutrino-theory/}{LEP_Z_Res.pdf_tex}
  \caption[Measurement of the hadronic cross-section at the Z mass pole by the LEP experiments.]{Measurement of the hadronic cross-section at the Z mass pole by the LEP experiments \cite{LEP_Z_res}.}
  \label{fig:neutrino-theory:LEP_Z_Res}
\end{figure}

The observations described above provide the setting to how neutrinos were described in the SM. More recent observations have deviated from the SM description. These will be detailed in section \ref{oscillations} of this thesis.

\section{Neutrinos in the SM}
\label{sec:nus_in_sm}

In the SM, weak processes occur via the exchange of the massive vector bosons $W^\pm$ and $Z$\cite{Giunti-Kim-2007}. At low exchanges of momentum (compared to the boson masses: \mbox{$m_W \approx$ 80 GeV}, \mbox{$m_Z \approx$ 91 GeV}) these interactions approximate Fermi's four-fermion contact interaction. The possible reactions involving neutrinos in the SM are shown in Figure \ref{fig:nu_vtx}.

\begin{figure}[htpb]
  \parbox{100mm}{
    \unitlength=1mm
    \vspace*{5mm}
    \subfloat[Charged current vertex.]{
      \begin{fmffile}{CC-vtx}
        \fmfframe(0,0)(0,5){
          \begin{fmfgraph*}(35,35)
            \fmfstraight
            \fmftop{i1,o1}
            \fmfbottom{b}
            \fmf{fermion}{i1,v1,o1}
            \fmf{photon,tension=1.0,label=$W^{\pm}$, label.side=left}{b,v1}
            \fmflabel{$\nu_\ell$}{i1}
            \fmflabel{$\ell^-$}{o1}
          \end{fmfgraph*}
        }
      \end{fmffile}
      \label{fig:neutrino-theory:CC-vtx}
    }
    \hfill
    \subfloat[Neutral current vertex.]{
      \begin{fmffile}{NC-vtx}
        \fmfframe(0,0)(0,5){
          \begin{fmfgraph*}(35,35)
            \fmfstraight
            \fmftop{i1,o1}
            \fmfbottom{b}
            \fmf{fermion}{i1,v1,o1}
            \fmf{photon,tension=1.0,label=$Z$, label.side=left}{b,v1}
            \fmflabel{$\nu_\ell$}{i1}
            \fmflabel{$\nu_\ell$}{o1}
          \end{fmfgraph*}
        }
      \end{fmffile}
      \label{fig:neutrino-theory:NC-vtx}
    }
  }

 \caption[Standard Model vertices involving neutrinos.]{Standard Model vertices involving neutrinos.}
  \label{fig:nu_vtx}
\end{figure}

%\begin{centering}
%  $\rm W^- \rightarrow \ell^- + \nu_{\ell}$,
%  
%  $\rm W^+ \rightarrow \ell^+ + \overline{\nu}_{\ell}$,
%  
%  $\rm Z \rightarrow \nu_{\ell} + \overline{\nu}_{\ell}$.
%
%\end{centering}
%Where $\ell$ are the three lepton generations: $e$, $\mu$ and $\tau$.
%
Neutrinos can be detected by scattering reactions off quarks in nuclei or atomic electrons. Four different processes can occur, either through flavour-blind neutral currents or through flavour-changing charged currents (Fig. \ref{fig:neutrino-theory:neutrino-interactions}). Neutrino interactions with detector materials are inferred from the appearance of charged leptons with no leading tracks (charged currents) or from nuclear or electron recoil signals (neutral currents).

\begin{figure}[htpb]
  \parbox{100mm}{
    \unitlength=1mm
    \vspace*{5mm}
    \subfloat[CC $\nu$-$e^-$ Scattering]{
      \begin{fmffile}{CC-Electron}
        \fmfframe(0,0)(0,5){
          \begin{fmfgraph*}(35,40)
            \fmfstraight
            \fmfleft{i1,i2}
            \fmfright{o1,o2}
            \fmf{fermion}{i1,v1,o1}
            \fmf{fermion}{i2,v2,o2}
            \fmf{photon,tension=1.0,label=$W^{\pm}$, label.side=left}{v1,v2}
            \fmflabel{$e^{-}$}{i1}
            \fmflabel{$\nu_\ell$}{i2}
            \fmflabel{$\nu_e$}{o1}
            \fmflabel{$\ell^-$}{o2}
          \end{fmfgraph*}
        }
      \end{fmffile}
      \label{fig:neutrino-theory:CC-Electron}
    }
    \hfill
    \subfloat[NC $\nu$-$e^-$ Scattering]{
      \begin{fmffile}{NC-Electron}
        \fmfframe(0,0)(0,5){
          \begin{fmfgraph*}(35,40)
            \fmfstraight
            \fmfleft{i1,i2}
            \fmfright{o1,o2}
            \fmf{fermion}{i1,v1,o1}
            \fmf{fermion}{i2,v2,o2}
            \fmf{photon,tension=1.0,label=$Z$, label.side=left}{v1,v2}
            \fmflabel{$e^{-}$}{i1}
            \fmflabel{$\nu_\ell$}{i2}
            \fmflabel{$e^{-}$}{o1}
            \fmflabel{$\nu_\ell$}{o2}
          \end{fmfgraph*}
        }
      \end{fmffile}
      \label{fig:neutrino-theory:NC-Electron}
    }
  \vspace*{8mm}
    \\
    \subfloat[CC $\nu$-$N$ Scattering]{
      \begin{fmffile}{CC-Nucleon}
        \fmfframe(0,0)(0,5){
          \begin{fmfgraph*}(35,40)
            \fmfstraight
            \fmfleft{i1,i2}
            \fmfright{o1,o2}
            \fmf{plain}{i1,v1}
            \fmf{plain}{v1,o1}
            \fmf{fermion}{i2,v2,o2}
            \fmf{photon,tension=1.0,label=$W^{\pm}$, label.side=left}{v1,v2}
            \fmflabel{$n$}{i1}
            \fmflabel{$\nu_\ell$}{i2}
            \fmflabel{$p$}{o1}
            \fmflabel{$\ell^-$}{o2}
            \fmffreeze
            \fmfi{fermion}{vpath (__i1,__v1) shifted (thick*(0,-2))}
            \fmfi{fermion}{vpath (__v1,__o1) shifted (thick*(0,-2))}
            \fmfi{plain}{vpath (__i1,__v1) shifted (thick*(0,-4))}
            \fmfi{plain}{vpath (__v1,__o1) shifted (thick*(0,-4))}
          \end{fmfgraph*}
        }
      \end{fmffile}
      \label{fig:neutrino-theory:CC-Nucleon}
    }
   \hfill
    \subfloat[NC $\nu$-$N$ Scattering]{
      \begin{fmffile}{NC-Nucleon}
        \fmfframe(0,0)(0,5){
          \begin{fmfgraph*}(35,40)
            \fmfstraight
            \fmfleft{i1,i2}
            \fmfright{o1,o2}
            \fmf{plain}{i1,v1}
            \fmf{plain}{v1,o1}
            \fmf{fermion}{i2,v2,o2}
            \fmf{photon,tension=1.0,label=$Z$, label.side=left}{v1,v2}
            \fmflabel{$N$}{i1}
            \fmflabel{$\nu_\ell$}{i2}
            \fmflabel{$N$}{o1}
            \fmflabel{$\nu_\ell$}{o2}
            \fmffreeze
            \fmfi{fermion}{vpath (__i1,__v1) shifted (thick*(0,-2))}
            \fmfi{fermion}{vpath (__v1,__o1) shifted (thick*(0,-2))}
            \fmfi{plain}{vpath (__i1,__v1) shifted (thick*(0,-4))}
            \fmfi{plain}{vpath (__v1,__o1) shifted (thick*(0,-4))}
          \end{fmfgraph*}
        }
      \end{fmffile}
      \label{fig:neutrino-theory:NC-Nucleon}
    }
  }

 \caption[Neutrino interactions with matter: charged and neutral current scattering off nucleons or atomic electrons.]{Neutrino interactions with matter: charged and neutral current scattering off nucleons (elastic) or atomic electrons.}
  \label{fig:neutrino-theory:neutrino-interactions}
\end{figure}

The parity violating character of the weak interaction is accounted for in the SM by the vector minus axial-vector (V-A) nature of the Lagrangian. This violates parity maximally in all neutrino interactions. Since only left-handed neutrinos (and right handed anti-neutrinos) are observed in nature, neutrinos are assumed to be massless in the SM.

It is now known that this description of neutrinos is partially incorrect given the observation of oscillations between neutrino flavour states. This is the first observation in particle physics that does not conform to the SM.

\section{Neutrino oscillations}
\label{oscillations}

Neutrino oscillations were first proposed by Bruno Pontecorvo in 1957 \cite{Pontecorvo:1957cp}. In an analogy with neutral kaon mixing ($K_{0} \leftrightarrow \bar{K}_{0}$, observed in 1956), Pontecorvo suggested that if the two-component description of neutrinos was not correct they could oscillate between the neutrino and anti-neutrino state. Although this idea proved observationally insignificant given the parity violation in weak interactions and the smallness of neutrino masses, it was a useful contribution to the solution of the solar neutrino puzzle.

\subsection{The solar neutrino anomaly}
\label{sec:solar}

In the late 1960s John Bahcall and Ray Davis set up a radiochemical experiment in the Homestake mine in South Dakota to measure the flux of $\nu_{e}$ produced by reactions in the Sun \cite{Davis:1968cp}. The experiment consisted of a large vessel filled with a chlorine-rich fluid. Neutrinos interacted with the chlorine nuclei via the reaction

\begin{centering}
  $ \nu_{e} +\, ^{37}\text{Cl} \rightarrow \, ^{37}\text{Ar} + e^-$.

\end{centering}

The argon gas was extracted from the vessel and the amount of radioactive \ts{37}Ar was measured in a proportional counter. The results revealed a significant deficit in the flux of solar $\nu_{e}$ predicted by the Standard Solar Model. This discrepancy was later confirmed by the GALLEX \cite{Gallex} and SAGE \cite{SAGE} experiments which used similar techniques with \ts{71}Ga as a target.

Neutrino flavour oscillations were proposed as a solution to the solar neutrino anomaly\cite{Maki:1962mu,Pontecorvo:1967fh}. Given that the experiments mentioned above were sensitive only to $\nu_{e}$, oscillations in neutrino flavour ($\nu_{e} \rightarrow \nu_{\alpha}$, $\alpha = \nu_{\mu}, \nu_{\tau}$) could explain the observed discrepancy.

This hypothesis was confirmed in 2001 by the SNO experiment \cite{SNO}, which consisted of a heavy water \v{C}erenkov detector. Both neutral and charged current reactions between neutrinos and deuterium nuclei or atomic electrons were detectable, providing a handle on neutrino flavour:

\begin{centering}
  $\nu_{e} + d \rightarrow p + p + e^- $,

  $\nu_{\ell} + d \rightarrow p + n + \nu_{\ell}$,

  $\nu_{\ell} + e^- \rightarrow \nu_{\ell} + e^-$.

\end{centering}

Additional evidence for neutrino oscillations had been obtained a few years earlier with atmospheric neutrino data. Neutrinos produced by cosmic ray interactions in the atmosphere have a wide energy spectrum up to 10 GeV. Unlike solar neutrinos, these have sufficient energy for muons to be produced in charged current reactions. Super-Kamiokande is a large water \v{C}erenkov detector which measured the $\nu_{e}$ and $\nu_{\mu}$ composition of the atmospheric neutrino flux, again observing a discrepancy which could be explained by neutrino flavour oscillations \cite{SuperK}.

Following these results, it became clear in the 2000s that neutrino oscillations occurred and so neutrinos were massive particles.

\subsection{Neutrino mixing}
\label{sec:nu_mix}

The mixing between neutrino flavour and mass states that gives rise to oscillation phenomena can be incorporated into the SM in an analogous way to mixing in the quark sector. A mixing matrix relates the neutrino flavour states with the mass states:
\begin{equation}
\ket{\nu_{\alpha}} = U^*_{\alpha j} \ket{\nu_j},
\end{equation}
\noindent where $\alpha$ are the neutrino flavour eigenstates (e, $\mu$ and $\tau$) and $j$ the mass eigenstates with masses $m_1$, $m_2$ and $m_3$. This matrix is unitary and it is commonly referred to as the Pontecorvo-Maki-Nakagawa-Sakata (PMNS) matrix \cite{Maki:1962mu}. It is useful to parametrise it in terms of three angles and three phases:
\begin{equation}
  U = \left( \begin{array}{ccc}
    1 & 0       & 0 \\
    0 & c_{23}  & s_{23} \\
    0 & -s_{23} & c_{23} \end{array} \right) \left( \begin{array}{ccc}
    c_{13} & 0       & s_{13} e^{-i\delta} \\
    0 & 1  & 0 \\
    -s_{13} e^{i\delta} & 0 & c_{13} \end{array} \right)\left( \begin{array}{ccc}
    c_{12} & s_{12}       & 0 \\
    -s_{12} & c_{12}  & 0 \\
    0 & 0 & 1 \end{array} \right) D_{M},
\end{equation}
\begin{equation}
D_{M} = \left( \begin{array}{ccc}
    1 & 0 & 0 \\
    0 & e^{i\alpha} & 0 \\
    0 & 0 & e^{i\beta} \end{array} \right),
\end{equation}
\noindent where $c_{ij} = \textrm{cos}(\theta_{ij})$, $s_{ij} = \textrm{sin}(\theta_{ij})$, $\theta_{ij}$ are angles which quantify the mixing between states $i$ and $j$, $\delta$ is the CP-violating Dirac phase, and $\alpha$, $\beta$ are two Majorana phases. The latter two parameters only have physical meaning if neutrinos are Majorana particles, while the others are accessible to oscillation experiments.

The probability of detecting a neutrino produced as flavour $\nu_{\alpha}$ as flavour $\nu_{\beta}$ is measured in oscillation experiments. This probability can be obtained by solving the Schr\"odinger equation for the mass eigenstates in the rest frame of the neutrino\footnote{Even though the time evolution of neutrino flavour is derived in the rest frame, the ultra-relativistic approximation is used when converting the kinematic quantities to the laboratory frame.} \cite{Kayser:2005cd}, giving:
\begin{equation}
  P \left( \nu_{\alpha} \rightarrow \nu_{\beta} \right) = \left| \sum_{i} U^*_{\alpha i} e^{-i m_{i}^2 \frac{L}{2 E}} U_{\beta i} \right|,
\label{eq:osc_prob}
\end{equation}
\noindent where $U_{i j}$ is the matrix described above, $m_{i}$ the masses of the mass eigenstates, $E$ the energy and $L$ the distance travelled by the neutrino ($L/E$ representing proper time for neutrinos travelling close to the speed of light).

In a simplified two-flavour scenario, the oscillation probability reduces to
\begin{equation}
  P \left( \nu_{\alpha} \rightarrow \nu_{\beta} \right) = \textrm{sin}^{2}( 2 \theta_{ij} ) \, \textrm{sin}^{2} \left( \Delta m_{ij}^{2} \frac{1.27 L}{E} \right) ,
\end{equation}
\noindent where $\theta_{ij}$ are the neutrino mixing angles as given in the parameterisation above, $\Delta m_{ij}^2 \equiv m_i^2 - m_j^2$ in eV\ts{2}, $L$ is given in km and $E$ in GeV.

It is clear from the equation above that experiments designed to measure these probabilities are sensitive to the angles in the PMNS matrix as well as to the mass differences between the mass eigenstates.

\subsection{Oscillations in matter}
\label{sec:osc_matter}

While the formalism described above is valid in vacuum, interactions with matter must be taken into account when neutrinos traverse high density regions such as the interior of the Sun or the Earth's crust \cite{Wolfenstein_MSW, Mikheev:1986wj}. Astrophysical matter is composed of protons, neutrons and electrons. All neutrino flavours will interact with these through neutral currents and $\nu_{e}$ will experience charged current reactions with electrons. 

The effect of neutral current interactions with matter cancels out in oscillation phenomena, but the coherent charged current scattering has measurable consequences. An effective potential term appears in the Hamiltonian which depends on the energy of the neutrino and the density of electrons in the material it traverses. Crucially, this potential has opposite sign for $\nu_{e}$ and $\bar{\nu}_{e}$.

The same oscillation formalism given in Section \ref{sec:nu_mix} can be used for oscillations in matter, provided the mass-squared difference and mixing angle are replaced by effective parameters. These parameters now depend on the sign of the mass-squared difference between the vacuum mass eigenstates.

This effect is particularly relevant for neutrinos with energies of a few MeV originating in the Sun (\emph{e.g.}, from $^8\text{B}$) as they traverse the very high density interior of the star on their way to detectors on Earth. At the centre of the Sun the matter effect dominates and neutrinos propagate in mass eigenstates of the effective potential. As the electron density slowly decreases towards the edge of the Sun the effective mass decreases until it coincides with the more massive vacuum eigenstate. This means that neutrinos will leave the Sun's surface in a pure $\nu_{2}$ eigenstate which does not change as neutrinos traverse the distance between the Sun and the Earth. Solar neutrinos with energies of a few MeV interacting in detectors on the Earth are thus in a pure $\nu_{2}$ state\cite{Kayser:2005cd}.

\subsection{Oscillation parameters}

In addition to the solar and atmospheric neutrino experiments described in Section \ref{sec:solar}, reactor and accelerator based experiments have constrained the parameters in the PMNS matrix. The three $\theta_{i j}$ angles have been measured and two distinct $\Delta m^2$ have been observed\footnote{There are hints of a third, larger, $\Delta m^2$ which would imply the existence of a fourth, sterile neutrino \cite{lsnd}. These remain unconfirmed \cite{miniboone}.}, which is consistent with the three-neutrino picture. The current best fit values for these parameters are listed in Table \ref{table:osc_parameters}.

\begin{table}[h]
  \centering
  \begin{tabular}{l c}
    Parameter                              & Value ($\pm$ 1$\sigma$) \\
    \hline
    $\Delta m_{21}^2$                      & 7.54$^{\rm +0.26}_{\rm -0.22} \times \rm 10^{\rm -5} \, \rm eV^{\rm 2}$ \\
    $\left| \Delta m^2 \right|$            & 2.43$^{\rm +0.06}_{\rm -0.10} \times \rm 10^{\rm -3} \, \rm eV^{\rm 2}$ \\
    $\rm sin^{\rm 2} (\theta_{\rm 1 2})$   & 0.307$^{\rm +0.018}_{\rm -0.016}$ \\
    $\rm sin^{\rm 2} (\theta_{\rm 2 3})$   & 0.386$^{\rm +0.024}_{\rm -0.021}$ \\
    $\rm sin^{\rm 2} (\theta_{\rm 1 3})$   & 0.0241$\pm \rm 0.0025$ \\
  \end{tabular}
  \caption[Best fit oscillation parameters assuming normal hierarchy.]{Best fit oscillation parameters assuming normal hierarchy \cite{pdg}.}
  \label{table:osc_parameters}
\end {table}

The CP-violating phase $\delta$ is yet to be constrained, but given that all mixing angles have been measured to have finite values this parameter will be accessible to oscillation experiments \cite{LBNE, NOvA}.
% The sign of the large $\Delta m^2$ is also unknown. This will be discussed in the next section.
\subsection{Mass hierarchy}

It is apparent in Equation \ref{eq:osc_prob} that, to first order, oscillation experiments are sensitive only to the magnitude of the mass-squared differences, but not their sign. This presents a potential ambiguity in the ordering of the mass states. The fact that charged current interactions with matter affect $\nu_{e}$ and not other neutrino flavours provides a handle on this ambiguity.

The strong matter effects felt by neutrinos originating from the Sun described in Section \ref{sec:osc_matter} constrain the sign of $\Delta m_{21}^2$.

The sign of the larger $\Delta m^2$ is so far unconstrained, allowing for two possible scenarios. In the normal hierarchy, the mass eigenstate with the largest $\nu_{e}$ contribution ($\nu_{1}$) has the lowest mass followed by $\nu_{2}$ and $\nu_{3}$. This would be analogous to the charged lepton masses, where the electron has the lowest mass and the smallest mass splitting is between the two lighter particles. Alternatively, the inverted hierarchy scenario has $\nu_{3}$ as the lightest state followed by $\nu_{1}$ and $\nu_{2}$ (Figure \ref{fig:neutrino-theory:MassHierarchy}).

\begin{figure}[htpb]
  \def\svgwidth{\largefigwidth}
  \import{chapters/neutrino-theory/}{MassHierarchy.pdf_tex}
  \caption[Normal and inverted scenarios for the neutrino mass spectrum.]{Normal and inverted scenarios for the neutrino mass spectrum\cite{motty}.}
  \label{fig:neutrino-theory:MassHierarchy}
\end{figure}

The mass hierarchy ambiguity is likely to be resolved in future oscillation experiments by exploiting the matter effect described in Section \ref{sec:osc_matter} \cite{LBNE, NOvA}.

\section{Neutrino Mass}

Given the unambiguous evidence for neutrino mass provided by oscillation phenomena, perhaps the most interesting open question in neutrino physics is how this mass can be added to the SM formalism.

This is particularly interesting because, being electrically neutral, neutrinos are unique among the fundamental particles. The only distinguishing feature between a neutrino and an anti-neutrino in the SM is the lepton number $L$, defined as $L=+1$ for neutrinos and negatively charged leptons, and $L=-1$ for anti-neutrinos and positively charged leptons. This quantity is conserved in the SM; however, it is not associated to any fundamental symmetry in the theory, and as such there are no strong reasons to uphold this principle. The treatment of this subject in the next sections follows the one in \cite{Kayser:2005cd}.

\subsection{Dirac mass term}

In the SM description neutrinos are massless and so right handed neutrino fields have no physical meaning and are left out of the theory. It would seem natural to add neutrino mass terms to the Lagrangian analogous to those of the charged leptons, \emph{i.e.}, in an $L$-conserving way. For this, a non-interacting right-handed neutrino field must be postulated:
\begin{equation}
\centering
\mathcal{L} = -m_{D} \bar{\nu}_{L} \nu_{R} + h.c. \, ,
\label{eq:dirac_mass}
\end{equation}
\noindent where $m_D$ represents the Yukawa coupling to the Higgs field. 

One problem presented by this scenario is that an unnaturally small coupling to the Higgs field would be required to generate the very small observed neutrino masses. 
%It is also unsatisfactory to postulate an additional field which has no interactions with the rest of the particle content of the theory.

\subsection{Majorana mass term}
\label{sec:maj_mass_term}
It should be noted that the right handed neutrino field postulated above has no charge or weak isospin. Apart from $L$ conservation, there is no reason why a mass term of the form
\begin{equation}
\centering
\mathcal{L} = -m_{M} \bar{\nu}^c_{R} \nu_{R} + h.c.\, ,
\label{eq:maj_mass}
\end{equation}
\begin{equation}
\centering
{\nu}^c_{R} = i \gamma^2 \nu_{R}^* \, ,
\label{eq:maj_conj}
\end{equation}
\noindent should not be added to the Lagrangian, as it conforms to all of the SM fundamental symmetries. 

Here we used only one field to build the mass term. This mass term violates $L$, mixing $\nu$ with $\bar{\nu}$ and consequently making the neutrino and anti-neutrino the same particle. Particles that obey this condition are called Majorana particles after Ettore Majorana, who proposed this treatment of neutral particles in 1936 \cite{majorana}.

Right handed neutrino fields are not the only way of adding neutrino mass to the SM Lagrangian. A model with a Higgs triplet with hypercharge $Y=+2$ would allow for mass terms of the form given in Equation \ref{eq:maj_mass} to be built out of left-handed fields without violating weak isospin, a well motivated conserved quantity \cite{SchechterValle}. Whichever the case, the existence of neutrino masses requires new physics beyond the SM.

\subsection{See-saw mechanism}

The most general mass term that can be written for neutrinos combines the two mass terms defined above:
\begin{align}
\centering
\mathcal{L}_{\rm General} &= - \frac{m_{D}}{2} \left( \bar{\nu}_{L} \nu_{R} + \bar{\nu}_{R} \nu_{L} \right) - \frac{m_{M}}{2} \bar{\nu}^c_{R} \nu_{R} \\
 &= - \frac{1}{2} \left( \begin{array}{cc}
   \bar{\nu}_{L} & \bar{\nu}^c_{R}
   \end{array} \right) \mathcal{M} \left( \begin{array}{c}
  \nu^c_{L} \\
  \nu_{R} \end{array} \right) \, ,
\label{eq:gen_mass}
\end{align}
\noindent where $\mathcal{M}$ is
\begin{equation}
\centering
\mathcal{M} = \left( \begin{array}{cc}
  0 & m_{D} \\
  m_{D} & m_{M}
  \end{array} \right) \, .
\label{eq:mass_matrix}
\end{equation}

The neutrino states in the above expressions are weak eigenstates. To find the neutrino mass states $\mathcal{M}$ must be diagonalised, yielding the eigenvalues:
\begin{equation}
m_{1,2} = \frac{1}{2} m_{M} \pm \frac{1}{2} \sqrt{m^2_M + 4 m^2_D} \,.
\label{eq:mass_eigenvals}
\end{equation}

This result gives a method for generating the very small neutrino masses we observe. If we set $m_M$ at a scale where new physics might be expected, such as somewhere below the GUT scale $m_M \leq \Lambda_{\rm GUT}$ and $m_D$ at the scale of the charged lepton masses we get $m_M \gg m_D$, and so:
\begin{align}
\centering
m_1 &\approx m_M \\
m_{2} &\approx \frac{m^2_D}{m_M} \, .
\label{eq:mass_eigenvals_limit}
\end{align}

We retrieve two neutrino masses, one of which is naturally made small by the presence of an as yet unobserved very massive state. We can express these states in terms of the fields added to the theory: 
\begin{align}
\centering
\nu_1 &\approx (\nu_L + \nu^c_L) + \frac{m_D}{m_M} ( \nu_R + \nu^c_R )\\
\nu_2 &\approx (\nu_R + \nu^c_R) + \frac{m_D}{m_M} ( \nu_L + \nu^c_L ) \, .
\label{eq:mass_in_SM_states}
\end{align}

We are left with a light neutrino mostly made of the fields which interact in the SM Lagrangian and a heavy neutrino mostly made of sterile fields.

While Majorana mass terms still require new physics (in the case described above sterile neutrino fields) they do provide an elegant solution to the problem of the smallness of neutrino masses.

It should be pointed out that other see-saw mechanisms exist which require new physics of the type alluded to at the end of Section \ref{sec:maj_mass_term}. All these mechanisms require Majorana mass terms.

\section{Constraints on neutrino mass}

We now have a detailed knowledge of the mixing between neutrino mass and weak eigenstates, including the magnitude of the two squared-mass splittings. Even though neutrino oscillation experiments are not sensitive to the absolute mass scale of neutrinos, they provide a lower bound in the form of the largest mass splitting: at least one of the neutrino masses must be as large as the largest mass splitting ($\sqrt{\Delta m^2} = $ 49 meV).

There are three independent experimental methods which give upper bounds to the neutrino masses. The only model independent constraint comes from the measurement of the electron energy spectrum in tritium $\beta$ decay. Provided that neutrinos are Majorana particles, $0\nu\beta\beta$ gives one of the most stringent constraints on neutrino mass. Cosmological measurements can provide an upper limit on the neutrino mass, however it depends on assumptions of the cosmological models. 

\subsection{Beta decay of tritium}
The most direct limits on neutrino masses are obtained by measuring the electron energy spectral shape in $\beta$ decays. The effect of a finite neutrino mass can be observed via a change in the energy available to the emitted electron compared to the massless case (Figure \ref{fig:neutrino-theory:TritiumSpectrum}). Nuclei which undergo $\beta$ decays in which the difference between the masses of the parent and daughter atoms (Q-value) is small are suitable for this type of measurement: the effect of a small neutrino mass will lead to a larger relative change in the measured electron spectrum.
\begin{figure}[htpb]
  \def\svgwidth{\largefigwidth}
  \import{chapters/neutrino-theory/}{TritiumSpectrum.pdf_tex}
  \caption[Electron energy spectrum in the $\beta$ decay of tritium]{Electron energy spectrum in the $\beta$ decay of tritium, showing the effect of an exaggerated neutrino mass\cite{Bowles:1997br}.}
  \label{fig:neutrino-theory:TritiumSpectrum}
\end{figure}

Two of the lowest Q-value $\beta$ decaying isotopes known are \ts{115}In and \ts{187}Rh, with Q-values of 0.16 and 2.47 keV, respectively. However, these decays have very long half-lives that limit the sensitivity of potential experiments. There are also theoretical uncertainties in the calculation of the electron final states \cite{Drexlin:2013lha, Wieslander:2009zz}.

The best limits on neutrino mass using this method come from tritium decay. Tritium is an isotope of hydrogen which undergoes $\beta$ decay with a Q-value of \mbox{18.6 keV}:
\begin{equation}
\centering
^3\textrm{He} \rightarrow\,  ^3\textrm{He}^+ + e^- + \bar{\nu}_{e} \, .
\label{eq:tritium_decay}
\end{equation}
This decay has a short enough half-life (12.3 years) and the simple electronic structure of the intervening atoms allows for precise calculations yielding small systematic effects on the mass measurement.

Experiments of this type are sensitive to the average value of the mass states of the $\bar{\nu}_{e}$ produced in the decay. This is given by the sum over the PMNS matrix:
\begin{equation}
  \braket{m_{\beta}}^2 = \sum_{i} \left| U_{ei}^2 \right| m_{i}^2 \, .
\label{eq:tritium_mass}
\end{equation}
The current upper limit on $\braket{m_{\beta}}$ was obtained by the Troitsk \cite{troitsk} and Mainz \cite{mainz} experiments. A combination of the results of the two experiments constrains $\braket{m_{\beta}}$ to be smaller than 2 eV (95\% CL).

KATRIN is an experiment built on the same technology as the two experiments mentioned above which is expected to be sensitive to a $\braket{m_{\beta}}$ of 0.2 eV (90\% CL) \cite{Osipowicz:2001sq}. While an unpractically large spectrometer would make a further improvement on sensitivity unfeasible with the KATRIN technique, alternative $\beta$-decay end-point measurement techniques are currently being developed which might provide stricter constraints in the future. Two examples of this are the bolometric measurement of the $\beta$-decay of \ts{187}Rh proposed by the MARE collaboration\cite{Nucciotti:2010tx}, and Project 8, which aims to study the $\beta$-decay of tritium by measuring the cyclotron radiation of the emitted electrons as they travel in a magnetic field\cite{PhysRevD.80.051301}.

\subsection{Cosmology}
Another handle on neutrino masses is provided by cosmological observations. The hot big bang model predicts a sea of relic neutrinos slightly smaller in number density than the cosmic microwave background (CMB). These cosmic neutrinos are yet to be observed. A combination of fits over a number of cosmological observables provide constraints on the sum of neutrino masses ($\sum_{i} m_i$).

One of these constraints comes from measurements of the power spectrum of the CMB. By combining CMB measurements with other cosmological observables it is possible to limit $\sum_{i} m_i$ to 0.44 eV (95\% CL) \cite{Hinshaw:2012aka}. Another constraint arises from probing galaxy clustering through photometric galaxy redshift surveys. These are sensitive to neutrino mass since formation of structures in the early universe is suppressed by free streaming neutrinos. A combination of galaxy clustering data and CMB measurements yields an upper limit on $\sum_{i} m_i$ of 0.28 eV (95\% CL) or higher, depending on assumed cosmological parameters \cite{Thomas:2009ae}.

\subsection{Neutrinoless double-$\beta$ decay}
If neutrinos are Majorana particles, neutrinoless double-$\beta$ decay (\0nu) can occur. This decay mode will be discussed in more detail in Chapter 3 of this thesis. If the mass mechanism dominates this decay, its rate will have a dependence on the effective neutrino mass:
\begin{equation}
\braket{m_{\beta\beta}} = \left| \sum_i  U_{ei}^2 m_{i} \right| \, .
\label{eq:eff-maj-mass}
\end{equation}

The current best limit on this quantity is provided by a combination of the KamLAND-Zen and EXO-200 experiments' searches for the double-$\beta$ decay of $^{136}$Xe. The limit is set at $\braket{m_{\beta\beta}} < 0.12 - 0.25$ eV (90\% CL) \cite{kamland-zen}, with the range reflecting the choice of Nuclear Matrix Element (NME) used.

It should be emphasised that limits on neutrino mass from \0nu are only valid if neutrinos are Majorana particles, otherwise no relation between limits on the decay half-life to neutrino mass may be drawn.

A summary of the constraints on the mass of neutrinos from the three sources described above is given in Table \ref{tab:nu_mass_lim}.

\begin{table}[htb]
\begin{center}
  \begin{tabular}{ c c c }
    Parameter & Value & Source \\
    \midrule
    $\braket{m_\beta}$            & $< 2 $ eV (95\% CL) & Tritium decay\cite{troitsk, mainz}\\
    $\sum_i m_i$                    & $< 0.28-0.44 $ eV (95\% CL) & Cosmology\cite{Thomas:2009ae,Hinshaw:2012aka}\\
    $\braket{m_{\beta\beta}}$     & $< 0.12-0.25$ eV (90\% CL) & \0nu\cite{kamland-zen}\\
    $\textrm{max}(m_i)$                & $> 0.05$ eV (68\% CL) & Oscillations\cite{pdg}\\
  \end{tabular}
  \caption[Current limits on neutrino mass.]{Current limits on neutrino mass.}
  \label{tab:nu_mass_lim}
\end{center}
\end{table}



\section{Summary and outlook}
A detailed knowledge of neutrino mixing parameters has been attained and is summarised in Table \ref{table:osc_parameters}. Two important parameters are yet to be constrained by experiment, namely the hierarchy of the neutrino mass spectrum, and the \mbox{CP-violating} phase $\delta$. The next generation of oscillation experiments have as their main objectives the measurement of these parameters.

The mass hierarchy is of particular importance to the determination of the nature of neutrinos, and so also to the determination of their absolute mass scale from \0nu. The relation of the effective mass constrained by \0nu with the mass of the lightest neutrino is shown in Figure \ref{fig:bbmass_smallestmass} together with the constraints from \0nu and cosmological observations. The width of the allowed regions for each neutrino mass hierarchy hypothesis is due to the unknown CP-violating phases.  

\begin{figure}[htpb]
  \def\svgwidth{\mediumfigwidth}
  \import{chapters/neutrino-theory/}{DiscoveryProspects.pdf_tex}
  \caption[Effective \0nu mass against smallest neutrino mass.]{Effective \0nu mass against smallest neutrino mass\cite{motty} (adapted from \cite{Rodejohann:2012xd}). The parameter space allowed for the normal hierarchy is shown in red and for the inverted hierarchy in green.}
  \label{fig:bbmass_smallestmass}
\end{figure}


The next generation of \0nu experiments is expected to be sensitive to the region allowed by the inverted hierarchy. If the neutrino mass spectrum is established to be inverted, a conclusive determination of the nature of neutrinos might be attained by next or next-to-next generation experiments.

In the case that the mass spectrum is determined to be normal the nature of neutrinos might remain elusive, though studies show that it might be possible to build a detector to probe \0nu to the level as low as $\braket{m_{\beta\beta}} = 2.5$ meV at a reasonable cost in the foreseeable future \cite{Biller:2013wua}.
