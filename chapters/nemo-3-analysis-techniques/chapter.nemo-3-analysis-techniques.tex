\chapter{General analysis techniques}
\label{sec:analysis-tech}
The general strategy for NEMO-3 data analyses consists of the comparison of simulated data to real data obtained with the detector. The tracking capabilities and the segmented calorimeter of NEMO-3 allow for independent sets of data to be selected with different criteria. This allows for the tuning of simulation parameters as well as for obtaining a precise knowledge of the backgrounds by analysing control channel data.

\section{Monte Carlo simulation}
Monte Carlo data are produced by generating primary particles randomly positioned in specific volumes of the detector and propagating them through the detector taking into account interactions with its materials. Data are then obtained by registering the interactions of either the primary or secondary particles produced in detector interactions with sensitive components of the detector.

The package DECAY0\cite{Ponkratenko:2000um} is used for generating the primary particle information. This package generates a full kinematic description of particles originating from radioactive decays according to known nuclear data or, in the case of as yet unobserved decays, according to theoretical expectations of available models. Both signal and background processes are generated with DECAY0. 

The primary particles are distributed randomly in selected detector components and propagated through a detailed geometrical description of the detector with the GEANT-3.21 package\cite{Brun:1987ma}. The components available for positioning decay vertices include the source foils (for signal and internal backgrounds), the surfaces of internal components such as the source foils and the wires (for backgrounds due to \Rn{222}) and other detector components where radioactive contaminants are expected such as the glass in PMTs.

A last step in the simulation is to identify the events which satisfy the trigger criteria (Section \ref{sec:trigger}). Events which would not trigger the DAQ system (\emph{e.g.}, with energy below threshold or without hits in the tracker) are discarded. The information on events which pass the trigger criteria is converted to the quantities that would be observed in real events (\emph{i.e.}, ADC and TDC counts) and written to disc.

\section{Event reconstruction}
The experimentally obtained data and those resulting from the simulation described above are then converted into useful physical quantities by an event reconstruction program. This package is interfaced with a database where the running conditions of the detector and PMT calibration data are stored. This allows for data recorded by PMTs to be converted into an accurate energy measurement and for simulated data to be smeared according to measured resolutions of individual PMTs to reflect real data in the closest possible way. Running conditions data are also used to remove simulated hits in detector modules that were inactive during the time period to which events were assigned.

Also at the reconstruction stage a tracking algorithm identifies clusters of hits in the tracking chamber in a given event and finds the helix or set of helices which better describe them, producing the tracking information used in analyses. Three helices are fit to each track: a global fit which includes all the hits in the cluster and is used to measure the length of the track; a fit local to the foil in which only the tracker hits closest to the foil are used; and a calorimeter local fit which takes into account only the hits closest to the calorimeter wall. The latter two fits are used to obtain precise measurements of the intersections of the particle path with the source foils and the calorimeter modules, respectively.

\section{Particle identification}
The information produced at the reconstruction stage is used to build objects useful for analyses. The tracking capabilities of NEMO-3 are used in conjunction with data from the segmented calorimeter to identify electrons, positrons, photons and $\alpha$ particles emitted in \Bi{214} - \Po{214} decay cascades.

\subsection{Electrons}
\label{sec:pid_e}
Electrons are identified in NEMO-3 by long tracks intersecting both the source foil and a calorimeter block with a registered energy deposit. As it is expected that electrons interacting with the scintillator are fully contained within a calorimeter module, it is required that no modules neighbouring the one associated to the track are hit in the event.

In events where more than one particle produces a hit in the calorimeter, the relative timing of these hits can be used to infer the direction of travel of the tracked particles. In these cases it is possible to discriminate electrons from positrons with the sign of the curvature of the track relative to the direction of travel and orientation of the magnetic field.

An example of an event with two electrons is shown in Figure \ref{fig:ex_2e}.

\begin{figure}[htpb]
  \centering
  \includegraphics[width=0.8\mediumfigwidth]{chapters/nemo-3-analysis-techniques/2eEvent.pdf}
  \caption[Example of a two-electron event.]{Example of a two-electron event. Hit tracker cells are displayed as small open circles and calorimeter modules with a registered energy deposit are marked in yellow. Two tracks are seen emerging from the same point in the foil which intersect the calorimeter hits.}
  \label{fig:ex_2e}
\end{figure}


\subsection{Photons}

As $\gamma$'s are not charged, they do not ionise the gas in the detector leaving no tracks in the chamber. These particles are identified by localised energy deposits in the calorimeter which are not associated to tracks. As they do not ionise the tracking chamber, events in which only $\gamma$'s are present do not trigger the DAQ and therefore these particles can only be observed in events with at least one electron.

Unlike electrons, $\gamma$'s interacting with the scintillator will not, in general, by fully contained in one calorimeter module. In order to minimise the effect of partially contained $\gamma$ interactions with scintillator blocks, hits recorded on neighbouring calorimeter modules are clustered to form a single $\gamma$ object. The energies of all the clustered hits are summed to give the energy of the $\gamma$ while its position and timing information are given by the earliest hit in the cluster.

\subsection{Alpha emission from \Bi{214} - \Po{214} decays}
Being electrically charged and significantly more massive than electrons, $\alpha$'s interact more strongly with the tracking gas and are less susceptible to the curving effect of the magnetic field. As a result, $\alpha$'s leave straight and short tracks in the detector. As $\alpha$'s emitted in radioactive decays traverse only a fraction of the width of the tracking chamber they do not leave signals in the calorimeter and therefore can only be identified in events in which electrons are present. 

The only naturally occurring radioactive process where an electron and an $\alpha$ are emitted with a time interval which allows for their detection in NEMO-3 is the $\beta$-decay of \Bi{214} followed by the $\alpha$-decay of \Po{214}, which is discussed in more detail in Section \ref{sec:radon}. An example of an event with a delayed $\alpha$ track is shown in \mbox{Figure \ref{fig:ex_BiPo}}.

\begin{figure}[htpb]
  \centering
  \includegraphics[width=0.8\mediumfigwidth]{chapters/nemo-3-analysis-techniques/BiPoEvent.pdf}
  \caption[Example of an event with a delayed $\alpha$ track.]{Example of an event with a delayed $\alpha$ track. A short, straight track is seen emerging from the same point on the foil as the electron track (red). The delayed tracker hits are shown as black squares.}
  \label{fig:ex_BiPo}
\end{figure}


\section{Time of flight}
\label{sec:tof-def}
In NEMO-3 analyses it is important to distinguish between events where the particles originate from radioactive decays occurring in the source foils and those of external origin. This is particularly relevant in events where two electron tracks are reconstructed. As described above (Section \ref{sec:pid_e}), electron tracks are defined from the point of intersection with the source foil to the point of intersection with the calorimeter. In the cases where a single electron traverses the entire chamber, crossing the source foil, two tracks are reconstructed resulting in two electron objects (Figure \ref{fig:ex_xe}). A similar ambiguity exists in events with one electron and one or more $\gamma$'s. There is a possibility that the event is a result of a decay in the source foil with the emission of an electron and the $\gamma$'s and also the hypothesis where a single $\gamma$ interacts first in the calorimeter, leaving an energy deposit, and then Compton scatters in the dense source foil producing an electron track (Figure \ref{fig:ex_1e1g_ext}).

\begin{figure}[htpb]
        \centering
        \subfloat[Crossing electron]{
          \includegraphics[width=\smallfigwidth]{chapters/nemo-3-analysis-techniques/Ext_OCE_Event.pdf}
          \label{fig:ex_xe}
        }
        \hfill
        \subfloat[External electron and $\gamma$]{
          \includegraphics[width=\smallfigwidth]{chapters/nemo-3-analysis-techniques/Ext_EG_Event.pdf}
          \label{fig:ex_1e1g_ext}
        }
        \caption[Example of events with external origin.]{Example of events with external origin. An event where a single electron crosses the tracking chamber is shown in (a) and an example of an event with one electron and one $\gamma$ is shown in (b). The hypothetical paths of the $\gamma$'s are shown as black lines.}
\end{figure}

Discriminating between these classes of events is crucial for reducing the background in the two-electron event selection used for signal measurement and also to obtain precise measurements of the background contribution originating from sources external to the source foil. This distinction can be made using the precise timing information from the calorimeter hits in conjunction with the particle path length information provided by the tracker. The $\chi^2$ test statistic is calculated for both hypotheses allowing internal or external events to be selected by making appropriate cuts on the resulting p-values, defined for one degree of freedom as:
\begin{equation}
  P\left(\chi^2\right) = 1 - \frac{1}{\sqrt{2\pi}} \int_0^{\chi^2} x^{-\frac{1}{2}} e^{-\frac{x}{2}} dx\, .
\end{equation}

\subsection{Internal hypothesis}
To calculate the $\chi^2$ for the internal hypothesis it is assumed that the two particles are emitted from the same point in the foil and at the same time. Under this assumption, subtracting the time of flight of each particle from the relevant measurement time recorded by the calorimeter yields to hypothesised emission times which, for internal events, should be consistent with each other within experimental uncertainty. The $\chi^2$ statistic is given by:
\begin{equation}
  \chi^2_{\text{Internal}} = \frac{\left( \left(t^{\text{Meas}}_1 - t^{\text{TOF}}_1\right) - \left(t^{\text{Meas}}_2 - t^{\text{TOF}}_2\right) \right)^2 }{\sigma_{t^{\text{Meas}}_1}^2 + \sigma_{t^{\text{TOF}}_1}^2 + \sigma_{t^{\text{Meas}}_2}^2 + \sigma_{t^{\text{TOF}}_2}^2} \, ,
\end{equation}
\noindent where $t^{\text{Meas}}_i$ are the measurement times recorded by the calorimeter modules, $t^{\text{TOF}}_i$ are times of flight of the particles, calculated from tracker and calorimeter module position data, and the $\sigma$'s are their respective uncertainties.

The uncertainty on the time measurement ($\sigma_{t^{\text{Meas}}_i}$) is estimated from calibration data, with the values being optimised separately for different types of calorimeter modules.
\subsubsection{Electrons}
In the case of electrons the time of flight is given by:
\begin{equation}
  t^{\text{TOF}}_e = \frac{L_e}{\beta c} \, ,
\end{equation}
\noindent where $L_e$ is the length of the electron track, $c$ is the speed of light, and the ratio of the speed of the electron to the speed of light $\beta$ is calculated with the electron energy measured in the calorimeter ($E_e$) and the known mass of the electron ($m_e$):
\begin{equation}
  \label{eq:e_beta}
  \beta = \frac{\sqrt{E_e\left(E_e + 2m_e\right)}}{E_e + m_e}\, .
\end{equation}

Given the precision of the tracker and the fact that electrons interact very close to the face of the scintillator blocks, the uncertainty on the path length of electrons is negligible. The uncertainty on the electron time of flight is therefore dominated by the uncertainty on the speed of the particle:
\begin{equation}
  \sigma_{t^{\text{TOF}}_e}^2 = \left|\frac{ \partial t^{\text{TOF}}_e}{\partial \beta} \right|^2 \sigma_\beta^2 \, ,
\end{equation}
\noindent where $\sigma_\beta$ is obtained by propagating the energy resolution of the relevant calorimeter module through Equation \ref{eq:e_beta}.

\subsubsection{$\gamma$'s}
In the case of $\gamma$'s, the path length $L_\gamma$ is given by the distance between the intersection of the electron track with the source foil and the position of the $\gamma$ energy deposit in the calorimeter. Given that $\gamma$'s are not tracked and are not guaranteed to interact close to the face of the scintillator block, their path length is not known as precisely as the electrons'. To account for this, a parameter $t_{\text{Scint}}$ is added to the time of flight of the $\gamma$'s which depends on the type of calorimeter block and the $\gamma$ energy. This parameter and its corresponding uncertainty ($\sigma_{t_{\text{Scint}}}$) are optimised in calibration source data\cite{TOF_note}. The time of flight for $\gamma$'s is given by:
\begin{equation}
  t^{\text{TOF}}_\gamma = \frac{L_\gamma}{c} + t_{\text{Scint}}\,,
\end{equation}
\noindent and its corresponding uncertainty by:
\begin{equation}
  \sigma_{t^{\text{TOF}}_\gamma} = \sigma_{t_{\text{Scint}}} \, .
\end{equation}

The distribution of p-values for the internal hypothesis for two-electron events is shown in Figure \ref{fig:2e_intProb} and for events with one electron and one $\gamma$ in Figure \ref{fig:1e1g_intProb}. The p-values for MC events generated in the source foil (\emph{i.e.}, internal backgrounds and \2nu signal) are distributed uniformly, as expected. The exception to this is the distribution \Sr{90}/\Y{90} component of the internal background in the selection of events with one electron and one $\gamma$. As this isotope does not emit $\gamma$'s itself, the events included in this selection are mostly a result of bremmstrahlung interactions. In cases where these interactions do not occur in the source foil (\emph{e.g.}, bremmstrahlung in the tracker wires) it is expected that the p-value for the internal hypothesis is lower than for events where the $\gamma$'s are produced in the foil. More details on the backgrounds and selection criteria are given in Chapters \ref{sec:bkgd_meas} and \ref{sec:dbd_results}.

\begin{figure}[htpb]
        \centering
        \subfloat[Two-electron]{
          \includegraphics[width=0.5\textwidth]{chapters/nemo-3-analysis-techniques/2eNg_intProb.pdf}
          \label{fig:2e_intProb}
        }
        \subfloat[One electron and one $\gamma$]{
          \includegraphics[width=0.5\textwidth]{chapters/nemo-3-analysis-techniques/1e1g_intProb.pdf}
          \label{fig:1e1g_intProb}
        }
        \caption[Distributions of p-values for the internal time of flight hypothesis.]{Distributions of p-values for the internal time of flight hypothesis. }
\end{figure}

\subsection{External hypothesis}
For the external hypothesis it is assumed that one particle has interacted in a scintillator module and then traversed the tracking chamber, possibly interacting with the dense source foil before leaving an energy deposit in some other calorimeter module. The difference between the times recorded by the two calorimeter modules should then be consistent with the total time of flight of the particles:
\begin{equation}
    \chi^2_{\text{External}} \left(1 \rightarrow 2 \right) = \frac{ \left( t^{\text{Meas}}_1  - \left( t^{\text{Meas}}_2 - t^{\text{TOF}}_2 - t^{\text{TOF}}_1 \right) \right) ^2 }{\sigma_{t^{\text{Meas}}_1}^2 + \sigma_{t^{\text{Meas}}_2}^2 + \sigma_{t^{\text{TOF}}_2}^2 + \sigma_{t^{\text{TOF}}_1}^2} \, ,
\end{equation}
\noindent where the direction of travel is from calorimeter hits labelled $1$ to $2$.

\subsubsection{Crossing electron}
In the case where two electron tracks are found in the event it is assumed in the external hypothesis that only one particle traversed the tracking chamber, where some energy is deposited in the calorimeter module where the electron originates and another calorimeter hit is found at the end of the particle trajectory. The times of flight for each leg (from the origin calorimeter module to the source foil and from the source foil to the calorimeter where the electron is absorbed) are calculated with the energy measured in the calorimeter where the electron is assumed to be absorbed:
\begin{equation}
  t^{\text{TOF}}_1 \left(1 \rightarrow 2 \right) = \frac{L_1}{\beta_2 c} \, ,
\end{equation}
\begin{equation}
  t^{\text{TOF}}_2 \left(1 \rightarrow 2 \right) = \frac{L_2}{\beta_2 c} \, .
\end{equation}

The p-value is calculated for both directions of travel, $1 \rightarrow 2$ and $2 \rightarrow 1$, with the largest one taken as the p-value for the external crossing electron hypothesis.

\subsubsection{External electron and $\gamma$}
To calculate the $\chi^2_{\text{External}}$ for events with one electron and one $\gamma$ it is always assumed that the event originates from a $\gamma$ interaction in the calorimeter followed by Compton scattering in the foil producing an electron (Figure \ref{fig:ex_1e1g_ext}). The expression for the $\chi^2$ is therefore given by:
\begin{equation}
    \chi^2_{\text{External}} \left(\gamma \rightarrow e \right) = \frac{ \left( t^{\text{Meas}}_\gamma  - \left( t^{\text{Meas}}_e - t^{\text{TOF}}_e - t^{\text{TOF}}_\gamma \right) \right) ^2 }{\sigma_{t^{\text{Meas}}_\gamma}^2 + \sigma_{t^{\text{Meas}}_e}^2 + \sigma_{t^{\text{TOF}}_e}^2 + \sigma_{t^{\text{TOF}}_\gamma}^2} \, ,
\end{equation}
\noindent where the times and uncertainties are as described for the internal hypothesis above.

Distributions of the p-values for the external crossing electron and external electron and $\gamma$ hypotheses are shown in Figure \ref{fig:extProb_dists}, where a very pure selection of external background components is seen. More details about these backgrounds and the selection criteria used in the channels which constrain them are given in \mbox{Chapter \ref{sec:bkgd_meas}}.

\begin{figure}[htpb]
        \centering
        \subfloat[Crossing electron]{
          \includegraphics[width=0.5\textwidth]{chapters/nemo-3-analysis-techniques/xe_extProb.pdf}
          \label{fig:xe_extProb}
        }
        \subfloat[One electron and one $\gamma$]{
          \includegraphics[width=0.5\textwidth]{chapters/nemo-3-analysis-techniques/1e1g_extProb.pdf}
          \label{fig:1e1g_extProb}
        }
        \caption[Distributions of p-values for the external time of flight hypothesis.]{Distributions of p-values for the external time of flight hypothesis. }
        \label{fig:extProb_dists}
\end{figure}

\section{Statistical analysis}
To measure the activity of backgrounds and extract the signal strength, the MC and experimental data are split into a number of independent channels with selection criteria designed to enhance sensitivity to particular contributions. The best fit of the MC samples to the real data is then obtained with a binned likelihood optimisation. In the case where no statistically significant signal contribution is observed, a limit is set on its strength.

\subsection{Fitting MC predictions to data}
\label{sec:ana_fit}
In this analysis the fitting of MC to data is performed over several histograms originating from different analysis channels. For each bin $i$ in each histogram $j$, it is assumed that the MC predicts the number of expected counts accurately and so the probability of observing $n_{i,j}$ data counts given a MC prediction $\nu_{i,j}$ is given by Poisson statistics:
\begin{equation}
  p_{i,j} = \frac{{\nu_{i,j}}^{n_{i,j}}}{n_{i,j}!} e^{-\nu_{i,j}} \, ,
\end{equation}

\noindent where the MC prediction $\nu_{i,j}$ is given by the sum of the $k$ background contributions and the signal prediction $s_{i,j}$:

\begin{equation}
  \nu_{i,j} = s_{i,j} + \sum_k b_{i,j,k} \, .
\end{equation}

The likelihood function is taken to be the product of the probabilities $p_{i,j}$ for all bins $i$ and histograms $j$:
\begin{equation}
  L = \prod\limits_{i,j} \frac{{\nu_{i,j}}^{n_{i,j}}}{n_{i,j}!} e^{-\nu_{i,j}} \, .
  \label{eq:likelihood}
\end{equation}

The best fit of the MC prediction to data is obtained by scaling the individual background contributions and the signal strength by the set of factors which maximises $L$. Since the logarithmic function behaves monotonically, this is equivalent to maximising the log-likelihood function:
\begin{equation}
  \ln L = \sum_{i,j} \left( - \nu_{i,j} + n_{i,j} \ln \left( \nu_{i,j}\right) - \ln \left(n_{i,j}!\right) \right) \, .
\end{equation}

In some cases independent constraints are available on background activities, for example, from HPGe measurements of the source foils (Section \ref{sec:HPGe_meas}). These constraints are added to the log-likelihood function as the Gaussian probability of observing activity $a_\kappa$ for background contribution $\kappa$, given the independent measurement $\alpha_\kappa$ with a standard deviation $\sigma_{\alpha_\kappa}$:
\begin{equation}
  p_\kappa = \frac{1}{\sigma_{\alpha_\kappa}\sqrt{2\pi}}\, e^{-\frac{\left(a_\kappa - \alpha_\kappa\right)^2}{2\sigma_{\alpha_\kappa}^2}} \, .
%  p_\kappa = \frac{e^{-\frac{\left(a_\kappa - \alpha_\kappa\right)^2}{2\sigma_{\alpha_\kappa}^2}}}{\sigma_{\alpha_\kappa}\sqrt{2\pi}}  \, .
\end{equation}

The log-likelihood function becomes:
\begin{equation}
  \ln L = \sum_{i,j} \left( - \nu_{i,j} + n_{i,j} \ln \left( \nu_{i,j}\right) - \ln \left(n_{i,j}!\right) \right) - \sum_\kappa \frac{\left(a_\kappa - \alpha_\kappa\right)^2}{2\sigma_{\alpha_\kappa}^2} \, ,
\end{equation}
\noindent where constant terms are neglected.

To extract the background activities and the signal strength this function is maximised using the TMinuit tool\cite{Minuit} included in the ROOT data analysis package\cite{ROOT}. The statistical uncertainty of measurements is taken to be the interval around the best fit value where $\ln L$ changes by less than $1/2$. This corresponds to one standard deviation\cite{GlenCowan}.  

\subsection{Limit setting}
\label{sec:ana_cls}
Where no statistically significant signal contribution is observed in data, an upper limit is set on its strength.

The upper limits reported in this thesis are obtained with the $\text{CL}_{s}$ method\cite{Zech1989608, 0954-3899-28-10-313}, which gives the confidence level at which the signal is excluded for a given signal strength.

A negative log-likelihood ratio (NLLR) test statistic is built by taking the logarithm of the ratio of the likelihoods (as defined in Equation \ref{eq:likelihood}) for the \emph{signal plus background} ($s+b$) to the \emph{background only} ($b$) hypotheses:
\begin{equation}
  Q (\mathbf{n}) = -2 \ln \left(\frac{L_{s+b}}{L_{b}}\right) = 2 \sum_i \left(s_i - n_i \ln \left(1+\frac{s_i}{b_i}\right) \right)
  \label{eq:NLLR}
\end{equation}
\noindent where $s_i$ are the predicted signal counts, $b_i$ the predicted background counts and $n_i$ the observed number of events. As searches in this analysis are done over one channel the index $j$ is omitted and the index $i$ represents the bin number, as in the formulae written above.

While for a large $n_i$ the NLLR follows the $\chi^2$ distribution\cite{wilks1938}, this is not the case for the analysis presented in this thesis. Instead, the distributions of the NLLR for the \emph{background only} ($\frac{\partial P_{b}}{\partial Q}$) and \emph{signal plus background} ($\frac{\partial P_{s+b}}{\partial Q}$) are obtained by generating a large number of pseudo-experiments according to both hypotheses and substituting the resulting $n_i$ in Equation \ref{eq:NLLR} (Figure \ref{fig:NLLR}).
\begin{figure}[htpb]
  \def\svgwidth{\mediumfigwidth}
  \import{chapters/nemo-3-analysis-techniques/}{cowan_NLLR.pdf_tex}
  \caption[Example of NLLR distributions.]{Example of NLLR distributions for the \emph{background only} and \emph{signal plus background} hypothesis. The quantities $1-\text{CL}_{s+b}$ and $\text{CL}_b$ used for calculating the confidence level on the signal are shown as shaded areas. Adapted from \cite{Cowan:2013pha}.}
  \label{fig:NLLR}
\end{figure}

Confidence levels can therefore be built for the scenarios where statistical fluctuations would yield a NLLR larger than the one observed with data $\mathbf{n}_\text{Obs}$ for the \emph{background only} hypothesis:
\begin{equation}
  \text{CL}_{b} = P_b ( Q > Q(\mathbf{n}_\text{Obs} )) = \int_{Q(\mathbf{n}_\text{Obs})}^\infty \frac{\partial P_{b}}{\partial Q} \, dQ \, ,
\end{equation}
\noindent and for the \emph{signal plus background} hypothesis:
\begin{equation}
  \text{CL}_{s+b} = P_{s+b} ( Q > Q(\mathbf{n}_\text{Obs} )) = \int_{Q(\mathbf{n}_\text{Obs})}^\infty \frac{\partial P_{s+b}}{\partial Q} \, dQ \, .
\end{equation}

The confidence level on the signal is given by:
\begin{equation}
  \text{CL}_s = \frac{\text{CL}_{s+b}}{\text{CL}_b} \, .
\end{equation}

To extract the limit on the signal strength, the scale of the signal is optimised such that the confidence level is at the desired level. Limits reported in this thesis exclude the signal at 90\% CL (\emph{i.e.}, CL$_s$ = 10\%) and are calculated with the Collie\cite{Collie} package. This tool also allows for the estimation of the effects of systematic uncertainties on the signal and background distributions on the extracted limits, by producing pseudo-data with the MC predictions shifted by a Gaussian-distributed random number.



\section{Half-life calculation}
\label{sec:ana_hl}
It is useful to report the observed signal strength (or limits on signal strength) as a half-life so that it can be compared to other experimental results and theoretical calculations.

Exponential decay gives the number of unstable nuclei as a function of time ($N(t)$) as:
\begin{equation}
  N(t) = N(0)e^{-\lambda t} \, ,
\label{eq:exp_decay}
\end{equation}
\noindent where $N(0)$ is the initial number of nuclei and $\lambda$ is the decay constant. The half-life ($t^{1/2}$) is related to the decay constant by:
\begin{equation}
  t^{1/2} = \frac{\ln (2)}{\lambda} \, .
\end{equation}

Given that the half-lives of the processes investigated in NEMO-3 are much larger than the time span of the experiment, Equation \ref{eq:exp_decay} is very well described by its Taylor expansion to the first order:
\begin{equation}
  N(t) = N(0) \left( 1 - \lambda t \right ) \, .
\end{equation}

The observed number of decays during run time $t_\text{Run}$ is given, for a detection efficiency $\eta$, by:
\begin{equation}
  N_\text{Obs} = \eta \left(N(0) - N(t_\text{Run}) \right) = \eta N(0) \frac{\ln (2)}{t^{1/2}} t_\text{Run} \, .
\end{equation}

The initial number of nuclei can be written as:
\begin{equation}
  N(0) = \frac{N_{A} m}{M} \, ,
\end{equation}
\noindent where $N_A$ is Avogadro's number, $m$ is the mass of the isotope of interest and $M$ its atomic mass.

The measured half-life is therefore given, as a function of the experimental quantities defined above, as:
\begin{equation}
  t^{1/2} = \frac{\eta}{N_\text{Obs}} \frac{N_A m}{M} \ln (2) \, t_\text{Run} \, .
\end{equation}

\section{Analysis data set}
For the analysis described in this thesis the \emph{standard} set of NEMO-3 runs is used. In this sample runs are included in which part of the PMT DAQ system was switched off in addition to runs where the entire detector was operating nominally.

The data set is split in two subsets: phase 1 includes the runs taken before the installation of the anti-radon facility while runs taken with the facility in operation constitute phase 2.

The exposure time of the data set is listed in Table \ref{tab:run_time}. With a \Ca{48} mass of \mbox{6.9 \gram}, the total exposure of the analysed data is 36.7\gyr.

\begin{table}[htpb]
  \begin{tabular}{ c c c c }
    Phase         &      Run time (days)     &   Dead time (days)   &   Exposure (days) \\ \hline
    1             &      391.9               &    5.3               &   386.6 \\
    2             &      1548.3              &    16.3              &   1531.9 \\
    Both          &      1940.2              &    21.6              &   1918.5 \\ 
  \end{tabular}
  \caption[Exposure of the NEMO-3 \emph{standard} analysis run set.]{Exposure of the NEMO-3 \emph{standard} analysis run set. Exposures are given for each phase and for the entire run set.}
  \label{tab:run_time}
\end{table}
