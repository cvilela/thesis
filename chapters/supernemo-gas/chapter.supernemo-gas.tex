\chapter{Gas mixing system for the Demonstrator module}
\label{sec:sn-gas}
As described in Section \ref{sec:sn-sn-cell} the SuperNEMO tracker cells operate in a gas mix of helium, ethanol and argon at 95\%, 4\% and 1\% fractions by volume, respectively. The three components of the gas mix are obtained separately, with the gaseous components being supplied in cylinder manifolds and the ethanol in its liquid state. The system to mix the three components of the gas and supply it to the SuperNEMO Demonstrator was recovered from NEMO-3 and largely rebuilt for the SuperNEMO Demonstrator. The main purposes of the rebuild were to reduce the \Rn{222} emanation level of the system and to make it more compact and transportable such that it can be used both for the commissioning of the tracker and for running the experiment in its final configuration at LSM.

\section{Principle of operation}
The purpose of the gas system is to supply the tracking detector with a well defined and adjustable gas mixture at a given flow rate, while maintaining a small but constant over-pressure inside the detector volume.

Helium and argon are supplied to the system from separate gas cylinder manifolds. The gases enter the system through separate lines, each going through a filter to remove any particulate contaminants that might have been introduced into the gas flow upstream of the system (Figure \ref{fig:flow-diagram}). The required ratio of helium to argon is achieved by regulating the flow rate of the gases in the two lines with mass flow controllers (MFCs). The MFCs contain an electrically operated flow control valve and measure the flow rate of the gas through thermal dispersion: the gas is heated at one point and its temperature is measured at the same point and further downstream. The resulting temperature gradient depends on the flow rate and composition of the gas, with the flow meters being factory calibrated for the required gases, allowing for the measurement of the flow rate. A system supplied by the manufacturer is connected to the MFCs and regulates the flow control valve such that the desired flow rate is achieved.

The two MFCs in the system have a range up to 1.3 and 0.13 m$^3/$hr for helium and argon, respectively. This allows for a range of argon to helium ratios to be used, depending on the required flow rate. At the nominal ratio of 1\%, the system allows for precise control of the gas mixture from around 0.2 to 1.3 m$^3/$hr, with the maximum flow rate corresponding to 2.6 replacements of the Demonstrator's internal volume per day. This volume change rate is two orders of magnitude faster than what was used in NEMO-3.

The system includes a line for faster flow rates which bypasses the MFCs, going instead through a less accurate variable area flow meter which provides readings up to 30 m$^3/$hr. The flow rate on this line is manually adjustable with a flow control needle valve. This line is intended for initially flushing the detector with pure helium to remove air and moisture from the system.

\begin{figure}[htpb]
  \def\svgwidth{\textwidth}
  \import{chapters/supernemo-gas/}{gas-sys-flow.pdf_tex}
  \caption[Flow diagram for the Demonstrator's gas mixing system.]{Flow diagram for the Demonstrator's gas mixing system. The gas flow is from left to right with the argon line shown on the top and the helium line on the bottom. The diversion for fast flow rates is shown between the two lines. The vent pressures are indicated over the pressure relief valves and the maximum flow rates for each flow controller/meter are displayed above the relevant components. The two ethanol bubblers are shown on the bottom of the diagram, with typical values for the controlled conditions indicated below them.}
  \label{fig:flow-diagram}
\end{figure}

The system contains several pressure relief valves to prevent a large over-pressure being transmitted to the internal volume of the detector, in the event of a failure of one of the gas system's components (\emph{e.g.}, a pressure regulator or a flow controller). In normal operating conditions the over-pressure in the detector will be set by an output bubbler. While the design of this component is in progress, it is likely to consist of a mineral oil bubbler placed downstream of a sump. In the event of a large under-pressure being generated in the detector volume, the mineral oil is transferred to the sump without contaminating the internal volume of the detector. An over-pressure of \mbox{10 -- 20 mbar} is foreseen, corresponding to \mbox{13 -- 26 cm} of mineral oil (Figure \ref{fig:flow-gas-out}). This level of over-pressure presents an engineering challenge as it generates forces equivalent to around 15 -- 30 tonnes on the main calorimeter walls, which are themselves populated with fragile detector equipment. However, pressure measurements at LSM have revealed that the passage of heavy vehicles in the road tunnel to which the laboratory is attached can generate pressure fluctuations as large as 10 mbar over a few minutes\cite{me-press}.

\begin{figure}[htpb]
  \def\svgwidth{\textwidth}
  \import{chapters/supernemo-gas/}{gas-output.pdf_tex}
  \caption[Diagram of a tentative gas outlet system of the Demonstrator.]{Diagram of a tentative gas outlet system of the Demonstrator. The gas flow is from left to right with the gas coming out of a detector going through a sump followed by a mineral oil bubbler from which it is released to the atmosphere. The height of the mineral oil column ($h$) determines the over-pressure of the detector relative to the atmosphere. A height of \mbox{13 -- 26 cm} corresponds to an over-pressure of \mbox{10 -- 20 mbar}.}
  \label{fig:flow-gas-out}
\end{figure}


\subsection{Two-stage ethanol vapour addition}
The ethanol vapour component of the gas mixture is added to the helium before it is mixed with the argon. The gas is passed through two ethanol bubblers which are under controlled pressure and temperature conditions so that the required amount of ethanol evaporates into the gas flow. 

For ideal gases at equilibrium, Dalton's law states that the total pressure of a gas mix in a constant volume at constant temperature is equal to the sum of the partial pressures of the individual components, with the partial pressure of a gas component being defined as the hypothetical pressure that component would exert if it occupied the volume by itself. This can be combined with Amagat's law, stating that at fixed temperature and pressure, the total volume of an ideal gas mix is equal to the sum of its constituents' volumes, to give:
\begin{equation}
  V_i = V_\text{Total} \times \frac{p_i}{p_\text{Total}} \, ,
  \label{eq:gas-eq}
\end{equation}
\noindent where $V_i$ and $V_\text{Total}$ are the volumes of gas component $i$ and the total gas volume; $p_i$ is the partial pressure of gas $i$; and $p_\text{Total}$ is the pressure of the combined gas mix.

It is clear from Equation \ref{eq:gas-eq} that the fractional volume taken up by a gas component is given by the ratio of its partial pressure to the total pressure of the gas mix. 

Through evaporation, liquid surfaces exert a vapour pressure on their surroundings. This pressure is described, in terms of the temperature of the liquid surface, by the Antoine equation:
\begin{equation}
  \log_{10} p_\text{Vapour} = A - \frac{B}{C+T} \, ,
  \label{eq:antoine}
\end{equation}
\noindent where $p_\text{Vapour}$ is the vapour pressure at equilibrium, $T$ is the temperature and $A$, $B$, $C$ are parameters tuned for each particular substance, and valid over a range of temperatures.

The dependence of the fractional volume of a vapour on the total pressure of the vessel (Equation \ref{eq:gas-eq}) and on the temperature of the liquid (Equation \ref{eq:antoine}) is used to control the amount of ethanol added to the helium component of the tracker gas mixture.

At a first stage, the helium is bubbled through ethanol at room temperature in a large vessel under pressure. The pressure is controlled by a back-pressure regulator placed downstream of the vessel, and adjusted such that 4\% of ethanol is added to the helium (Figure \ref{fig:antoine-vs-p}). While this allows for the helium to be exposed to a large surface of ethanol, giving a high evaporation rate, the pressure control is not precise and the amount of ethanol added to the helium will change with fluctuations of the ambient temperature.

The helium mixed with roughly the required quantity of ethanol vapour is then put through a second ethanol bubbler, this one at atmospheric pressure but under precise temperature control. The volume fraction of ethanol is fine tuned as the mix passes through the bubbler: if a small excess of vapour was added in the first stage it will condense in the refrigerated bubbler; and conversely, if not enough ethanol was added in the first stage, ethanol evaporates to fill the deficit. The volume fraction of ethanol achieved at equilibrium over a range of temperatures is shown in \mbox{Figure \ref{fig:antoine-vs-t}}, for atmospheric pressures at sea level (\mbox{1 bar}) and at the level of LSM (\mbox{880 mbar}). 
\begin{figure}[htpb]
  \centering
  \subfloat[]{
    \def\svgwidth{\smallfigwidth}
    \import{chapters/supernemo-gas/}{antoine-vs-p.pdf_tex}
    \label{fig:antoine-vs-p}
  }
  \hfill
  \subfloat[]{
    \def\svgwidth{\smallfigwidth}
    \import{chapters/supernemo-gas/}{antoine-vs-t.pdf_tex}
    \label{fig:antoine-vs-t}
  }
  \caption[Fractional volumes of ethanol obtained at equilibrium under controlled temperature and pressure conditions.]{Fractional volumes of ethanol obtained at equilibrium under controlled temperature and pressure conditions. The curves follow from Equations \ref{eq:gas-eq} and \ref{eq:antoine}, where the Antoine equation parameters from\cite{NIST} were used. In (a) the volume fraction is shown as a function of the total pressure of the gas, for three temperatures of the ethanol. The dependence of the fractional volume on the temperature of the ethanol is shown in (b) for two atmospheric pressures: at sea level in red; and at the level of the underground laboratory in blue. Dotted lines indicate the parameters required to achieve an ethanol volume fraction of 4\%.}
\end{figure}

The addition of a small amount of water vapour to the gas mixture, if it is deemed necessary, can be achieved with the same method, with the vapour pressure of water at 22 $^\circ$C and 1 bar giving a partial pressure of around 3\%. 

The pressure and temperature conditions on both bubblers are electronically monitored and accessible to a computer via an RS-485 network for logging or remote monitoring. The probes and monitoring system are reused from NEMO-3. The MFCs are also connected to the RS-485 network, allowing for remote operation and logging of the flow rate readings.

\section{Materials}
Any \Rn{222} which emanates from the gas system's components or otherwise diffuses into the gas will be transferred to the detector's volume. As such, the \Rn{222} activity originating from the gas system must be considered to be part of the total \Rn{222} activity budget allowed in the tracker. While this criterion is the main constraint on the materials which can be used in the gas mixing system, compatibility with ethanol and the out-gassing of substances which might drive the ageing of the tracker must also be taken into account for the selection of materials used. A photograph of the system is shown in Figure \ref{fig:gas-photo}.

\begin{figure}[htpb]
  \def\svgwidth{0.8\textwidth}
  \import{chapters/supernemo-gas/}{gas-sys-photo.pdf_tex}
  \caption[Photograph of the Demonstrator's gas mixing system.]{Photograph of the Demonstrator's gas mixing system. The two ethanol bubblers are indicated by the dotted lines.}
  \label{fig:gas-photo}
\end{figure}

The NEMO-3 system was entirely rebuilt in stainless steel and mounted on an aluminium frame, designed for easy transportation. Stainless steel was chosen as the main material to be used in the gas routing (tube and gas connectors) as it had previously been used in the \Rn{222} concentration line with good results\cite{motty}. Another consideration is that the wires in the tracker are made of the same material and present a larger area to the gas.

Both bubblers were initially reused from NEMO-3. However, it was found that the large pressurised bubbler emanated too much \Rn{222} into the gas. After unsuccessful efforts to reduce this by sealing the bubbler with SBR and removing potentially contaminated parts (such as a glass window), it was decided to build a new vessel out of seamless tube, as the weld running the length of the NEMO-3 bubbler was suspected to be responsible for the high \Rn{222} emanation. The new component was electropolished and installed in the system and \Rn{222} emanation measurements are ongoing.

Other material considerations included removing all glass (used for ethanol level sights) and replacing all sealing materials (O-rings) with PTFE, or other materials previously used in the \Rn{222} concentration line.

\section{Tests}
Several tests were performed to verify that the gas mixing system was performing well and supplying an adequate gas mixture to the tracker.

The MFCs were tested by placing them in line with other available flow meters (both thermal and variable-area) and comparing the readings over a range of flow rates. It was also shown that the flow rates on the two lines of the gas system could be set at a ratio of 1\%, with the flow rate readings reporting stable values over a range of flow rates.

The amount of ethanol being evaporated into the gas flow was verified using only the second-stage refrigerated bubbler. The system was operated at a flow rate of \mbox{0.12 m$^3/$hr} for two days, during which the level of ethanol in the bubbler was monitored. The readings agreed with an expected depletion rate of 1 cm$/$day, corresponding to a volume fraction of ethanol vapour in the gas mixture of 4\% (Figure \ref{fig:eth-frac}).

\begin{figure}[htpb]
  \def\svgwidth{\mediumfigwidth}
  \import{chapters/supernemo-gas/}{ethanolFraction.pdf_tex}
  \caption[Ethanol volume fraction in the gas measured by the depletion rate of liquid ethanol.]{Ethanol volume fraction in the gas measured by the depletion rate of liquid ethanol. Volume fractions corresponding to the difference in the ethanol level between each reading are shown as black markers. The measurement equivalent to the level drop over the course of the full two days is shown as a red line with a yellow band representing the estimated uncertainty.}
  \label{fig:eth-frac}
\end{figure}

The final test of the gas mixing system was performed by using it to run the single-cell tracker prototype at UCL. This test did not include the pressurised bubbler, with the ethanol being added only in the second-stage and pre-mixed cylinders of the helium/argon mixture were used. Good performance of the cell was attained and the system was run for a long time during which studies on the ageing of the tracker cell and on the detector performance under high flow rates were carried out.

\section{Flow rate and \Rn{222} activity in the tracker}
The replacement rate of the gas inside the detector volume can be used as a tool to further reduce the backgrounds due to \Rn{222} in the detector. As an inert gas \Rn{222} will tend to simply follow the gas flow in and out of the detector. However, \Rn{222} will $\alpha$ decay to \Po{218} with a half-life of 3.8 days, leaving it in an ionised state, with a tendency to drift towards the wires and surfaces of the detector and stick to them. The backgrounds resulting from the progeny of \Rn{222} can therefore be reduced by increasing the flow rate of the gas through the detector, flushing away \Rn{222} before it decays. The suppression factor from the gas flow is given by:
\begin{equation}
  F = \frac{1}{(\phi\tau/V) + 1} \, ,
\end{equation}
\noindent where $\phi$ is the gas flow rate, $V$ is the detector internal volume (15.2 m$^3$) and $\tau$ the life-time of \Rn{222}. At the nominal flow rate envisaged for the SuperNEMO Demonstrator, of \mbox{0.5 m$^3/$hr}, the \Rn{222} activity inside the detector would be reduced by a factor of $1/F=5$ compared to the static case\cite{motty}. At the maximum flow rate supported by the gas system (\mbox{1.3 m$^3/$hr}), a larger reduction factor of 12 would be obtained. An increase in the flow rate by a factor of two has been shown to produce no negative effects on the operation of a SuperNEMO tracker cassette\cite{decapua-flow}.

If very high gas flow rates are required, to reduce the \Rn{222} activity in the detector, it is likely that the cost of helium, which in the baseline design is vented to the atmosphere after being used in the detector, will become the limiting factor. In this case, a gas re-circulation system might be necessary. This would require the removal of \Rn{222} out of the used gas before injecting it back into the detector. The removal of \Rn{222} by passing the gas through a charcoal trap would require the gas to be largely free of ethanol, as it would otherwise saturate the charcoal, preventing the adsorption of \Rn{222}. Cooling the gas down to -40 $^\circ$C and 5 bar would condense most of the ethanol out, leaving, at equilibrium, around 0.01\% of vapour in the gas mixture. Studies are ongoing to investigate the performance of charcoal traps for gas mixtures with this amount of ethanol vapour\cite{busto}.
