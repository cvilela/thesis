\chapter{The SuperNEMO tracker}
\label{sec:tracker}
As evidenced in the first part of this thesis, the strength of NEMO-3 relied in identifying electrons emerging from the source foil and tracking them to an energy deposition in the calorimeter. This method allows for the unique identification of electrons, distinguishing them from other types of naturally occurring radiation which would otherwise be a very significant background to double-$\beta$ decay. Furthermore, individually tracking the electrons emitted in processes of interest allows for the measurement of their individual energies as well as the opening angle between them. The time-of-flight criteria that are essential in removing the significant backgrounds originating from sources external to the detector also rely fundamentally on measuring the position of the electron vertices in the source foil and the length of their tracks.

The considerations above make clear the necessity for a large-scale detector which is capable of tracking electrons over distances which allow for time-of-flight discrimination. 

At energies of a few\MeV multiple scattering has a significant impact on electron trajectories. Given that the energy measurement in SuperNEMO is performed with a dedicated calorimeter, energy losses through ionisation in the tracking chamber should be as low as possible, while still allowing for efficient tracking. These considerations motivate a tracking detector based on a low-$Z$ medium with a minimal amount of material. 

As with all other detector components, it is essential that the tracker is made as radiopure as possible. In particular, contamination on the tracker layers closest to the source foil are indistinguishable from source foil contamination, from a data analysis perspective.

Finally, it is important that the tracker has a good vertex resolution: the identification of vertices common to pairs of electrons plays a crucial role in the selection of double-$\beta$ decay events and fiducial cuts are essential to remove contaminated areas of the detector from the analysed data. An accurate measurement of the entry point of electrons in the scintillator blocks is also desired as it allows for corrections to be applied to the energy measurement.

The SuperNEMO tracker uses the same detection principle as NEMO-3, meeting the requirements outlined above by having an array of cells operating in the Geiger regime immersed in a He based gas mixture. This chapter describes the SuperNEMO tracking detector: the fundamental principles of Geiger counters are discussed in Section \ref{sec:sn-gg}; a detailed description of the SuperNEMO tracker cells is given in Section \ref{sec:sn-sn-cell}; and of the tracker frame in Section \ref{sec:sn-tracker-frame}. Overviews of the tracker assembly process and of its associated electronics and DAQ are given in \mbox{Sections \ref{sec:sn-tracker-const} and \ref{sec:sn-tracker-elec}}.

\section{The physics of Geiger counters}
\label{sec:sn-gg}
Geiger counters rely on the ionisation of atoms in a gas to detect passing charged particles. A strong electric field is maintained across a volume of gas by electrodes. Typically the anode is a thin wire in the centre of a cylinder or collection of wires which form the cathode. Ionisation of the gas results in the freed electrons drifting towards the anode and the heavier ions moving slowly in the opposite direction. The ionised electrons acquire enough energy that they too will ionise the gas, resulting in an avalanche of electron-ion pairs drifting in opposite directions. The movement of these charges in the electric field induces a current on the electrodes in a phenomenon known as the Townsend avalanche.

In the Geiger regime, the electric field and gas composition are tuned such that avalanches become saturated. As the avalanche reaches the anode wire, the number of ion pairs produced is so large that the charge of relatively motionless ions clustered around the anode reduces the electric field strength substantially. At this point, newly ionised electrons will not acquire sufficient energy to further ionise the gas. The region of the anode wire where this condition is met is said to be burned out\cite{Wilkinson1992195}. The result of this effect is that regardless of the number of atoms ionised by the traversing particle, the integrated current induced on the electrodes will be, on average, the same. This makes for very efficient detectors, given that even the creation of a single electron-ion pair will induce a relatively large current across the electrodes, and allows for the large signals to be transmitted to the electronics racks without pre-amplification. This is advantageous both in terms of the cost of the electronics systems and in terms of radiopurity, as all tracker electronics can be placed outside the detector shielding.

Another feature of avalanches in the Geiger regime is their propagation along the very strong electric field region close to the anode wire. As sections of the wire become burned out, the avalanche spreads stochastically along the wire to regions where avalanche breeding is still possible. This occurs through the isotropic emission of de-excitation and recombination photons in an avalanche which ionise the gas in different points in the detector, seeding new avalanches\cite{Wilkinson1996516,PhysRev.72.602}. Once the entire wire is burned out, the avalanche breeding stops and the detector is left in a non reactive state. The dead time resulting from the burn out and subsequent recovery of Geiger detectors is typically of the order of $\micro$s -- ms, making these detectors unsuitable for high-rate applications.

A sketch of the Geiger counter phenomena described above is shown in \mbox{Figure \ref{fig:geiger}}.

\begin{figure}[htpb]
  \def\svgwidth{1.3\largefigwidth}
  \import{chapters/supernemo-tracker/}{Geiger.pdf_tex}
  \caption[Sketch of the signal generating process in Geiger counters.]{Sketch of the signal generating process in Geiger counters. A generic Geiger counter is shown, formed of a cylindrical grounded cathode with a thin anode wire in the middle which is set at a high potential. The diagram on the left shows a view looking into the anode wire and on the right the length of the cell is seen. A passing charged particle is shown as a black dotted line and the avalanches in blue. On the right, wavy lines indicate the photons which seed secondary avalanches. The radial and longitudinal coordinates of the point of closest ionisation are indicated as $r$ and $z$, respectively\cite{manu}.}
  \label{fig:geiger}
\end{figure}

In general, Geiger detectors require quenching to prevent the avalanches becoming self-sustained: if regions of the detector recover to a state where new avalanches can be formed before avalanche breeding has died down elsewhere in the detector, the seeding of avalanches can continue indefinitely producing a continuous current across the electrodes, rendering the device useless as a particle detector. Quenching can be achieved by reducing the voltage applied to the anode as a signal is detected or, more commonly, by using a self-quenching gas mixture. In the latter mode of operation, a quencher (usually an organic vapour) is added to the gas which absorbs photons produced in avalanches. This process occurs primarily through the dissociation of the quencher molecules\cite{geiger-disso,PhysRev.72.602}. 

A common challenge encountered in the operation of Geiger detectors is the degradation of the detector's operating characteristics over time. One of the causes of this is the contamination of the detector's surfaces with quencher residues. The dissociation of the organic molecules produces polymers which can drift to the wires. Over time polymer chains are formed which disrupt the electric field close to the wire\cite{geiger-disso}. This can result in the blockage of avalanche propagation or in spurious signals caused by the ejection of electrons from the conductor's surface into the gas volume in what is known as the Malter effect\cite{PhysRev.50.48}. Ageing can be mitigated by including additives in the gas mix which prevent polymerisation by reacting with the quencher residues to form stable and volatile molecules which are simply flushed away with the gas flow. Common additives include simple oxygen rich molecules such as CO$_2$ and water vapour\cite{Vavra20031}.


\section{The SuperNEMO tracker cells}
\label{sec:sn-sn-cell}
The SuperNEMO tracker consists of an array of cells each of which functions as a Geiger detector, as described above. Each cell is formed of 13 wires: one anode wire in the middle of the cell to which a high voltage is applied; and 12 field-shaping wires which are grounded. The cells are 3 m long with a nominal diameter of \mbox{4.4 cm}. The anode wire has a diameter of 40\um while the field shaping wires have a diameter of 50\um. The larger diameter for the anodic wires was chosen to minimise ageing effects due to polymer deposition on the cathode. Two copper rings, 4.2 cm in diameter and 4 cm long, are placed on the ends of the cell. These rings are grounded and the current induced on them by the arrival of the avalanches is read out. The geometry of a SuperNEMO Geiger cell is sketched in Figure \ref{fig:sn-cell-sketch}.

\begin{figure}[htpb]
        \centering
        \subfloat[]{
          \def\svgwidth{0.17\textwidth}
          \import{chapters/supernemo-tracker/}{celltop.pdf_tex}
          \label{}
        }
        \hfill
        \subfloat[]{
          \def\svgwidth{0.73\textwidth}
          \import{chapters/supernemo-tracker/}{cellside.pdf_tex}
          \label{}
        }
        \caption[Sketch of a SuperNEMO tracker cell.]{Sketch of a SuperNEMO tracker cell. The geometry of a Geiger cell is shown in the view looking into the wires in (a) and looking across the wires in (b). The field shaping wires are shown as black dotted lines and the anode wire is drawn in red. The copper rings are shown at both ends of the cell in brown.}
        \label{fig:sn-cell-sketch}
\end{figure}

The tracker operates in the same atmosphere as NEMO-3, the main component of the gas being helium, with argon and ethanol added at 1\% and 4\% volume fractions. Argon has a lower ionisation energy than helium (15.8\eV and 24.6\eV, respectively\cite{NIST}) and its addition to the tracking gas was found to enhance the propagation of the avalanches. Ethanol is used as a quenching agent. While in NEMO-3 a small quantity of water vapour (1500 ppm) was added to the tracking gas, no significant improvement was found in the tracker operation. The addition of water vapour to the SuperNEMO tracking gas is not envisaged at first.

In standard operation, a potential difference of around 1800 V is applied between the grounded field shaping wires and the anode wire. The optimal operating voltage is specific to each cell, depending on the position of the cell in the array (\emph{i.e.}, the number of neighbouring cells) and on the intrinsic properties of the cell. The main properties which characterise a tracker cell are the Geiger plateau width, the avalanche propagation efficiency and the avalanche propagation time.

The Geiger plateau is defined as the range of anode voltages for which the cell is in the Geiger regime, with an approximately constant amount of charge being generated with each hit. For voltages below the plateau the efficiency of the cell decreases significantly as the size of the signal pulses becomes small and dependent on the number of gas atoms ionised by the traversing particle. At voltages above the plateau the cells become unstable and spuriously trigger at high rates (hundreds of Hz) generating large currents. A robust operation of the cells is achieved when a voltage roughly in the middle of the Geiger plateau is used. A Geiger plateau spanning a wide range of voltage is thus a desirable characteristic of a cell. Typical plateaus for SuperNEMO tracker cells are \mbox{200 -- 300 V} wide, starting at around \mbox{1700 V}. Examples of Geiger plateaus measured in SuperNEMO tracker cells can be seen in Figure \ref{fig:plateau-scan}.

The avalanche propagation efficiency is defined as the fraction of hits where the avalanches have propagated the whole length of the cell, reaching the pick-up rings. For voltages in the Geiger plateau this efficiency is very high being typically above 99\%. However, it drops significantly for voltages outside the plateau.

An additional characteristic related to the propagation of avalanches which is a useful tool to assess the performance of tracker cells is the time it takes for the avalanches to propagate through the entire length of the wire. This property depends on the operating conditions, with times becoming shorter at higher voltages. In standard operating conditions (middle of the plateau) the avalanches spread through the 3 m long cells in around 50\us.

After a traversing particle triggers an avalanche in the cell which successfully propagates through the length of the wires, there is a period during which the cell is insensitive to additional passing particles, \emph{i.e.}, no part of the wire is able to produce avalanches, or if avalanches start developing they will reach infertile regions of the wire where they will die down and will not propagate the entire length of the cell. This period of dead time or reduced propagation efficiency has been measured in SuperNEMO cells to be 3 ms, with measurable but harmless effects lasting up to \mbox{10 ms}\cite{manu-dead-time}.

\begin{figure}
  \includegraphics[width=\mediumfigwidth]{chapters/supernemo-tracker/cellSignal.png}
  \caption[Example set of signals from a Geiger discharge in a SuperNEMO tracker cell.]{Example set of signals from a Geiger discharge in a SuperNEMO tracker cell. The signal extracted from the anode wire is shown in red and the cathode signals extracted from the pick-rings are shown in green\cite{manu-tracker-signal}.}
  \label{fig:sn-cell-sig}
\end{figure}

An example of a set of signals from a Geiger discharge in a SuperNEMO tracker cell is shown in Figure \ref{fig:sn-cell-sig}. Three signals are read-out from each cell: one anode pulse on the wire and two cathode pulses from the pick-up rings. The anode pulse is of negative polarity and it lasts for the duration of the avalanche propagation process, beginning with the earliest avalanche reaching the vicinity of the anode wire and ending when the avalanches reach both ends of the cell. Once one end of the cell is reached by the avalanches, the amplitude of the anode signal drops to roughly half of its initial value as with only one ``propagation front'' the amount of charge being generated is halved. As the avalanches reach the copper rings at the ends of the cell a current is induced on them. This generates a short pulse, positive in polarity. The amplitude of the signals is highly dependent on the impedance of the read-out circuit. Typical amplitudes for the anode pulse are 50 -- 200 mV, while the cathode pulses are smaller at 10 -- 20 mV.

From the arrival times of this set of signals ($t_{Anode}$, $t_{Cathode_1}$ and $t_{Cathode_2}$) two positional coordinates can be extracted: $r$, the radial distance of the closest ionisation point to the anode wire; and $z$, the position along the length of the cell.

The arrival time of the anode signal $t_{Anode}$ is measured relative to the much faster signal of the calorimeter. An avalanche will take up to 7\us to reach the anode wire, depending on how far from it the initial point of ionisation is. This time can be converted back to a radial position with a drift model which is measured by fitting tracks to a series of hits in a cell array. In NEMO-3 the drift model took the form:
\begin{equation}
  r = \frac{A \times t_{Anode}}{t_{Anode}^B + C} \, 
\end{equation}
\noindent where $A$, $B$ and $C$ are parameters measured in data. Alternative drift models are under investigation for SuperNEMO, some of which include the angle of incidence of the track as a parameter.

The propagation times of the avalanche to the ends of a tracker cell are given by the difference between the arrival times of the anode pulse and the relevant cathode ring pulse: $t_{Prop_i} = t_{Cathode_i} - t_{Anode}$. These times can be as long as 50\us, depending on the voltage applied to the anode wire. The sum of the two cathode times ($t_{Prop_1} + t_{Prop_2}$) is the avalanche propagation time described above. In NEMO-3, the coordinate $z$ was extracted via the expression:
\begin{equation}
  z = \frac{L_{eff}}{2} \, \frac{t_{Prop_2} - t_{Prop_1}}{t_{Prop_1} + t_{Prop_2}} \, \left( 1 + D \left| \frac{t_{Prop_2} - t_{Prop_1}}{t_{Prop_1} + t_{Prop_2}} \right| \right) \, ,
\end{equation}
\noindent where $L_{eff}$ is an effective length, roughly corresponding to the length of the cells with the copper rings subtracted, $D$ is a tuned parameter and $Cathode_2$ is on the top of the detector (positive $z$). The last factor in the expression above corrects for the fact that the total propagation time is longer for hits very close to one end of the cell, where only one ``propagation front'' travels the entire length of the cell.

\section{The tracker frame}
\label{sec:sn-tracker-frame}
In a SuperNEMO module the source foil is sandwiched between two tracker frames. A tracker frame accommodates 1017 cells in a 9 $\times$ 113 configuration. Each frame is further subdivided into two C-sections, named after their C-shaped structural frames. Each C-section contains 504 cells in a 9 $\times$ 56 configuration. The 9 additional cells required to complete the frame are positioned at the joining of two C-sections. These will be added at the time of detector integration at LSM.

In addition to the tracker cells, the tracker frame is populated with calorimeter modules, with a C-section having 32 modules on its vertical wall (X-wall blocks) and 16 larger blocks on the top and bottom (veto blocks). These modules are identical in construction to the NEMO-3 calorimeter blocks and hence of lower specification than those used on the main walls, with energy resolutions of around 10\% and 16\% at 1\MeV for X-wall and veto blocks, respectively. The modules are equipped with the best performing 5'' PMTs recovered from NEMO-3 and the scintillator blocks are custom made to fit the dimensions of the new detector.

A sketch of a C-section is shown in Figure \ref{fig:sn-c-section-sketch}, where the cells and calorimeter modules are shown but the structural frame is omitted.

\begin{figure}[htpb]
        \centering
        \subfloat[]{
          \def\svgwidth{0.9\textwidth}
          \import{chapters/supernemo-tracker/}{c-section-side.pdf_tex}
          \label{}
        }
        \hfill
        \subfloat[]{
          \def\svgwidth{0.9\textwidth}
          \import{chapters/supernemo-tracker/}{c-section-top.pdf_tex}
          \label{}
        }
        \caption[Sketch of a SuperNEMO tracker C-section.]{Sketch of a SuperNEMO tracker C-section. Tracker cells are shown as in Figure \ref{fig:sn-cell-sketch} with the field shaping wires omitted for clarity. Scintillator blocks are shown in blue and PMTs in grey. In (a) the positions of the source foil and the main calorimeter wall are shown relative to the C-section. Dimensions are approximate and not to scale.}
        \label{fig:sn-c-section-sketch}
\end{figure}

\section{Construction of the tracker}
\label{sec:sn-tracker-const}
The SuperNEMO tracker cells are produced at the University of Manchester in units of 2 $\times$ 8 referred to as cassettes. All the components that will be in contact with the tracker volume go through an intensive cleaning procedure adapted from a technique with proven results in other low-background experiments\cite{Maneschg2008448}.

In addition to the cleaning, the stainless steel wire undergoes a testing procedure during which it is spooled through a test tank that mimics the operating conditions of a tracker cell. Data is acquired under cosmic rays as the wire is passed through the tank, allowing for the assessment of the wire performance. In particular, points that block the propagation of avalanches and points responsible for high rates of spurious discharges are identified. These can be caused by dust contamination or imperfections (dents or twists) on the wire. Sections of the wire that perform poorly are rejected\cite{manu-wire-test}.

The cassettes are strung by a purpose built robot and without human manipulation of the components (Figure \ref{fig:sn-robot}). They are then transferred to a tank that is filled with the tracking gas mixture and a high voltage is applied to the anodes. A conditioning period of around two days is required for all the cells to stabilise and start showing normal operation characteristics. Cassettes that fail to stabilise during the conditioning period are rejected. Criteria for the acceptance of a cassette include: uniform hit rate, signal amplitude, avalanche propagation time and optimal operating voltage across all cells; avalanche propagation efficiency above 95\% for all cells; and all plateaus being wider than 50 V. The conditioning of the cassettes dominates the total production time, with cassettes being built and tested at a rate of two per week.

\begin{figure}[htpb]
  \includegraphics[width=0.7\mediumfigwidth]{chapters/supernemo-tracker/robot-cassette_light.jpeg}
  \caption[Photograph of the wiring robot in the University of Manchester clean-room.]{Photograph of the wiring robot in the University of Manchester clean-room. The end-caps of an assembled cassette are seen in the foreground and the wiring robot in the background.}
  \label{fig:sn-robot}
\end{figure}

The calorimeter modules are assembled and tested at UCL. They are then transported to the Mullard Space Science Laboratory (MSSL) where the structural frames of the C-sections are built. After being populated with the calorimeter modules, the C-sections are sealed and the \Rn{222} activity inside them is measured. Upon satisfactory results the cassettes are inserted into the C-section frame (Figure \ref{fig:sn-cell-insertion}). At the time of writing the first C-section (C0) has been fully populated with tracker cells. 

\begin{figure}[htpb]
  \includegraphics[width=0.9\textwidth]{chapters/supernemo-tracker/mssl-c-section-photo.jpg}
%  \caption[Photograph of the first cassette being inserted into the C0 frame.]{Photograph of the first cassette being inserted into the C0 frame at the MSSL clean-room. The back of the X-wall calorimeter blocks is seen in the foreground and in the background the cassette is being moved into its position in the frame.}
  \caption[Photograph of the C0 frame at MSSL, partially populated with cells.]{Photograph of the C0 frame at MSSL, partially populated with cells. The X-wall calorimeter modules PMTs are seen in the foreground.}
  \label{fig:sn-cell-insertion}
\end{figure}


Before the C-section is shipped to LSM, the \Rn{222} activity in the fully populated frame is re-measured. At this point the tracker cells comprising an entire C-section are commissioned under cosmic rays. This is discussed in detail in Chapter \ref{sec:ch-elec}.

\section{Tracker electronics and DAQ}
\label{sec:sn-tracker-elec}
The SuperNEMO tracker HV supply and DAQ systems differ significantly from those used in NEMO-3. 

The main improvement in the HV distribution system is the ability to regulate the supply to individual cells within a range 500 V from a common supply feeding several cells\cite{perry-sn-hv}. This will allow for each cell to be operated at its optimum voltage level.

Regarding the DAQ system, a novel technique to measure the longitudinal position ($z$) of tracker cell hits has been developed which, if successful will reduce the number of DAQ channels for future SuperNEMO modules. In parallel with recording the arrival time of pulses on the pick-up rings, the Demonstrator electronics will pass the anode signal through a differentiating circuit and record the time of a series of three peaks. The first peak indicates the leading edge of the anode pulse, \emph{i.e.}, the arrival of the avalanche to the anode wire, and the further two peaks signal the arrival of the avalanche to the ends of the cell (Figure \ref{fig:sn-der-an}). These three recorded times allow for the full reconstruction of a tracker cell hit, apart from an ambiguity regarding which $t_{Cathode_i}$ corresponds to the arrival of the avalanche to the top and bottom of the detector. This degeneracy can be resolved by either reading out a single pick-up ring, or by matching tracks to calorimeter hits at track reconstruction level.

\begin{figure}[htpb]
  \def\svgwidth{\mediumfigwidth}
  \import{chapters/supernemo-tracker/}{derivative-anode.pdf_tex}
  \caption[Derivative of the anode signal.]{Derivative of the anode signal. The leading edge of the anode pulse is labelled $t_{Anode}$ and the peaks which signal the arrival of the avalanche to the ends of the cell are indicated as $t_{Cathode_i}$\cite{manu-derived-sig}}
  \label{fig:sn-der-an}
\end{figure}


% - HV supply with individual voltage adjustment
% - novel technique for reading-out the signals with the derivative of the anode will be tested
