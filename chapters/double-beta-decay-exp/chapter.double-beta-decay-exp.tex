\chapter{Double-$\beta$ decay experiments and techniques}
\label{sec:dbd-exp}
The experimental investigation of \0nu is a rich field of research, with a wide range of complementary techniques employed to place increasingly stringent limits on the half-life of this as yet unobserved\footnote{The claim of observation of the \0nu of \Ge{76}\cite{KlapdorKleingrothaus:2004wj} has been largely discredited\cite{Aalseth:2002dt} and is currently all but directly ruled out\cite{kamland-zen,PhysRevLett.109.032505,Agostini:2013mzu}.} process. Indirect detection experiments using radio and geochemical techniques have been performed. These experiments do not allow for discrimination between the \2nu and \0nu modes of the decay, being sensitive only to the combined decay rate\cite{0034-4885-75-10-106301}. The experimental techniques used in the direct detection of double-$\beta$ decay processes are discussed in this chapter, with a brief overview of experiments using different technologies being given in Section \ref{sec:dbd-det-tech} and experiments using \Ca{48} as the source being described in Section \ref{sec:dbd-ca-48-exp}. The current best double-$\beta$ decay measurements are listed in Section \ref{sec:dbd-results-summary}. 

The sensitivity to the \0nu half-life can be estimated for a generic experiment\cite{RevModPhys.80.481}:
\begin{equation}
t^{1/2}_{0\nu} = \frac{4.16 \times 10^{26} \, \text{yr}}{n_\sigma}  \, \left( \frac{\varepsilon \, a \, M \, t}{W} \right)  \, \sqrt{ \frac{1}{ N_b  }  }  \, ,
\label{eq:generic-sens}
\end{equation}
\noindent where:
\begin{itemize}
  \item $n_\sigma$ is the number of standard deviations corresponding to the exclusion CL ($n_\sigma = 1.64$ for 90\% CL);
  \item $\varepsilon$ is the signal detection efficiency;
  \item $a$ is the abundance of the double-$\beta$ decay isotope;
  \item $W$ is the molecular weight of the source material;
  \item $M\, t$ is the exposure of the source in kg yr;
  \item and $N_b$ is the number of expected background events.
\end{itemize}

The equation above is derived under the assumption that the number of background events is distributed according to a Poisson distribution, with its uncertainty being given by $\sqrt{N_b}$.

For the half-life sensitivity of a double-$\beta$ decay experiment to be maximised, each of the variables in Equation \ref{eq:generic-sens} must be considered.

The half-life sensitivity increases linearly with the signal detection efficiency. Double-$\beta$ decay experiments can generally be categorised as having a homogeneous or heterogeneous design. The first refers to experiments in which the source is an integral part of the sensitive components of the detector, while in the latter designs the source is a separate part of the detector, independent of the sensitive components. Experiments of the homogeneous type in general have higher detection efficiency, with limiting factors on this parameter including, for example, the light attenuation in large scintillator detectors. Conversely, heterogeneous detectors tend to have lower efficiencies, due to energy losses in the non-sensitive source, or incomplete coverage of the source by the sensitive components.

The number of double-$\beta$ isotope nuclei in an experiment is determined by the interplay of three factors: the source mass, the abundance of the isotope and its molar mass. 

The mass of the source is the ultimate limit for the number of nuclei in an experiment, and therefore experiments aim to increase the source mass as much as possible. One negative consequence of a large source mass is that, in general, backgrounds will increase with the source mass. Experiments of the homogeneous type are more suitable for very large source masses, with proposed experiments aiming towards a tonne scale source\cite{BarnabéHeider2012141,Shirai201328,Gomez-Cadenas:2013lta}.

Isotopes with a small molar mass allow for a large number of nuclei for a given source mass. Using such an isotope might help mitigate the problem of the increase in background counts associated to larger source masses. It should be noted, however, that in terms of extracting the physics parameters of interest, such as $\braket{m_{\beta\beta}}$, the nuclear matrix elements and phase space factors must be taken into consideration when selecting the source isotope.

The last factor determining the number of nuclei in an experiment is the abundance. With the exception of \Te{130}, the isotopes considered for double-$\beta$ decay experiments occur with low natural abundances (Table \ref{tab:isotope-q-abundance}). This requires the sources to be enriched in the isotope of interest. With the exception of \Ca{48}, \Zr{96} and \Nd{150}, the isotopes can be enriched by means common in the nuclear industry, for example, by being synthesised into hexafluorides and processed in gas centrifuges. This allows for affordable enrichment at relatively large scales. It has been argued, however, that for possible future experiments aiming towards isotope masses of tens of tonnes (for probing the normal hierarchy parameter space) the costs related to enrichment would become prohibitive, in which case the use of \Te{nat} stands out as a viable alternative\cite{Biller:2013wua}. The only method currently available to enrich the isotopes which cannot be enriched through centrifugation is electromagnetic separation. This method allows only for small amounts to be enriched at a time and is costly. Alternative means of enrichment are being investigated such as laser separation methods. The very small natural abundance of \Ca{48} coupled with the lack of an affordable enrichment procedure makes the enrichment of this isotope particularly challenging. %in a reasonable scale particularly difficult. %Only a few grams of Ca were ever enriched to a high level in \Ca{48} (75.3\%), with a portion of this material being used in the NEMO-3 experiment (Section \ref{sec:det_ca_source}).

\begin{table}[htpb]
  \begin{tabular}{ c c c }
    Isotope    &    $Q_{\beta\beta}$ (keV)    & Natural abundance (\%) \\ \hline
    \Ca{48}    &    4273.7                    &  0.187                 \\
    \Ge{76}    &    2039.1                    &  7.8                   \\
    \Se{82}    &    2995.5                    &  9.2                   \\
    \Zr{96}    &    3347.7                    &  2.8                   \\
    \Mo{100}   &    3035.5                    &  9.6                   \\
    \Cd{116}   &    2809.1                    &  7.6                   \\
    \Sn{124}   &    2287.7                    &  5.6                   \\
    \Te{130}   &    2530.3                    &  34.5                  \\
    \Xe{136}   &    2461.9                    &  8.9                   \\
    \Nd{150}   &    3367.3                    &  5.6                   \\
  \end{tabular}
  \caption[$Q_{\beta\beta}$ and natural abundances for isotopes commonly considered for double-$\beta$ decay experiments.]{$Q_{\beta\beta}$ and natural abundances for isotopes commonly considered for double-$\beta$ decay experiments\cite{Rodejohann:2012xd}.}
  \label{tab:isotope-q-abundance}
\end{table}

The other important parameter in Equation \ref{eq:generic-sens} is the expected number of background counts. In general, the background counts will increase linearly with the exposure ($N_b \propto M \, t$), making the half-life sensitivity proportional to the square root of the exposure ($t^{1/2}_{0\nu} \propto \sqrt{M \, t}$). If on the other hand the background expectation is too small for any events to occur due to backgrounds, or more generally, if the expected number of background events does not increase with the exposure, the relation is linear ($t^{1/2}_{0\nu} \propto M \, t$). It is therefore crucial to achieve the lowest possible rate of background counts in a double-$\beta$ decay experiment.

The main background sources for double-$\beta$ decay experiments are due to the decays of \Bi{214} and \Tl{208}, present in the uranium and thorium natural decay chains. A detailed discussion of these backgrounds is given in Chapter \ref{sec:bkgd_meas}. Detector materials must be screened and chosen very carefully for their radiopurity to prevent these backgrounds. Contamination of the sources is particularly dangerous and source materials go through purification procedures to remove potential sources of background. Another important consideration is the use of isotopes with high $Q_{\beta\beta}$, such that the \0nu peak occurs at energies higher than those of the naturally occurring backgrounds. In particular, sources with $Q_{\beta\beta}$ higher than the most energetic $\gamma$ emission from \Tl{208} (2.6\MeV) are preferred. Having the highest $Q_{\beta\beta}$, \Ca{48} is a highly desirable source to this effect, as very low levels of background are expected in the region of its \0nu peak.

Given that the \0nu signal consists of a sharp peak at $Q_{\beta\beta}$, a good energy resolution is a powerful tool to reduce the number of background counts. By selecting a narrow energy window to measure the \0nu peak a significant fraction of the more broadly distributed background can be rejected.

An additional contribution to the background level in double-$\beta$ decay experiments has its origin in sources external to the detector. Experiments rely on passive and active forms of shielding to reduce these backgrounds. To mitigate the backgrounds due to cosmic rays, detectors are placed in underground laboratories, typically at depths of a few thousand meters of water equivalent. Dense shielding materials are placed around the detectors to absorb $\gamma$ radiation, often coupled with neutron-moderating materials to prevent neutron activation of materials inside the detectors. Active shielding takes the form of detector components designed to detect crossing particles, such that events due to external backgrounds can be removed from analysis data sets.

In experiments which can identify event topologies, analysis techniques are used to greatly reduce the backgrounds, namely the selection of events with two electrons. Another technique which does not require the full reconstruction of event topologies is to use positional information to define fiducial regions in the innermost parts of the detector. Events of external origin will induce detector activity outside these regions, and therefore can be rejected. Analysis of pulse shapes in semiconductor experiments allows for discrimination between single-site and multiple-site interactions, which is used to reject $\gamma$ backgrounds.

\section{Detector technologies}
\label{sec:dbd-det-tech}
A number of different detector technologies have been used to address the points made above. In general, there is a trade-off between attaining a large mass of source isotope and achieving a very low level of background. An overview of the current and future double-$\beta$ decay experiments is given here. Experiments which use \Ca{48} as the double-$\beta$ decay source are discussed in Section \ref{sec:dbd-ca-48-exp}.

\subsection{Semiconductor experiments}
Semiconductor experiments exploit the existence of suitable materials which are also double-$\beta$ decay isotopes. Detectors are built by placing a semiconductor enriched in a double-$\beta$ decay isotope between two electrodes. Ionising radiation creates electron-hole pairs which drift towards the electrodes, producing a current on them. The signals on the electrodes are proportional to the energy deposited in the detector. These detectors operate at cryogenic temperatures, achieving very good energy resolution, as low as $\sim$ 0.3\% FWHM.

This technique has been particularly successful when used with \Ge{76}, having provided some of the most stringent limits on the effective Majorana mass to date. High-purity germanium detectors (HPGe) are used in nuclear spectroscopy for which there is a well established manufacturing industry. In addition, affordable enrichment methods exist for \Ge{76}. 

The Heidelberg-Moscow (H-M) experiment ran from 1990 to 2003 with five HPGe detectors enriched in \Ge{76} to 86.6\%. The detector operated in the Laboratori Nazionali del Gran Sasso (LNGS), and with a total exposure of 35.5 kg yr obtained a limit on the \0nu of \Ge{76} of $t^{1/2}_{0\nu} > $ 1.9 $\times$ 10$^{25}$ yr, corresponding to \mbox{$\braket{m_{\beta\beta}} < 0.25 - 0.50$ eV}\cite{KlapdorKleingrothaus:2000sn}. A similar experiment, IGEX, ran concurrently with the H-M experiment, with six HPGe detectors and a plastic scintillator veto. With an exposure of 8.9 kg yr the experiment obtained a slightly weaker \0nu half-life limit of $t^{1/2}_{0\nu} > $ 1.57 $\times$ 10$^{25}$ yr, giving \mbox{$\braket{m_{\beta\beta}} < 0.28 - 0.55$ eV}\cite{igex}.

The current generation of \Ge{76} experiments is being led by GERDA. In its first phase, this experiment consists of eight HPGe detectors recovered from the H-M and IGEX experiments and newly commissioned broad energy germanium detectors (BEGe) immersed in liquid argon, which provides both cooling and shielding. The liquid argon volume is in turn surrounded by a water \v{C}erenkov active shield. After two years of operation with 21.3 kg of source mass, a limit on the half-life of the \0nu of \Ge{76} was obtained: $t^{1/2}_{0\nu} > $ 2.1 $\times$ 10$^{25}$ yr, corresponding to $\braket{m_{\beta\beta}} < 0.24 - 0.48$ eV\cite{Agostini:2012nm}. This limit is an improvement on the H-M result, despite the lower exposure. The second phase of the experiment will include an additional 20 kg of source mass and is expected to be sensitive to an effective Majorana mass of 50 -- 100 meV\cite{Knöpfle201331}.

MAJORANA is a \Ge{76} experiment which is expected to start taking data soon. It follows the strategy of the H-M experiment, but with enhanced shielding. With a source mass of 40 kg it is expected to achieve a sensitivity to $\braket{m_{\beta\beta}}$ of 80 -- 160 meV in 2.5 years of operation\cite{Abgrall:2013rze}.

A future collaboration between GERDA and MAJORANA is envisaged, with the aim of building a 1 tonne \Ge{76} experiment, which would be sensitive to an effective Majorana mass as low as 10 meV\cite{BarnabéHeider2012141}.

COBRA is an experiment currently in the R\&D phase which will employ a semiconductor approach with CdZnTe crystals enriched in \Cd{116}. The experiment will consist of a segmented detector, for background reduction, operated at room temperature\cite{Wilson2011313}. 

\subsection{Bolometer experiments}
Bolometer experiments measure small changes in the temperature of crystals due to energy deposits from ionising radiation. These detectors are operated at very low temperatures (10 mK) to achieve a very good energy resolution. CUORICINO consisted of 62 TeO$_2$ crystals passively shielded in a cryostat. With an exposure of 19.75 kg yr a half-life limit on the \0nu of \Te{130} was obtained: \mbox{$t^{1/2}_{0\nu} > $ 2.8 $\times$ 10$^{24}$ yr}, corresponding to $\braket{m_{\beta\beta}} < 0.3 - 0.7$ eV\cite{Andreotti:2010vj}. This technology has been further developed for the CUORE experiment, which is planned to consist of 1000 crystals hosting 200 kg of \Te{130}. A prototype of this detector, CUORE-0, is currently running with 52 crystals, having an expected sensitivity to a \0nu half-life of 8 $\times$ 10$^{24}$ yr, corresponding to a mass of 0.18 -- 0.42 eV. It is expected that the full detector will be sensitive to a half-life of 2.1 $\times$ 10$^{26}$ yr, corresponding to an effective Majorana mass of 35 -- 82 meV\cite{1742-6596-447-1-012066}. 

\subsection{Scintillator experiments}
Being economical and radiopure, scintillating materials are well suited for double-$\beta$ decay experiments. The scintillating properties of these materials are exploited by embedding double-$\beta$ decay isotopes in them and surrounding them with photomultiplier tubes (PMTs) to capture the light produced by the electrons emitted in the decay.

The most stringent limit on the effective Majorana mass has been obtained by KamLAND-Zen, a liquid scintillator \Xe{136} experiment. The detector consists of a nylon balloon filled with 13 tonnes of Xe-loaded liquid scintillator placed inside another balloon, this one filled with 1 kilotonne of Xe-free liquid scintillator. The balloons are surrounded by 2000 PMTs which detect the scintillation light. This apparatus is in turn surrounded by a water \v{C}erenkov detector which acts as a cosmic ray veto. A limit on the \0nu half-life of \Xe{136} of $t^{1/2}_{0\nu} > $ 1.9 $\times$ 10$^{25}$ yr, corresponding to $\braket{m_{\beta\beta}} < 0.16 - 0.33$, was obtained with an exposure of 89.5 kg yr\cite{kamland-zen}. The collaboration is currently involved in R\&D activities to reduce the background levels and increase the mass of \Xe{136} in the detector by a factor of two. There are plans for a second generation of the experiment which, with 1 tonne of \Xe{136}, would be sensitive to a Majorana mass of 20 meV\cite{Shirai201328}.

SNO+ is another experiment, currently under construction, which will use isotope-loaded liquid scintillator techniques to investigate double-$\beta$ decay processes. An acrylic sphere will be filled with liquid scintillator loaded with 800 kg of \Te{130}. This sphere will be immersed in water for shielding and surrounded by 9500 PMTs. A sensitivity to a mass of 50 -- 100 meV is expected in the first phase of the experiment, with an increase in the mass of \Te{130} by one order of magnitude planned which is expected to improve the sensitivity to 20 -- 40 meV\cite{Wilson:2013iop}. One challenge with this approach is the loss in efficiency due to the attenuation of scintillation light under a high \Te{130}-loading fraction of 3\%.

\subsection{Time projection chamber experiments}
In time projection chamber (TPC) experiments, the position and energy of ionising particles are measured simultaneously. A high potential difference is set across two or more electrodes in the gaseous or liquid medium. As the medium is ionised, the freed electrons drift towards the anodes. When the electrons reach the anode, a current is generated across the electrodes which is proportional to the energy initially deposited in the medium. By arranging the electrodes in sets of orthogonal wires, a two-dimensional reconstruction of the position of the ionisation is possible. The third coordinate is given by the time taken by the ionisation electrons to reach the anode. This technique is commonly coupled with scintillation methods to improve the energy resolution and provide particle identification.

EXO-200 is a \Xe{136} experiment which uses a TPC technique. The experiment consists of a cylindrical chamber filled with liquid Xe enriched in \Xe{136} with the cathode placed in the middle. Both charge and scintillation light are measured, the latter with arrays of avalanche photodiodes on both sides of the chamber. The fiducial volume analysed contains around 80 kg of \Xe{136}. With an exposure of \mbox{32.5 kg yr} a limit on the \0nu half-life of \Xe{136} was set at $t^{1/2}_{0\nu} > $ 1.6 $\times$ 10$^{25}$ yr which corresponds to $\braket{m_{\beta\beta}} < 0.17 - 0.36$\cite{Albert:2013gpz}. An increase of the source mass to 1 tonne is planned, as well as the deployment of a new technique in which the identification of barium ions resulting from double-$\beta$ decays of \Xe{136} will be used to greatly reduce the background levels\cite{exo-ton}.

Another experiment using TPC techniques is NEXT, currently in the R\&D phase. Unlike EXO, this experiment will use gaseous Xe under pressure as the TPC medium. The chamber will have the TPC anodes on one side and PMTs on the other. The PMTs will be used to detect two signals: prompt scintillation signals, and electroluminescence produced as the drifting electrons cross the anode plane. With 100 kg of Xe, the experiment is expected to be sensitive to a 1.9 $\times$ 10$^{25}$ yr \0nu half-life, or a Majorana mass of 70 -- 150 meV\cite{Gomez-Cadenas:2013lta}.

\subsection{Tracker and calorimeter experiments}
While tracker and calorimeter experiments have modest energy resolution, this is overcome by the very large reduction of the backgrounds obtained by selecting events with a two-track topology. The full reconstruction of the event topology also allows for independent analysis channels to be used for the precise measurement of background activities. Additionally, in the event of the discovery of \0nu, tracker and calorimeter detectors have the unique advantage of being able to measure the decay kinematics, providing a deeper investigation of the mechanism responsible for \0nu. One advantage of having detector components which are independent from the source is that many different isotopes can be studied.

The notable tracker and calorimeter experiments are NEMO-3 and SuperNEMO. These experiments are described in detail in Chapters \ref{sec:nemo-3-det} and \ref{sec:sn-det}, respectively. NEMO-3 has produced the most precise measurement of the \2nu half-life of seven of the nine isotopes for which \2nu has been directly observed (Table \ref{tab:2nu-best-meas}). The best \0nu limit obtained with NEMO-3 was for the \Mo{100} isotope, with $t^{1/2}_{0\nu} > $ 1.1 $\times$ 10$^{24}$ yr, corresponding to a mass limit of 0.3 -- 0.9 eV\cite{Arnold:2013dha}. SuperNEMO will use the technique of NEMO-3, with an increase in the source mass by one order of magnitude and improved experimental characteristics, and is expected to be sensitive to a Majorana mass of 50 -- 100 meV.

\section{Experiments with \Ca{48}}
\label{sec:dbd-ca-48-exp}
A number of techniques have been employed in the search for the \0nu of \Ca{48}. An overview of the experiments performed with this isotope is given here, in historical order.

\subsubsection{Mateosian-Goldhaber experiment}
The Mateosian-Goldhaber experiment was the first experiment to search for lepton number violating processes in \Ca{48}. The experiment was performed in 1966, with a live time of only 28.7 days. A large \CaF2 crystal scintillator, enriched in \Ca{48} was housed inside a steel naval gun, with additional active shielding provided by plastic scintillators surrounding the crystal. No evidence for either \2nu or \0nu was found, with half-life limits of $t^{1/2}_{2\nu} > $ 5 $\times$ 10$^{18}$ yr and $t^{1/2}_{0\nu} > $ 2 $\times$ 10$^{20}$ yr being obtained\cite{PhysRev.146.810}.

\subsubsection{Beijing experiment}
Another \Ca{48} experiment was performed in a coal mine near Beijing. It employed the technique of Mateosian and Goldhaber with a final setup of four \CaF2 crystals fully surrounded by plastic scintillator active shielding and encased in lead. The volume defined by the lead shielding was ventilated with argon. With a live time of 0.87 years a limit of $t^{1/2}_{0\nu} > $ 9.5 $\times$ 10$^{21}$ (76\% CL) on the \0nu half-life was obtained\cite{You199153}. 

\subsubsection{Hoover Dam experiment}
The Hoover Dam experiment achieved the first observation of the \2nu of \Ca{48}. The experiment consisted of thin sheets of CaCO$_3$ at 18 mg$/$cm$^2$ enriched in \Ca{48} acting as the central cathode in a TPC. The tracking capabilities of the detector allowed for sophisticated analysis techniques to be employed: helices were fit to electron tracks and delayed $\alpha$ particles were used to reject background events due to \Bi{214}. With a total mass of 42.2 g of enriched CaCO$_3$ the \2nu half-life was measured to be \mbox{4.3 $^{+2.4}_{-1.1}$ (stat.) $\pm 1.4$ (syst.) $\times$ 10$^{19}$ yr}\cite{PhysRevLett.77.5186}.

\subsubsection{TGV}
The Telescope Germanium Vertical (TGV) consisted of a tower of 16 HPGe detectors of the planar type housed in a single cryostat. Thin sheets of CaCO$_3$, enriched in \Ca{48}, were placed on top of each detector, with a total \Ca{48} mass of 1 g. The experiment was performed at the Laboratoire Souterrain de Modane (LSM), with a live time of 1 year. The \2nu half-life was measured to be 4.2 $^{+3.3}_{-1.3}$ $\times$ 10$^{19}$ yr, in agreement with the Hoover Dam experiment. In addition, a limit on the \0nu half-life was obtained: $t^{1/2}_{0\nu} > $ 1.5 $\times$ 10$^{21}$ yr\cite{Brudanin200063}.

\subsubsection{ELEGANT VI}
The ELEGANT VI experiment has obtained the most stringent limit on the \0nu of \Ca{48} to date. The experiment consisted of 23 CaF$_2$(Eu) crystals produced with natural calcium, giving a total of 7.6 g of \Ca{48}. Additional CaF$_2$ (pure) and CsI(Tl) crystals were used for rejecting events of external origin. With a live time of 0.6 years, a limit was obtained on the \0nu half-life of 5.8 $\times$ 10$^{22}$ yr, corresponding to an upper limit on the effective Majorana mass of $\braket{m_{\beta\beta}} < 3.5 - 22$ eV\cite{PhysRevC.78.058501}.

\subsubsection{CANDLES}
CANDLES is the successor of ELEGANT VI. The experiment, which is currently running, consists of 60 cubic \CaF2 scintillators (with natural Ca) immersed in liquid scintillator. The scintillation light is detected by 40 large PMTs which surround the scintillator. To veto external events, the different time constants of the two types of scintillator are used. While interactions in the liquid scintillator produce short light pulses, events originating in the \CaF2 produce light pulses which last longer. By integrating the PMT signals over two time windows, one short and one long, and taking their ratio, interactions in the liquid scintillator can be distinguished from those in the \CaF2 crystals. The experiment is expected to be sensitive to a half-life of 10$^{26}$ yr.

\section{Current status of double-$\beta$ decay measurements}
\label{sec:dbd-results-summary}
The best double-$\beta$ decay measurements currently available are summarised here. The most precise measurements of the \2nu half-life for the isotopes for which this process has been directly observed are listed in Table \ref{tab:2nu-best-meas}. The best limits on the \0nu modes of the same isotopes are given in Table \ref{tab:0nu-best-meas}.

\begin{table}[htpb]
  \begin{tabular}{c c l}
    Isotope     &  $t^{1/2}_{2\nu}$ (10$^{19}$ yr)                       & Experiment \\ \hline
    \Ca{48}     &  6.4 $^{+0.6}_{-0.7}$ (stat.) $^{+1.2}_{-0.9}$ (syst.)  & NEMO-3 (this work) \\
    {\color{gray}    \Ca{48} }    & {\color{gray} 4.2 $^{+3.3}_{-1.3}$ } & {\color{gray} TGV}\cite{Brudanin200063} \\
    {\color{gray}    \Ca{48} }    & {\color{gray} 4.3 $^{+2.4}_{-1.1}$ (stat.) $\pm 1.4$ (syst.) }  & {\color{gray} Hoover Dam}\cite{PhysRevLett.77.5186} \\
    \Ge{76}     &  184 $^{+9}_{-8}$ (stat.) $^{+11}_{-6}$ (syst.)         & GERDA\cite{Agostini:2012nm} \\
    \Zr{96}     &  2.35 $\pm$ 0.14 (stat.) $\pm$ 0.16 (syst.)             & NEMO-3\cite{Argyriades:2009ph}\\
    \Se{82}     &  9.6 $\pm$ 0.3 (stat.) $\pm$ 1.0 (syst.)                & NEMO-3\cite{Arnold:2005rz} \\
    \Mo{100}    &  0.711 $\pm$ 0.002 (stat.) $\pm$ 0.054 (syst.)          & NEMO-3\cite{Arnold:2005rz} \\
    \Cd{116}    &  2.88 $\pm$ 0.04 (stat.) $\pm$ 0.16 (syst.)             & NEMO-3\cite{1742-6596-375-4-042011} \\
    \Te{130}    &  70 $\pm$ 9 (stat.) $\pm$ 11 (syst.)                   & NEMO-3\cite{Arnold:2011gq} \\
    \Xe{136}    &  216.5 $\pm$ 1.6 (stat.) $\pm$ 5.9 (syst.)             & EXO-200\cite{Albert:2013gpz} \\ 
    \Nd{150}    &  0.911 $^{+0.025}_{-0.022}$ (stat.) $\pm$ 0.63 (syst.) & NEMO-3\cite{Argyriades:2008pr} \\
  \end{tabular}
  \caption[Most precise direct measurements of \2nu.]{Most precise direct measurements of \2nu. Additionally, two superseded results for \Ca{48} are shown in grey.}
  \label{tab:2nu-best-meas}
\end{table}

%\subsection{Current limits on \0nu}

\begin{table}[htpb]
  \begin{tabular}{c c c c l}
    Isotope     &  $t^{1/2}_{0\nu}$ limit (yr)  & $\braket{m_{\beta\beta}}$ limit (eV)  & Exposure (kg yr)       & Experiment \\ \hline
     \Ca{48}    &  5.8 $\times$ 10$^{22}$       & 3.5 -- 22                             & 0.015                  & ELEGANT VI\cite{PhysRevC.78.058501} \\
     \Ge{76}    &  2.1 $\times$ 10$^{25}$       & 0.24 -- 0.48                          & 16.4                   & GERDA\cite{Agostini:2013mzu} \\
     \Se{82}    &  2.1 $\times$ 10$^{23}$       & 1.0 -- 2.8                            & 4.9                    & NEMO-3\cite{motty}\\
     \Mo{100}   &  1.1 $\times$ 10$^{24}$       & 0.3 -- 0.9                            & 34.7                   & NEMO-3\cite{Arnold:2013dha}\\ 
     \Zr{96}    &  9.2 $\times$ 10$^{21}$       & 7.2 -- 19.5                           & 0.031                  & NEMO-3\cite{Argyriades:2009ph}\\
     \Cd{116}   &  1.7 $\times$ 10$^{21}$       & 1.4 -- 2.8                            & 0.14                   & Solotvina\cite{PhysRevC.68.035501}\\
     \Te{130}   &  2.8 $\times$ 10$^{24}$       & 0.3 -- 0.7                            & 19.75                  & CUORICINO\cite{Andreotti:2010vj} \\
     \Xe{136}   &  1.9 $\times$ 10$^{25}$       & 0.16 -- 0.33                          & 89.5                   & KamLAND-Zen\cite{kamland-zen}\\
     \Nd{150}   &  1.8 $\times$ 10$^{22}$       & 4.0 -- 6.3                            & 0.093                  & NEMO-3\cite{Argyriades:2008pr}\\
   \end{tabular}
  \caption[Best limits on the \0nu half-life of the isotopes for which \2nu has been directly observed.]{Best limits on the \0nu half-life of the isotopes for which \2nu has been directly observed. All limits are at 90\% CL.}
  \label{tab:0nu-best-meas}
\end{table}



