\chapter{Conclusion}
\label{chap:conclusion}
Neutrinoless double-$\beta$ decay is a lepton number violating process forbidden in the SM. The observation of this process would establish the Majorana nature of the neutrino. In the standard scenario, the decay proceeds through the exchange of light neutrinos. Other decay modes are predicted by theories beyond the SM, namely $\slashed{R}$ supersymmetry, models with left-right symmetry and those which predict the existence of Majorons.

The double-$\beta$ decay of \Ca{48} was investigated in data taken with the NEMO-3 detector over a period of eight years, from 2003 to 2011, with an accumulated live time of 5.2 years.

The \2nu half-life of \Ca{48} was measured to be:
\begin{equation}
  t^{1/2}_{2\nu} =  6.4 \,  ^{+0.6}_{-0.7} \,  (stat.) \,  _{-0.9}^{+1.2}\,  (syst.) \, \times 10^{19} \, \text{yr} \, ,
\end{equation}
\noindent being the most precise measurement to date. This measurement is in agreement with previous measurements by the Hoover Dam and TGV experiments, although the uncertainties on these results are significantly larger.

From this result, the nuclear matrix element for the decay can be extracted:
\begin{equation}
  \left| M^{2\nu} \right| = 0.020 \pm 0.002 \, .
\end{equation}
The value measured here is smaller than the nuclear theory predictions. It should be noted, however, that different models assumed in these calculations result in a spread of predictions which is wider than the discrepancy with the value reported here.

No evidence was found in the data for the neutrinoless modes of double-$\beta$ decay.
A limit on the half-life of the mass mechanism \0nu was set:
\begin{equation}
  t^{1/2}_{0\nu} \,  > \, 2.0 \times 10^{22} \, \text{yr} \, (90\% \, \text{CL}) \,  ,
  \label{eq:0nu_res_conc}
\end{equation}
\noindent which does not improve the limit previously set by the ELEGANT IV experiment. From this result, a limit on the effective Majorana mass of the neutrino can be extracted:
\begin{equation}
  \braket{m_{\beta\beta}} < 6.1 - 38 \eV \, (90\% \, \text{CL}) \,  ,
\end{equation}
\noindent where the range reflects the disparity in nuclear matrix element calculations. While this limit is not competitive with the current best limits obtained by the KamLAND-Zen and EXO-200 \Xe{136} experiments, it should be noted that due to the small mass (6.99 g) of \Ca{48} present in the NEMO-3 detector, the exposure with which the result presented here was obtained is three orders of magnitude smaller than the combined exposure of the \Xe{136} experiments. Given the very low level of background expected in the region of interest of \Ca{48} in the NEMO-3 experiment, a simple extrapolation indicates that, in principle, a large exposure \Ca{48} experiment would provide results competitive with current state-of-the-art experiments. The main challenge to this would be due to the very small natural abundance of \Ca{48} and difficulties associated with its enrichment.

The half-life limit of Equation \ref{eq:0nu_res_conc} also allows for the extraction of a limit on the $\slashed{R}$ supersymmetry trilinear coupling $\lambda_{111}'$:
\begin{equation}
  \lambda_{111}' < 0.11 - 0.14 \times f  \, (90\% \, \text{CL}) \, ,
\end{equation}
\noindent where $f$ is given by
\begin{equation}
  f = \left(\frac{m_{\tilde{q}}}{1\TeV}\right)^2 \left(\frac{m_{\tilde{g}}}{1\TeV}\right)^{1/2} \, .
\end{equation}

Left-right symmetric models predict \0nu, either through a small admixture of a right-handed component in the SM $W$ or via the existence of an as yet undiscovered heavy, right-handed $W'$, with such decays being governed by the couplings $\braket{\lambda}$ and $\braket{\eta}$, respectively. The limits on these decay modes of \Ca{48} were found to be:
\begin{equation}
  t^{1/2}_{0\nu\lambda} \,  > \, 1.2 \times 10^{22} \, \text{yr} \, (90\% \, \text{CL}) \, ,
  \label{eq:0nu_lambda_res_conc}
\end{equation}
\begin{equation}
  t^{1/2}_{0\nu\eta} \,  > \, 1.9 \times 10^{22} \, \text{yr} \, (90\% \, \text{CL}) \, .
  \label{eq:0nu_lambda_res_conc}
\end{equation}
The constraints on the couplings derived from the limits above are:
\begin{equation}
  \braket{\lambda} < 7.7 - 48 \times 10^{-6} \, (90\% \, \text{CL}) \, ,
\end{equation}
\begin{equation}
  \braket{\eta} < 0.76 - 55 \times 10^{-7} \, (90\% \, \text{CL}) \, .
\end{equation}

Lastly, the neutrinoless decay mode with the emission of a single Majoron with spectral index $n=1$ was investigated. The broader summed electron energy spectrum of this decay results in a weaker limit compared to the ones given above:
\begin{equation}
  t^{1/2}_{0\nu\chi^0} > 4.6 \times 10^{20} \text{yr}  \, (90\% \, \text{CL}) \, ,
\end{equation}
\noindent with the coupling of the Majoron to electrons being constrained to be:
\begin{equation}
  \braket{g_{\chi^0}} < 0.44 - 3.1 \times 10^{-3} \, (90\% \, \text{CL}) \, .
\end{equation}

SuperNEMO is a next-generation double-$\beta$ decay experiment. It is based on the same detection technique as NEMO-3, with both a tracking detector and a calorimeter being used to uniquely identify electrons and measure their individual energies. The first stage of this experiment, the Demonstrator module, is currently in an advanced stage of construction. Its tracker is being assembled and will be commissioned at surface with cosmic rays.

The data acquisition and high voltage supply systems of the NEMO-3 detector were recovered at the end of running to be used in the surface commissioning of the Demonstrator's tracker. The tracker will be commissioned in parts, with each quarter of the chamber being commissioned in turn. The NEMO-3 electronics were adapted to instrument 504 SuperNEMO cells at a time, with parts of the system, including the high-voltage supply, VME controller and data acquisition computer being newly acquired. A bespoke board hosting an FPGA was developed to provide the set of trigger signals required by the NEMO-3 data acquisition cards. A cosmic ray trigger was developed, based on the coincident signals on two 1 m$^2$ scintillator planes, to be placed above and below the tracker C-sections.

The system was shown to perform well in tests with a $2\times9$ array of SuperNEMO production tracker cells.

A gas mixing system to supply the SuperNEMO gas mix of He:Ar:ethanol at 95:1:4 \% fractions by volume to the Demonstrator's tracker was designed and built. The system is based on the one used in NEMO-3, with a number of parts being reused. The helium and argon are supplied separately and their flow rates are individually controlled to achieve the required mix. Ethanol vapour is added by passing the helium through two bubblers: one under controlled pressure and the other under controlled temperature. The conditions set in the bubblers dictate the partial pressure of the evaporated ethanol, which is tuned to achieve the required fraction in the final gas mix.

One important concern in the construction of the gas system was to reduce as much as possible the amount of \Rn{222} emanating or diffusing into the tracking gas. To this end, the system was entirely built in stainless steel (known for its low \Rn{222} emanation) and all sealing materials were selected based on previous evidence for low \Rn{222} emanation and permeation. The system is capable of sustaining flow rates much higher than what was used in NEMO-3, as high flow rates are a powerful tool for reducing the \Rn{222} activity inside the detector volume.

The system was successfully tested with a single-cell tracker prototype and the amount of ethanol being introduced into the gas was verified by measuring the ethanol depletion rate under a fast flow of gas over a few days.





