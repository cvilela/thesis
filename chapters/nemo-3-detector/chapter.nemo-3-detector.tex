\chapter{The NEMO-3 detector}
\label{sec:nemo-3-det}
The general detection principle of the NEMO-3 experiment is to track the two electrons produced in double-$\beta$ decays and measure their energies separately. This allows for a very pure selection of double-$\beta$ decay events by rejecting backgrounds with different event topologies.

The NEMO-3 detector operated in the Laboratoire Souterrain de Modane (LSM) from February 2003 to January 2011. The experiment housed seven double-$\beta$ decay isotopes in a thin foil surrounded by tracker and calorimeter modules in a cylindrical geometry (Figure \ref{fig:detector}). 

The most stringent NEMO-3 limit on \0nu was obtained with \Mo{100} data\cite{Arnold:2013dha}. The \0nu of this isotope was excluded up to a half-life of 1.1$\times 10^{24}$ years at 90\% CL, corresponding to an upper limit on the effective Majorana mass of 0.33 -- 0.87\eV.

An overview of the detector's components and systems is given in this chapter, written in reference to the experiment's technical design report\cite{Arnold:2004xq}.

\begin{figure}[htpb]
  \def\svgwidth{\largefigwidth}
  \import{chapters/nemo-3-detector/}{NEMO3Exploded.pdf_tex}
  \caption[Cutaway drawing of the NEMO-3 detector.]{Cutaway drawing of the NEMO-3 detector. The components described in this chapter are indicated by black arrows.}
  \label{fig:detector}
\end{figure}

\section{Source foils}
The NEMO-3 detector was designed to accommodate 10 kg of double-$\beta$ decay sources. The sources were shaped into thin foils arranged in a cylindrical geometry inside the detector, which was itself composed of 20 sectors. The position of the source cylinder in the detector and the division into sectors are shown in Figure \ref{fig:NemoTopViewDiag}.

\begin{figure}[htpb]
  \def\svgwidth{\largefigwidth}
  \import{chapters/nemo-3-detector/}{PlanView.pdf_tex}
  \caption[Top-view diagram of the NEMO-3 detector.]{Top-view diagram of the NEMO-3 detector. The main detector components are shown as is the arrangement into sectors.}
  \label{fig:NemoTopViewDiag}
\end{figure}

The surface density of the foils was optimised such that the amount of source isotope would be maximised while keeping energy losses of electrons in the foil within a level which did not compromise the sensitivity of the detector to double-$\beta$ decay processes. The resulting densities of most foils were between 30 and 60 mg/cm$^2$ which translates into a thickness of 60 to 300\um, depending on the foil construction.

Most of the detector was occupied by the \Mo{100} (6.9 kg) and \Se{82} (0.93 kg) sources. These isotopes were chosen to be the primary sources for \0nu searches. In addition to these two isotopes, five other source isotopes were selected, mainly for use in \2nu studies. The experiment included 0.61 kg of \Te{nat}, 0.45 kg of \Te{130}, \mbox{0.40 kg} of \Cd{116}, 36.5 g of \Nd{150}, 9.43 g of \Zr{96} and 6.99 g of \Ca{48}. Additionally, \mbox{0.62 kg} of very pure Cu was included which was used (with the \Te{nat} source) to measure the detector's backgrounds. The disposition of the different sources in the detector is shown in Figure \ref{fig:camembert}.

\begin{figure}[htpb]
  \def\svgwidth{\largefigwidth}
  \import{chapters/nemo-3-detector/}{Camembert.pdf_tex}
  \caption[Arrangement of the different sources in the NEMO-3 detector.]{Arrangement of the different sources in the NEMO-3 detector. The sector numbering scheme is shown.}
  \label{fig:camembert}
\end{figure}

In each sector the source foil is composed of seven 2.5 m $\times$ 6.5 cm strips. In sector 5, strips 1 to 5 are populated with \Mo{100}, strip 6 contains the \Nd{150} source, and the last strip consists of the \Zr{96} source on the upper half and the \Ca{48} source below it (Figure \ref{fig:source_in_data}).

\begin{figure}[htpb]
  \begin{tikzpicture}
    \node[anchor=south west,inner sep=0] (image) at (0,0) {
      \def\svgwidth{\smallfigwidth}
      \import{chapters/nemo-3-detector/}{sourcePhoto.pdf_tex}
    };

    \begin{scope}[x={(image.south east)},y={(image.north west)}]
      \draw[thick, rounded corners] (0.12, 0.83) -- (0.23,0.83) -- (0.23, 0.17) -- (0.12, 0.17) ;
      \draw[thick, -> ] (0., 0.55)  node [left] {\Mo{100}} to [out = 0, in = 120] (0.18,0.5);

      \draw[thick, rounded corners] (0.9, 0.83) -- (0.75,0.83) -- (0.75, 0.17) node[midway, right] {\Se{82}} -- (0.9, 0.17) ;
      \draw[thick, rounded corners] (0.45,0.5) rectangle (0.65,0.83) node [midway] {\Zr{96}};
      \draw[thick, rounded corners] (0.24,0.17) rectangle (0.44,0.83) node [midway] {\Nd{150}};
      \draw[thick, rounded corners] (0.45,0.333) rectangle (0.65,0.49) node [midway] {\Ca{48}};
    \end{scope}

  \end{tikzpicture}
  \caption[Map of the \Ca{48} and neighbouring sources as seen in NEMO-3 data.]{Map of the \Ca{48} and neighbouring sources as seen in NEMO-3 data. The linear colour scale indicates the density of electron track intersections with the source foil plane, with red being the densest and violet the least dense. The different sources are outlined in black. Above and below the source strips their support structure can be seen. The location of the calibration source tube (Section \ref{sec:det_rad_calib}) is seen between the \Ca{48}/\Zr{96} and the \Se{82} sources.}
  \label{fig:source_in_data}
\end{figure}

 
\subsection{\Ca{48} source}
\label{sec:det_ca_source}
The \Ca{48} source was produced from a CaCO$_\text{3}$ sample which was enriched in \Ca{48} to a fraction of 73.2 $\pm$ 1.6 \% by electromagnetic isotope separation methods. The CaCO$_\text{3}$ was purified to remove \Ra{226}, \Ra{228}, \Co{60}, \Eu{152} and other elements from the thorium and radium decay chains. It was then processed into \CaF2 powder, \mbox{17.5 g} of which were moulded into nine 4.6 cm diameter discs\footnote{The diameter of the discs is erroneously given as 4.0 cm in \cite{Arnold:2004xq}. The correct dimensions were confirmed when the source was extracted from the detector at the end of the experiment\cite{victorDia1, victorDia2}.}. 

For the production of the source discs the \CaF2 powder was sandwiched between 10\um thick discs of Mylar and heat resistant Teflon. It is not known if the Teflon layer was removed after pressing. The discs were then placed between two long Mylar strips, with bands of glue between them for mechanical support. The Mylar strips were 19\um thick and were perforated by exposure to a \Kr{84} ion beam to enhance their adhesion properties. The thickness of the pressed \CaF2 discs is not well known; it is estimated to be 0.7 mm. The mass of glue used in the produtction of the strip is not well known either, being estimated to be \mbox{0.7 g}. A sketch of the structure of the \CaF2 discs is shown in Figure \ref{fig:disc_sketch}, with the materials shown adding up to a surface density of 117 mg/cm$^2$. A photograph of the \Ca{48} source taken after it was removed from the detector can be seen in Figure \ref{fig:source_photo}.

\begin{figure}[htpb]
  \input{../Tikz/source3d.tex}
  \caption[Structure of the \CaF2 source discs.]{Structure of the \CaF2 source discs. The \CaF2 is shown in red, sandwiched between two layers of Mylar (green) and two layers of Teflon (blue). The discs are supported between two long Mylar strips (yellow) by bands of glue (not shown). Dimensions are not to scale.}
  \label{fig:disc_sketch}
\end{figure}

\begin{figure}[htpb]
  \includegraphics[width=\textwidth]{chapters/nemo-3-detector/sourcePhoto_real.pdf}
  \caption[Photograph of the \Ca{48} source.]{Photograph of the \Ca{48} source. Taken after the source was removed from the detector at the end of the experiment. The nine \CaF2 discs are seen as is the glue supporting them.}
  \label{fig:source_photo}
\end{figure}

\subsubsection{Rotation of the \Ca{48} source from the nominal position}
It was observed in NEMO-3 data that the \Ca{48} strip was rotated off its nominal position by 10.6 $\pm$ 0.6 $^\circ$. This measurement was obtained with two independent methods. The occupancy of the calorimeter modules near the \Ca{48} source was found to be differently distributed for data and Monte Carlo for events where one electron was identified and its originating vertex reconstructed to the \Ca{48} source. It was found that this discrepancy could be eliminated if the description of the source strip in the simulation was rotated. Another, data driven, method that returned the same result was the analysis of events where two electrons were reconstructed. The distribution of the intersection points of the two electron tracks was not in good agreement with the nominal position of the foil, favouring instead the same rotation angle as the previously described method (Figure \ref{fig:victor_rotation}).

While the detector was being disassembled several strips were found to have moved from their nominal position. An example of a rotated pure copper strip in sector 0 is shown in Figure \ref{fig:cu_rot}.

\begin{figure}[htpb]
        \centering
        \subfloat[]{
%          \begin{tikzpicture}[scale=0.5]
%            \begin{scope}[canvas is xy plane at z=0]
%              \draw[solid,white] (0,0) to (0,-5);
%
%              \draw[solid, ->] (-4,0) to (-4,2) node [above] {$y$};
%              \draw[solid, ->] (-4,0) to node [pos=0.8,below] {{\footnotesize Nominal plane}}(8,0) node [right] {$x$};
%              \begin{scope}[rotate=10.6]
%                \draw[dashed] (-4,0) to node [pos=0.9,rotate=10.6,above] {{\footnotesize Rotated plane}}(7,0)  ;
%                
%                \filldraw[fill=red, draw=red, fill opacity=0.8]     (-2.3,-0.2) rectangle (2.3,0.2);
%                
%                \filldraw[fill=green, draw=green, fill opacity=0.8] (-2.3, 0.2) rectangle (2.3,0.3);
%                \filldraw[fill=green, draw=green, fill opacity=0.8] (-2.3, -0.2) rectangle (2.3,-0.3);
%                
%                \filldraw[fill=blue, draw=blue, fill opacity=0.8] (-2.3, 0.3) rectangle (2.3,0.4);
%                \filldraw[fill=blue, draw=blue, fill opacity=0.8] (-2.3, -0.3) rectangle (2.3,-0.4);
%       
%                \filldraw[fill=yellow, draw=yellow, fill opacity=0.8] (-3, 0.4) rectangle (3,0.5);
%                \filldraw[fill=yellow, draw=yellow, fill opacity=0.8] (-3, -0.4) rectangle (3,-0.5);
%              \end{scope}
%              \draw[solid] (3.8,0) arc[radius = 3.8, start angle=0, end angle=5.3] node[right, rotate=5.3] {\footnotesize $\phi=10.6^\circ$} arc[radius = 3.8, start angle=5.3, delta angle=5.3];
%
%            \end{scope}
%          \end{tikzpicture}
%          \label{fig:rotation_diag}

          \includegraphics[width=\smallfigwidth]{chapters/nemo-3-detector/victor_rotation.pdf}
          \label{fig:victor_rotation}
        }
        \hfill
        \subfloat[]{
          \includegraphics[width=\smallfigwidth]{chapters/nemo-3-detector/cuRot.pdf}
          \label{fig:cu_rot}
        }
        \caption[Rotated source foil strips.]{Rotated source foil strips. A rotation of the \Ca{48} source strip from its nominal position as seen in the analysis of the intersection points of two-track events is shown in (a)\cite{victor_rotation}. The coloured lines represent the nominal position of the strips and the size of the black squares indicates the density of two-track intersections. A photograph of a rotated Cu strip taken during the disassembly of the detector is shown in (b).}
\end{figure}

\section{Tracker}
The NEMO-3 tracking detector was a wire chamber consisting of 6180 cells strung vertically on both sides of the source foil. The cells were arranged in 18 concentric layers, nine on either side of the source foil. These layers were combined into three groups on each side of the foil with four layers immediately adjacent to the source foil, two layers in the middle and three layers next to the main calorimeter walls. Four concentric rings of PMTs were placed on the top and bottom endcaps (petals), between the groups of tracker cells (Figure \ref{fig:sector_tracker}).

\begin{figure}[htpb]
  \def\svgwidth{\largefigwidth}
  \import{chapters/nemo-3-detector/}{TrackerLayout.pdf_tex}
  \caption[Plan view of the tracker in one NEMO-3 sector.]{Plan view of the tracker in one NEMO-3 sector. The position of the tracker cells and petal calorimeter blocks relative to the source foil is shown. The wiring of the tracker cells is shown in detail, where the black dots represent field wires, the anode wires are shown in green and the cathode rings in red.}
  \label{fig:sector_tracker}
\end{figure}

Each cell was made of eight 50\um $\times$ 2.7 m grounded field shaping wires placed in a radius of 1.5 cm around one anode wire of the same dimensions to which a high voltage was applied, creating an approximately cylindrical electric field. The field wires were shared between adjacent cells and one extra wire was placed between cells on different layers to minimise the electric field bleed between cells. At the top and bottom of each cell there were grounded copper rings to pick up induced signals.

The tracker was immersed in an atmosphere consisting of 95\% He, 4\% ethanol vapour and 1\% Ar. An electron with energy of a few\MeV traversing the chamber would ionise the gas at a rate of $\sim$ 6 electrons/cm. The freed electrons would then be accelerated through the strong electric field further ionising the gas. The voltage on the anode wire was set such that this avalanche of electrons would saturate, generating a constant amount of charge independently of the amount of initial ionisation. Ultraviolet radiation was emitted isotropically from the recombination of ionised gas atoms, spreading the avalanche stochastically along the length of the cell.

The gas mixture was optimised to provide an optimal tracking medium while keeping the energy losses in the tracker low: He is used for its low density and Ar is added to fine tune the amount of charge produced (having a lower ionisation potential than He); ethanol was added as a quencher, absorbing the recombination ultraviolet radiation through molecular dissociation. Other parameters such as the dimensions of the cells and thickness of the wires were also optimised, with the ageing effects common in gaseous detectors being taken into account.

For each cell, the voltage on the anode wire and on the cathode rings was monitored, providing three signals from which the positional information was extracted. The time of the first signal, on the anode wire, was compared to that of a hit calorimeter module. From the time difference between these two signals, typically up to 5\us, a radial distance from the anode wire was extracted. By comparing the arrival time of the avalanche to the cathode rings at the ends of a cell to the time of the anode signal, the longitudinal position of the hit was obtained. The resolution on the measured radial and longitudinal coordinates was, respectively, 0.5 mm and 0.8 cm.

While each cell provided only two coordinates of the hit position (radial distance from the anode wire and longitudinal position along the cell), a full three-dimensional reconstruction of the particle tracks was possible by clustering tracker hits in an event and finding the track or set of tracks which better resolved the ambiguities.

\section{Calorimeter}
Besides providing the energy measurement of particles detected in the experiment, the NEMO-3 calorimeter's signals were used as the basis of time-of-flight measurements and to provide a fast trigger. As such it was required to have good timing as well as energy resolutions.

The NEMO-3 calorimeter consisted of 1940 modules placed mostly in the inner and outer walls which completely surrounded the source foil and also in the top and bottom endcaps of the detector as shown in Figure \ref{fig:sector_tracker} above.

Each calorimeter module was composed of a plastic scintillator block attached to a PMT through a light guide. The scintillator blocks were inside the tracking volume, while the PMTs were outside the detector walls, to protect them from the He atmosphere (Figure \ref{fig:calo_block}).

\begin{figure}[htpb]
  \def\svgwidth{\smallfigwidth}
  \import{chapters/nemo-3-detector/}{CaloBlock.pdf_tex}
  \caption[Schematic drawing of a NEMO-3 calorimeter module.]{Schematic drawing of a NEMO-3 calorimeter module. An outer wall module is shown, with its main components indicated by black arrows.}
  \label{fig:calo_block}
\end{figure}

The base of the scintillators was polystyrene (PS), chosen for its radiopurity and to minimise backscattering. For modules in the main calorimeter walls this material was loaded with 1.5\% p-Terphenyl and 0.01\% POPOP acting as scintillating agent and wavelength shifter, respectively. The modules on the endcaps had a different mixture with 1.2\% p-Terphenyl and 0.05\% POPOP. The resulting material was measured to have better radiopurity than the selected PMTs by factors of 430 and 60 for the troublesome isotopes \Bi{214} and \Tl{208}, respectively. The scintillator blocks were produced in seven different shapes to accommodate for the cylindrical geometry of the detector, with all the block types having a depth of 10 cm. This value was chosen to provide some sensitivity to $\gamma$'s. The efficiency of detecting a 500\keV $\gamma$ was measured to be 50\%. The lateral faces of the scintillator blocks were wrapped with a 350\um thick layer of PTFE to enhance light collection by diffusively scattering the scintillation light. The whole blocks were then covered by aluminised Mylar. This final layer provided light-tightness of the block and increased the internal reflection of scintillation light.

The scintillator blocks were connected to the PMTs with a PMMA light guide which provided the interface between the tracker volume and the outside of the detector. The light guides were glued to an iron ring which provided the seal through a friction fit with the detector walls.

The PMTs used in the NEMO-3 calorimeter were developed for low radioactivity applications. Two types of PMT produced by Hamamatsu were chosen for their radiopurity and overall performance. The outer wall calorimeter and the outermost ring of endcap modules were populated with 5$^{\prime\prime}$ PMTs while the inner wall and rest of the endcap modules were instrumented with 3$^{\prime\prime}$ PMTs. The glass used for the production of these PMTs was 100 -- 1000 times more radiopure than standard glass. All PMTs were covered by a mu-metal shield to minimise the effect of the experiment's magnetic field. 

The optical modules were individually characterised and their properties were regularly monitored during the experiment's life time. At 1\MeV the calorimeter modules' energy resolutions ranged between 14\% and 17\% FWHM and their time resolution was 250 ps.

\section{Electronics, DAQ and trigger}
In NEMO-3, separate systems handled the distribution of high voltage (HV) and the data acquisition (DAQ) for the tracker and the calorimeter. A flexible trigger system required both parts of the detector to pass predefined criteria before initiating the read-out.

\subsection{Calorimeter electronics}
Multi-channel power supplies manufactured by CAEN provided the HV to the PMTs. Each HV channel fed three PMTs which were grouped according to their gains. A fixed resistor divider was used to fine tune the HV supplied to each of the PMTs in the group of three. Typically, 3$^{\prime\prime}$ PMTs required 1350 V and 5$^{\prime\prime}$ PMTs operated at 1800 V.

Each PMT was assigned to a DAQ channel which had two preset thresholds. When the low threshold, set at 23\keV, was exceeded, a TDC counter was started and an 80 ns long charge integration window was opened. If the higher threshold energy of 150\keV was exceeded a trigger pulse was sent to the main board responsible for the channel. Each main board monitored a half-sector's worth of PMTs, \emph{i.e.}, the PMTs on a given sector which were on the same side (inner or outer) of the source foil. The main boards would then produce an analogue Level 1 trigger pulse which was proportional to the number of PMTs in their half-sector which had exceeded the high threshold.

If the Level 1 trigger criteria were satisfied, the trigger system would send back a signal initiating the read-out of the contents of the timer and charge integrator. These counters had resolutions equivalent to 53 ps and 3\keV, respectively.

\subsection{Tracker electronics}
A detailed description of the tracker HV distribution and DAQ systems is given in Section \ref{sec:sn_n3_elec} of this thesis. The relevant information is briefly summarised here.

The NEMO-3 tracker cells typically operated at 1620 -- 1650 V. The HV was provided by CAEN power supplies, through fan-out and decoupling circuits. The decoupled signals from the anode wire and the two cathode rings were fed to DAQ boards. Each channel on the DAQ boards consisted of three TDC counters, one for each signal. When an anode signal crossed a preset threshold all TDCs were started. If the fast Level 1 trigger criteria had been satisfied, the trigger system would send a pulse with a pre-determined delay (7.14\us) to the tracker DAQ cards which would stop the TDC counter started by the anode signal. Given the much faster response of the calorimeter compared to the tracker, the time interval between the passage of the particle through the chamber and the arrival of the avalanche to the anode wire could be calculated with the anode TDC value and the known delay in the signal from the trigger system. The cathode TDCs were stopped by signals from the respective cathodes exceeding the threshold.

Each tracker layer in each sector was assigned a Level 2 trigger line, on which a pulse would be sent if any cell in the layer had an anode signal over the threshold. For the whole detector a total of 360 TTL signals were fed to the Level 2 trigger.

If no prompt anode signal was detected on a particular cell, the DAQ window would remain open for $\sim$ 700\us during which time the arrival of an anode signal would still start the anode TDC. This feature was used to detect delayed $\alpha$ emissions from \Bi{214} $\beta$-decay followed by the $\alpha$-decay of \Po{214} with a half-life of 164\us.

The tracker DAQ electronics operated with a 50 MHz clock which gave the TDCs a resolution of 20 ns.

\subsection{Trigger system}
\label{sec:trigger}
The NEMO-3 trigger system operated at three levels:
\begin{itemize}
  \item at Level 1 a minimum calorimeter hit multiplicity was required over the entire detector;
  \item Level 2 applied a set of criteria to the tracker layer multiplicity on a sector-by-sector basis;
  \item Level 3 combined the information used in Level 2 with the calorimeter hit multiplicity at the half-sector level.
\end{itemize}

Special trigger criteria were defined for different types of calibration runs which used the three levels.

For regular data taking runs only levels 1 and 2 were used. At Level 1 it was required that at least one calorimeter module had a signal over the 150\keV threshold. At Level 2 it was required that there were at least three layers with tracker hits in one half-sector. Furthermore, it was required that two of these layers were in the same group (the four nearest to the foil, the two in the middle, or the three nearest to the calorimeter wall). No correlation between the position of tracker hits and the calorimeter hits was required.

The average trigger rate during regular data acquisition runs was 7.5 Hz before the anti-radon facility was installed (Section \ref{sec:antirad}), dropping to 5.6 Hz after the system was in operation. At these trigger rates the dead time of the DAQ system was $\sim$ 1\% of the run time.

\section{Energy and time calibration}
The NEMO-3 calorimeter was calibrated regularly so that energy and time measurements taken throughout the 8 years the detector operated remained accurate. Time consuming calibration runs where radioactive sources were introduced in the detector were performed every $\sim$ 40 days. Between these runs, the stability of the calorimeter modules was monitored daily with a laser survey.

\subsection{Radioactive sources}
\label{sec:det_rad_calib}
Each sector had a vertical copper tube located on the source foil plane, between source strips, into which a Delrin rod could be inserted. Each tube had three pairs of Kapton windows, with paired windows pointing inwards and outwards at the main calorimeter walls. The Delrin rods carried calibration sources which would be located at the height of the Kapton window pairs when inserted in the calibration tubes. The location of the windows at Z $=0$, $\pm$ 90 cm was optimised to ensure a uniform exposure of the calorimeter modules to the calibration sources.

For the regular calibration runs, alternative trigger modes were used, depending on the source isotope.

Two source isotopes were used for energy scale calibration runs:
\begin{itemize}
\item \Bi{207} sources provided two calibration points through the emission of conversion electrons at 0.48\MeV and 0.98\MeV;
\item a higher energy calibration point was provided by \Sr{90} sources via the end-point of the $\beta$-decay spectrum of the daughter isotope \Y{90}, at 2.3\MeV.
\end{itemize}

The \Bi{207} source data was also used to evaluate the dependency of the calorimeter modules' response on the impact point of the electrons on the scintillator blocks' face.

For the time calibration, \Co{60} sources were used. This isotope produces a cascade of two $\gamma$'s emitted coincidentally with energies of 1.2\MeV and 1.3\MeV. This allowed for measurements of the time delays in individual calorimeter modules.

In addition to the above, \emph{special} calibration runs were performed where sources with well known activities were inserted into the calibration tubes and data was collected using the regular trigger mode. These runs provide the calibration of the overall efficiency of the detector.

\subsection{Laser calibration system}
The stability of the calorimeter modules was monitored daily with a laser calibration system. A N$_2$ laser beam was shifted to a wavelength of 420 nm and attenuated by an adjustable factor before being split into two beams. One of the beams was fed to all the scintillator blocks in the detector via optical fibre cables, while the other was sent to six reference calorimeter modules, external to the detector. The reference modules were exposed to \Bi{207} sources therefore allowing for an absolute measurement of the laser light intensity in terms of an equivalent electron energy. The values read out by the experiment's calorimeter modules were then compared to the reference.

Calorimeter modules which showed energy scale instability were flagged, to be removed during data analysis. During the whole experiment no periods of instability were found for 82\% of the calorimeter modules, with a further 7\% having unstable behaviour in only one isolated period.

The laser calibration system was also used to test the linearity of the calorimeter modules, by scanning the attenuation factor of the laser beam through a series of steps.

\section{Magnetic field}
The NEMO-3 detector was immersed in a magnetic field produced by a coil wound around the outer calorimeter wall. This solenoid provided a 2.5 mT field in a direction parallel to the tracker wires which allowed for the discrimination of electrons from positrons. The magnetic field enabled the rejection of electron-positron pair production events from the external $\gamma$ flux with an efficiency of 95\%.

\section{Passive shielding}
The laboratory where NEMO-3 operated, LSM, is located under the peak of Mount Fr\'ejus, on the French-Italian border. The cosmic muon flux is reduced by a factor of one million by the 1200 m of rock (4800 m water-equivalent) above the laboratory. This flux was measured in the laboratory to be 5.4 muons/m$^2$/day\cite{Schmidt:2013gdc}.

The remaining external sources of background are the $\gamma$ and neutron flux from radioactive decay in the laboratory's rock surroundings.

To shield the detector against the $\gamma$ flux a 20 cm layer of pure iron was placed around the magnetic coil and also on the top and bottom endcaps. This shield also provided a return path for the magnetic field.

The effect of the neutron flux was minimised by a three-part shield. A 20 cm thick layer of paraffin was placed below the central tower, 20 cm thick layers of wood were placed above and below the detector and finally ten 35 cm thick borated water tanks were positioned around the detector's walls, with 20 cm deep wood sections separating them. Incoming neutrons would be slowed down by the wood and water and captured by the boron or iron nuclei in the outer layers of the shield. Any $\gamma$ emissions resulting from neutron capture would be absorbed by the iron shield.

\section{Anti-radon facility}
\label{sec:antirad}
About one year into the running of the experiment it was found that the radioactivity due to \Rn{222}, a radioactive noble gas, was higher than it would be expected from the previously measured contamination in the detector components. This excess in \Rn{222} activity was thought to be due to diffusion of radon from the laboratory air into the detector volume.

To reduce the activity of this potential background source a polyethylene tent was built around the detector through which radon-clean air was continuously flushed at a rate of \mbox{150 m$^3$/h}. To produce the low-radon gas, air from the laboratory was passed through two columns, each with 500 kg of activated charcoal kept at a temperature of \mbox{$-$50 $^\circ$C}. The charcoal provided a very large surface on which the radon atoms were adsorbed, containing these atoms until they decayed into non-gaseous isotopes. A reduction factor of 1000 was obtained in the activity of \Rn{222}.

After the introduction of this system the measured activity of \Rn{222} in the detector volume was reduced from 37.7 $\pm$ 0.1 mBq/m$^3$ to 6.46 $\pm$ 0.05 mBq/m$^3$\cite{Argyriades:2009vq}. The smaller reduction factor inside the detector compared to the reduction in the surrounding air indicated that part of the \Rn{222} activity inside the detector was a result of radon emanation from the internal components of the detector.

Data taken before and after this system was in operation are analysed separately, given the different activities of backgrounds due to \Rn{222}.
