#!/usr/bin/python

# This is a python script to split a thesis into chapters - cv 27/6/14

import re
from subprocess import call

# Open table of contents file
tocFile = open('thesis.toc', 'r')
tocFileStr = tocFile.read()

# Read chapter number, name, and starting page from relevant lines
chapterPattern = '\\\\contentsline \{chapter\}\{\\\\numberline \{(?P<chNum>.*?)\}(?P<chName>.*)\}\{(?P<chPage>.*)\}'
chapterRe = re.compile(chapterPattern)
listChapters = chapterRe.findall(tocFileStr)

biblioPattern = '\\\\contentsline \{chapter\}\{Bibliography\}\{(?P<bibPage>.*)\}'
biblioRe = re.compile(biblioPattern)
biblioPage = biblioRe.findall(tocFileStr)

# Use gs so split the pdfs. cumbersome but available everywhere

# Chapter 0. i.e., front matter:
call(["gs", "-sDEVICE=pdfwrite", "-dNOPAUSE", "-dBATCH", "-dSAFER", "-dFirstPage=1", "-dLastPage="+str(int(listChapters[0][2])-1), "-sOutputFile=chapter0.pdf", "thesis.pdf"])
# Actual chapters
for i in range(0, len(listChapters)):
    if i != len(listChapters)-1 :
        print i, listChapters[i][1], 'Pages', listChapters[i][2], int(listChapters[i+1][2])-1
        call(["gs", "-sDEVICE=pdfwrite", "-dNOPAUSE", "-dBATCH", "-dSAFER", "-dFirstPage="+str(listChapters[i][2]), "-dLastPage="+str(int(listChapters[i+1][2])-1), "-sOutputFile=chapter"+listChapters[i][0]+".pdf", "thesis.pdf"])
    else :
        print i, listChapters[i][1], 'Pages', listChapters[i][2]
        call(["gs", "-sDEVICE=pdfwrite", "-dNOPAUSE", "-dBATCH", "-dSAFER", "-dFirstPage="+str(listChapters[i][2]), "-dLastPage="+str(int(biblioPage[0])-1),"-sOutputFile=chapter"+listChapters[i][0]+".pdf", "thesis.pdf"])

call(["gs", "-sDEVICE=pdfwrite", "-dNOPAUSE", "-dBATCH", "-dSAFER", "-dFirstPage="+str(biblioPage[0]), "-sOutputFile=bibliography.pdf", "thesis.pdf"])

